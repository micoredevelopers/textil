<!DOCTYPE html>
    <?include 'head.php' ?>
    <body>
        <?include 'header.php' ?>
     <main>
        <?include 'questions_answers.php' ?>
     	<section class="sec_1">
     		<h2>Наши направления</h2>
     		<p class="bot_h">
     			TEHCRISTAL - представительство французской компании Glassdebourg, специализирующаяся<br> на проектировании, монтаже и отделке, офисов
     			и апартаментов с применением стекла и зеркал.
     		</p>
     		<ul class="pros">
     			<li>Более 400 успешно реализованных проектов</li>
     			<li>8 из 10 проектов сдаются ранще срока</li>
     			<li>успешно реализованных проектов</li>
     			<li>Более 400 успешно реализованных проектов</li>
     			<li>Более 400 успешно реализованных проектов</li>
     			<li>8 из 10 проектов сдаются ранще срока</li>
     		</ul>
     		<ul class="structure">
     			<li><a href="#" style="background-image: url('/_/i/2ic1.jpg')">Отели и гостиницы</a></li>
     			<li><a href="#" style="background-image: url('/_/i/2ic2.jpg')">Офисные помещения</a></li>
     			<li><a href="#" style="background-image: url('/_/i/2ic3.jpg')">Общественные здания</a></li>
     			<li><a href="#" style="background-image: url('/_/i/2ic4.jpg')">Торговые центры </a></li>
     			<li><a href="#" style="background-image: url('/_/i/2ic5.jpg')">Спорт и развлечение</a></li>
     			<li><a href="#" style="background-image: url('/_/i/2ic6.jpg')">Частные интерьеры </a></li>
     			<li><a href="#" style="background-image: url('/_/i/2ic7.jpg')">Гос. учреждения</a></li>
     			<li><a href="#" style="background-image: url('/_/i/2ic8.jpg')"> Школы и университеты</a></li>
     			<li><a href="#" style="background-image: url('/_/i/2ic9.jpg')">Больницы и клиники</a></li>
     		</ul>
     	</section>
     	<section class="sec_2">
     		<h2>Примеры работ</h2>
     		<div class="ex_work">
     		    <div class="c4">
		     		<a href="#" class="c3 block1">
		     			<img src="/_/i/x.gif" alt="" style="background: url('/_/i/home/im1.jpg');background-size: cover !important;background-repeat: no-repeat;">
		     		    <div class="text_block">
		     		    	<div>
			     		    	<p>Дом у дороги</p>
			     		    	<p>Проектирование и монтаж мебели</p>
		     		    	</div>
		     		    </div>	
		     		</a>
		     		<a href="#" class="c3 block2">
		     			<img src="/_/i/x.gif" alt="" style="background: url('/_/i/home/im2.jpg');background-size: cover !important;background-repeat: no-repeat;">
		     		    <div class="text_block">
		     		    	<div>
			     		    	<p>Дом у дороги</p>
			     		    	<p>Проектирование и монтаж мебели</p>
		     		    	</div>
		     		    </div>	
		     		</a>
		     		<a href="#" class="c3 block3">
		     			<img src="/_/i/x.gif" alt="" style="background: url('/_/i/home/im3.jpg');background-size: cover !important;background-repeat: no-repeat;">
		     		    <div class="text_block">
		     		    	<div>
			     		    	<p>Дом у дороги</p>
			     		    	<p>Проектирование и монтаж мебели</p>
		     		    	</div>
		     		    </div>	
		     		</a>
		     		<a href="#" class="c3 block4">
		     			<img src="/_/i/x.gif" alt="" style="background: url('/_/i/home/im4.jpg');background-size: cover !important;background-repeat: no-repeat;">
		     		    <div class="text_block">
		     		    	<div>
			     		    	<p>Дом у дороги</p>
			     		    	<p>Проектирование и монтаж мебели</p>
		     		    	</div>
		     		    </div>	
		     		</a>
		    	</div> 
		    	<a href="#" class="c3 block5">
		     		<img src="/_/i/x.gif" alt="" style="background: url('/_/i/home/im6.jpg');background-size: cover !important;background-repeat: no-repeat;">
		     	    <div class="text_block">
		     		    	<div>
			     		    	<p>Дом у дороги</p>
			     		    	<p>Проектирование и монтаж мебели</p>
		     		    	</div>
		     		    </div>	
		     	</a>
		     	<a href="#" class="c3 block6">
		     		<img src="/_/i/x.gif" alt="" style="background: url('/_/i/home/im7.jpg');background-size: cover !important;background-repeat: no-repeat;">
		     	    <div class="text_block">
		     		    	<div>
			     		    	<p>Дом у дороги</p>
			     		    	<p>Проектирование и монтаж мебели</p>
		     		    	</div>
		     		    </div>	
		     	</a>	
		     	<div class="c3_2 block7">
		     		<img src="/_/i/x.gif" alt="" style="background: url('/_/i/home/im8.jpg');background-size: cover !important;background-repeat: no-repeat;">
			     	<div class="container_stock">
			     		<a href="#" class="stock">Акция</a>
			     		<p class="stock_time">20 мая 2015</p>
		     			<p class="stock_text">
			     			Каждое  произведение Чингиза Аитматова-это открытие,<br>
			     			Каждое  произведение Чингиза Аитматова-это<br>
			     			Каждое  произведение Чингиза<br>
			     			Каждое<br>
		     			</p>
		     		</div>	
		     	</div>			
	     	</div>	
     	</section>
     	<div class="news_partners">
     		<article>
	     		<a href="#">
	     			<h3>Партнерам</h3>
	     			<p>Ты жива еще, моя старушка? Жив и я. Привет тебе, привет! Пусть струится над твоей избушкой Тот вечерний несказанный сы жива еще, моя старушка? Жив и я. Привет тебе, привет! Пусть струится над твоей избушкой Тот вечерний несказанный свет.</p>
	     		    <p>Пишут мне, что ты, тая тревогу, Загрустила ши ив и я. Привет тебе, привет! Пусть струится над твоей избушкбко обо мне,</p>
	     		</a>
     		</article>
     		<article>
     			<h3>Новости</h3>
     			 <div>
	     			<div class="news" id="tab1">
		     			<time  datetime="2009-11-13">15 ноября 2014</time>
		     			<p>Ты жива еще, моя старушка? Жив и я. Привет тебе, привет! Пусть струится над твоей избушкой Тот вечерний несказанный сы жива еще, моя старушка? Жив и я. Привет тебе, привет! Пусть струится над твоей избушкой Тот вечерний несказанный свет.</p>
		     		    <p>Пишут мне, что ты, тая тревогу, Загрустила ши ив и я. Привет тебе, привет! Пусть струится над твоей избушкбко обо мне,</p>
	     	        </div>
	     	        <div class="news" id="tab2">
		     			<time  datetime="2009-11-13">15 ноября 2014 1</time>
		     			<p>Ты жива еще, моя старушка? Жив и я. Привет тебе, привет! Пусть струится над твоей избушкой Тот вечерний несказанный сы жива еще, моя старушка? Жив и я. Привет тебе, привет! Пусть струится над твоей избушкой Тот вечерний несказанный свет.</p>
		     		    <p>Пишут мне, что ты, тая тревогу, Загрустила ши ив и я. Привет тебе, привет! Пусть струится над твоей избушкбко обо мне,</p>
	     	        </div>
	     	        <div class="news" id="tab3">
		     			<time  datetime="2009-11-13">15 ноября 2014 2</time>
		     			<p>Ты жива еще, моя старушка? Жив и я. Привет тебе, привет! Пусть струится над твоей избушкой Тот вечерний несказанный сы жива еще, моя старушка? Жив и я. Привет тебе, привет! Пусть струится над твоей избушкой Тот вечерний несказанный свет.</p>
		     		    <p>Пишут мне, что ты, тая тревогу, Загрустила ши ив и я. Привет тебе, привет! Пусть струится над твоей избушкбко обо мне,</p>
	     	        </div>
	     	         <div class="news" id="tab4">
		     			<time  datetime="2009-11-13">15 ноября 2014 3</time>
		     			<p>Ты жива еще, моя старушка? Жив и я. Привет тебе, привет! Пусть струится над твоей избушкой Тот вечерний несказанный сы жива еще, моя старушка? Жив и я. Привет тебе, привет! Пусть струится над твоей избушкой Тот вечерний несказанный свет.</p>
		     		    <p>Пишут мне, что ты, тая тревогу, Загрустила ши ив и я. Привет тебе, привет! Пусть струится над твоей избушкбко обо мне,</p>
	     	        </div>
	     	     </div>   
	     		<ul class="tab">
	     			<li class="active"><a href="#tab1"></a></li>
	     			<li><a href="#tab2"></a></li>
	     			<li><a href="#tab3"></a></li>
	     			<li><a href="#tab4"></a></li>
	     		</ul>
     		</article>
     	</div>
     	<p class="slogan">
     		Tehcristal-инновации, качество и безопасность<br> 
     		<span>в сочетании с эстетикой и функциональностью</span>
     	</p>
     	
     <main>
        <?include 'footer.php' ?>
        
    </body>
</html>
