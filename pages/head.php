    <head>
      <meta http-equiv="content-type" content="text/html; charset=utf-8" />
      <title>Tehcristal</title>
      <link href="/_/i/favicon.ico" rel="shortcut icon" type="image/x-icon" />
      <link type="text/css" rel="stylesheet" href="/_/css/style.css" />
      <!-- <link type="text/css" rel="stylesheet/less" type="text/css" href="/_/css/style.less" /> -->
      <link type="text/css" rel="stylesheet" href="/_/css/content.css" />
      <link rel="stylesheet" type="text/css" href="/_/fancyBox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
      <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
      <!--<script type="text/javascript" src="/_/js/less.js"></script>-->
      <script type="text/javascript" src="/_/js/jquery.cycle.all.js"></script>
      <script type="text/javascript" src="/_/js/modernizr.custom.11333.js"></script>
      <script type="text/javascript" src="/_/js/jquery.easing.1.3.js"></script>
      <script type="text/javascript" src="/_/fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
      <script type="text/javascript" src="/_/js/my.js"></script>
      <!--[if lt IE 9]><script src="/_/js/html5shiv-3.7.min.js"></script><![endif]-->
      <!--[if IE 8 ]> 
      <link rel="stylesheet" type="text/css" href="/_/css/site_ie8.css">
      <![endif]-->
      <!--[if lt IE 8]>
      <link rel="stylesheet" href="/_/css/old-ie.css" media="screen,projection" />
      <script src="/_/js/old-ie.js"></script>
      <![endif]-->
    </head>