<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="language" content="en" />
  <link href="/_/i/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/www/themes/bootstrap/css/styles.css" />
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="/www/_/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="/www/themes/bootstrap/jquery.fileupload-ui-noscript.css"></noscript>
  <title><?php echo CHtml::encode($this->pageTitle); ?></title>

  <?php Yii::app()->bootstrap->register(); ?>
</head>

<body>
<??>
<?php 
  $this->widget('bootstrap.widgets.TbNavbar',array(
  'brandUrl' => Yii::app()->getBaseUrl(true),
   'collapse'=>true,
    'items'=>array(
              array(
                'class'=>'bootstrap.widgets.TbMenu',
                'items'=>array(                  
                  array(
                          'label'=>'Настройки',
                          'url'=>'#',
                          // 'active' => Yii::app()->controller->id == 'catalog',
                          'visible'=> Yii::app()->user->isAdmin(),
                          'items'=> array(
                              array(
                                'label'=>Yii::t('main','Текстовые страницы'),
                                'url'=>array("/admin/pages/admin"),
                                'active' => Yii::app()->controller->id == 'pages',
                                'visible'=> Yii::app()->user->isAdmin(),
                              ), 
                              // array(
                              //   'label'=>Yii::t('main','Вопросы ответы'),
                              //   'url'=>array("/admin/qa/admin"),
                              //   'active' => Yii::app()->controller->id == 'qa',
                              //   'visible'=> Yii::app()->user->isAdmin(),
                              // ), 
                              array(
                                'label'=>Yii::t('main','Карусель header'),
                                'url'=>array("/admin/carousel_header/admin"),
                                'active' => Yii::app()->controller->id == 'Carousel_header',
                                'visible'=> Yii::app()->user->isAdmin(),
                              ),
                              array(
                                'label'=>Yii::t('main','Карусель'),
                                'url'=>array("/admin/carousel/admin"),
                                'active' => Yii::app()->controller->id == 'carousel',
                                'visible'=> Yii::app()->user->isAdmin(),
                              ),
                                       array(
                                'label'=>Yii::t('main','Карусель главная маленкии'),
                                'url'=>array("/admin/carousel_main_small/admin"),
                                'active' => Yii::app()->controller->id == 'Carousel_main_small',
                                'visible'=> Yii::app()->user->isAdmin(),
                              ),
                              array(
                                'label'=>Yii::t('main','Фразы сайта'),
                                'url'=>array("/admin/labels/admin"),
                                'active' => Yii::app()->controller->id == 'labels',
                                'visible'=> Yii::app()->user->isAdmin(),
                              ),
                              // array(
                              //   'label'=>Yii::t('main','Прецентация'),
                              //   'url'=>array("/admin/prezentation/admin"),
                              //   'active' => Yii::app()->controller->id == 'prezentation',
                              //   'visible'=> Yii::app()->user->isAdmin(),
                              // ),
                              array(
                                'label'=>Yii::t('main','Emails'),
                                'url'=>array("/admin/email/admin"),
                                'active' => Yii::app()->controller->id == 'email',
                                'visible'=> Yii::app()->user->isAdmin(),
                              ),
                              array(
                                'label'=>Yii::t('main','Заказы'),
                                'url'=>array("/admin/orders/admin"),
                                'active' => Yii::app()->controller->id == 'orders',
                                'visible'=> Yii::app()->user->isAdmin(),
                              ),
                              array(
                                  'label'=>Yii::t('main','Изминение URL'),
                                  'url'=>array("/admin/routes"),
                                  'active' => Yii::app()->controller->id == 'routes',
                                  'visible'=> Yii::app()->user->isAdmin(),
                              ),
                              array(
                                  'label'=>Yii::t('main','Robots.txt'),
                                  'url'=>array("/admin/text_files/robots"),
                                  'active' => Yii::app()->controller->id == '',
                                  'visible'=> Yii::app()->user->isAdmin(),
                              ),
                              array(
                                  'label'=>Yii::t('main','Скрипты'),
                                  'url'=>array("/admin/text_files/scripts"),
                                  'active' => Yii::app()->controller->id == '',
                                  'visible'=> Yii::app()->user->isAdmin(),
                              ),
                              array(
                                  'label'=>Yii::t('main','Редиректы'),
                                  'url'=>array("/admin/redirects"),
                                  'active' => Yii::app()->controller->id == '',
                                  'visible'=> Yii::app()->user->isAdmin(),
                              ),
                            ),
                        ),

                  // array(
                  //         'label'=>'Foto site',
                  //         'url'=>array('/admin/fotoSite/admin'),
                  //         'active' => Yii::app()->controller->id == 'fotoSite',
                  //         'visible'=> Yii::app()->user->isAdmin(),
                  //         ),
                  array(
                          'label'=>'Фото саита',
                          'url'=>array('/admin/foto/admin'),
                          'active' => Yii::app()->controller->id == 'foto',
                          'visible'=> Yii::app()->user->isAdmin(),
                          ),
                  // array(
                  //         'label'=>Yii::t('main','Прецентация'),
                  //         'url'=>array("/admin/prezentation/admin"),
                  //         'active' => Yii::app()->controller->id == 'prezentation',
                  //         'visible'=> Yii::app()->user->isAdmin(),
                  //       ),
                  array(
                          'label'=>'Статьи',
                          'url'=>array('/admin/news/admin'),
                          'active' => Yii::app()->controller->id == 'news',
                          'visible'=> Yii::app()->user->isAdmin(),
                          ),
                  // array(
                  //         'label'=>'Статьи',
                  //         'url'=>array('/admin/action/admin'),
                  //         'active' => Yii::app()->controller->id == 'action',
                  //         'visible'=> Yii::app()->user->isAdmin(),
                  //         ),
                  /*array(
                          'label'=>'Решения',
                          'url'=>'#',
                          // 'active' => Yii::app()->controller->id == 'catalog',
                          'visible'=> Yii::app()->user->isAdmin(),
                          'items'=> array(
                              array(
                                'label'=>Yii::t('main','Категорий'),
                                'url'=>array("/admin/categorysolution/admin"),
                                'active' => Yii::app()->controller->id == 'categorysolution',
                                'visible'=> Yii::app()->user->isAdmin(),
                              ),
                              array(
                              'label'=>'Решения',
                              'url'=>array('/admin/solution/admin'),
                              'active' => Yii::app()->controller->id == 'solution',
                              'visible'=> Yii::app()->user->isAdmin(),
                              ), 
                            ),
                        ),*/
                  /*array(
                          'label'=>'Изделия',
                          'url'=>'#',
                          // 'active' => Yii::app()->controller->id == 'catalog',
                          'visible'=> Yii::app()->user->isAdmin(),
                          'items'=> array(
                              array(
                              'label'=>'Категорий',
                              'url'=>array('/admin/category/admin'),
                              'active' => Yii::app()->controller->id == 'category',
                              'visible'=> Yii::app()->user->isAdmin(),
                              ),
                              array(
                              'label'=>'Изделия',
                              'url'=>array('/admin/articols/admin'),
                              'active' => Yii::app()->controller->id == 'articols',
                              'visible'=> Yii::app()->user->isAdmin(),
                              ),

                            ),
                        ),*/
                  array(
                          'label'=>'Продукция',
                          'url'=>'#',
                          // 'active' => Yii::app()->controller->id == 'catalog',
                          'visible'=> Yii::app()->user->isAdmin(),
                          'items'=> array(
                              array(
                              'label'=>'Продукция',
                              'url'=>array('/admin/production/admin'),
                              'active' => Yii::app()->controller->id == 'production',
                              'visible'=> Yii::app()->user->isAdmin(),
                              ),
                              array(
                              'label'=>'Категорий',
                              'url'=>array('/admin/categoryproduct/admin'),
                              'active' => Yii::app()->controller->id == 'category',
                              'visible'=> Yii::app()->user->isAdmin(),
                              ),
                              // array(
                              // 'label'=>'Филтры',
                              // 'url'=>array('/admin/orientation/admin'),
                              // 'active' => Yii::app()->controller->id == 'orientation',
                              // 'visible'=> Yii::app()->user->isAdmin(),
                              // ),
                              // array(
                              // 'label'=>'Цвета',
                              // 'url'=>array('/admin/colors/admin'),
                              // 'active' => Yii::app()->controller->id == 'category',
                              // 'visible'=> Yii::app()->user->isAdmin(),
                              // ),
                              // array(
                              // 'label'=>'Размеры',
                              // 'url'=>array('/admin/size/admin'),
                              // 'active' => Yii::app()->controller->id == 'category',
                              // 'visible'=> Yii::app()->user->isAdmin(),
                              // ),

                            ),
                        ),
                  array(
                          'label'=>Yii::t('main','Пользователи'),
                          'url'=>array("/admin/user/admin"),
                          'active' => Yii::app()->controller->id == 'user',
                          'visible'=> Yii::app()->user->isAdmin(),
                        ),

                 

                  
       // ), 
  //  ),//Settings
          array(
            'label'=>'Profile',
            'url'=>array('/admin/user/profile'),
            'active' => Yii::app()->controller->id == 'user',
            'visible'=> !Yii::app()->user->isAdmin() && !Yii::app()->user->isGuest,
            ),
          
        ),
    ),
    array(
            'class'=>'bootstrap.widgets.TbMenu',
            'htmlOptions'=>array('class'=>'pull-right'),
            'items'=>array(
                      array(
                  'label'=>Yii::t('main','Выйти').' ('.Yii::app()->user->name.')',
                  'url'=>array('/site/logout'), 
                  'visible'=>!Yii::app()->user->isGuest
                  ),
            ),
        ),
    
    ),
)); ?>
<div class="container" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
      'homeLink'=>CHtml::link('Главная', array('/admin')),
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">

	</div><!-- footer -->

</div><!-- page -->


</body>
</html>
