<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/admin'); ?>

<div class="row">

<div class="span12"><?
              $this->widget('bootstrap.widgets.TbMenu', array(
                  'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
  #                'stacked'=>false, // whether this is a stacked menu
                  'items'=>$this->menu,
  #                'htmlOptions'=>array('class'=>'operations'),
              ));
  
  
   ?>
  <?php 
    $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            // 'error'
        ),
    )); ?>

   </div><? /* ?>
    <div class="span3">
        <div id="sidebar">
        <?php
#            $this->beginWidget('zii.widgets.CPortlet', array(
#                'title'=>'Operations',
#            ));
#            $this->endWidget();
        ?>
        </div><!-- sidebar -->
    </div>
    <div class="span10">
    <?*/ ?>
      <div class="span12">
        <div id="content">

            <?php echo $content; ?>

        </div><!-- content -->
      </div>
<?/* ?>    </div><?*/ ?>
</div>
<?php $this->endContent(); ?>
