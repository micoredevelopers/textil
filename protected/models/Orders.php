<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property string $name
 * @property string $date
 * @property string $ip
 * @property string $address
 * @property string $order
 * @property string $price
 * @property integer $status
 * @property string $phone_contacts
 * @property string $paymant
 * @property string $comment_user
 * @property string $user_email
 */
class Orders extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, date, ip, address, order, price, status, phone_contacts, paymant, comment_user, user_email', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('name, price, phone_contacts, paymant, user_email', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, date, ip, address, order, price, status, phone_contacts, paymant, comment_user, user_email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'date' => 'Date',
			'ip' => 'Ip',
			'address' => 'Address',
			'order' => 'Order',
			'price' => 'Price',
			'status' => 'Status',
			'phone_contacts' => 'Phone Contacts',
			'paymant' => 'Paymant',
			'comment_user' => 'Comment User',
			'user_email' => 'User Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('order',$this->order,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('phone_contacts',$this->phone_contacts,true);
		$criteria->compare('paymant',$this->paymant,true);
		$criteria->compare('comment_user',$this->comment_user,true);
		$criteria->compare('user_email',$this->user_email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}