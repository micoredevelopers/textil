<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $title_ru
 * @property string $title_ro
 * @property string $title_en
 * @property string $alias
 * @property integer $menu
 * @property integer $parent
 * @property integer $status
 */
class Category extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_ru, title_ro, title_en, alias, parent, status, description_ro, description_ru, description_en, position', 'safe'),
			array('alias', 'unique'),
			array('parent, status', 'numerical', 'integerOnly'=>true),
			array('title_ru, title_ro, title_en, alias', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title_ru, title_ro, title_en, alias, parent, status, description_ro, description_ru, description_en, position', 'safe', 'on'=>'search'),
		);
	}
	public function getTitle(){
	  return $this->{'title_'.Yii::app()->GetLanguage()};
	}
	public function setTitle($value){
	  $this->title = $value;
	}
	public function getDescription(){
	  return $this->{'description_'.Yii::app()->GetLanguage()};
	}
	public function setDescription($value){
	  $this->description = $value;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title_ru' => 'Title Ru',
			'title_ro' => 'Title Ro',
			'title_en' => 'Title En',
			'description_ro' => 'Description Ro',
			'description_ru' => 'Description Ru',
			'description_en' => 'Description En',
			'alias' => 'Alias',
			'parent' => 'Parent',
			'status' => 'Status',
			'position' => 'Position',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('title_en',$this->title_en,true);
		$criteria->compare('description_ro',$this->description_ro,true);
		$criteria->compare('description_ru',$this->description_ru,true);
		$criteria->compare('description_en',$this->description_en,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('status',$this->status);
		$criteria->compare('position',$this->position);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}