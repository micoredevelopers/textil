<?php

/**
 * This is the model class for table "articols".
 *
 * The followings are the available columns in table 'articols':
 * @property integer $id
 * @property string $title_ro
 * @property string $title_ru
 * @property string $title_en
 * @property string $description_ro
 * @property string $description_ru
 * @property string $description_en
 * @property string $body_ro
 * @property string $body_ru
 * @property string $body_en
 * @property string $image
 * @property string $price_new
 * @property string $price_old
 * @property integer $in_stoc
 * @property string $tehnic_x
 * @property string $price_list
 * @property integer $new_colection
 * @property string $alias
 * @property string $h1_ru
 * @property string $meta_title_ro
 * @property string $meta_title_ru
 * @property string $meta_title_en
 * @property string $meta_description_ro
 * @property string $meta_description_ru
 * @property string $meta_description_en
 * @property string $meta_keyword_ro
 * @property string $meta_keyword_ru
 * @property string $meta_keyword_en
 * @property string $color_description
 * @property integer unitID
 */
class Production extends CActiveRecord implements ECommercePMInterface, ARMetaTagsInterface
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Articols the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'production';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title_ro, title_ru, title_en, description_ro, description_ru, description_en, body_ro, body_ru, body_en, image, price_new, price_old, tehnic_x, price_list, alias, meta_title_ro, meta_title_ru, meta_title_en, meta_description_ro, meta_description_ru, meta_description_en, meta_keyword_ro, meta_keyword_ru, meta_keyword_en, orientation, category, colors, size, prod_propus,articul,size,zakaz,discount', 'safe'),
            array('in_stoc, zakaz,new_colection, unitID', 'numerical', 'integerOnly' => true),
            // array('category', 'required'),
            array('alias', 'unique'),
            array('title_ro, title_ru, title_en, price_new, price_old, h1_ru, alias, meta_title_ro, meta_title_ru, meta_title_en, meta_description_ro, meta_description_ru, meta_description_en, meta_keyword_ro, meta_keyword_ru, meta_keyword_en, color_description', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title_ro, title_ru, title_en, description_ro, description_ru, description_en, body_ro, body_ru, body_en, image, price_new, price_old, in_stoc, tehnic_x, price_list, new_colection, alias, meta_title_ro, meta_title_ru, meta_title_en, meta_description_ro, meta_description_ru, meta_description_en, meta_keyword_ro, meta_keyword_ru, meta_keyword_en, orientation, category, colors, size ,prod_propus,articul,size,zakaz,discount', 'safe', 'on' => 'search'),
        );
    }

    public function getTitle()
    {
       // dump($this->image,1);
        return $this->{'title_' . Yii::app()->GetLanguage()};
    }

    public function setTitle($value)
    {
        $this->title = $value;
    }

    public function getDescription()
    {
        return $this->{'description_' . Yii::app()->GetLanguage()};
    }

    public function setDescription($value)
    {
        $this->description = $value;
    }

    public function getBody()
    {
        return $this->{'body_' . Yii::app()->GetLanguage()};
    }

    public function setBody($value)
    {
        $this->body = $value;
    }

    public function getMeta_title()
    {
        return $this->{'meta_title_' . Yii::app()->GetLanguage()};
    }

    public function setMeta_title($value)
    {
        $this->meta_title = $value;
    }

    public function getMeta_description()
    {
        return $this->{'meta_description_' . Yii::app()->GetLanguage()};
    }

    public function setMeta_description($value)
    {
        $this->meta_description = $value;
    }

    public function getMeta_keyword()
    {
        return $this->{'meta_keyword_' . Yii::app()->GetLanguage()};
    }

    public function setMeta_keyword($value)
    {
        $this->meta_keyword = $value;
    }

    /**
     * @return mixed|null
     */
    public function getUnit(){
        return ProductUnits::getDataByKey($this->unitID);
    }

    /**
     * @return string
     */
    public function getColorDescription(){
        return $this->color_description;
    }

    public function getImagePath(){
        if (!is_file(Yii::getPathOfAlias('root').'/images/Production/'.$this->image)){
            return Yii::app()->request->getBaseUrl(true) . '/public/static/images/no_name.jpg';
        }
        return Yii::app()->request->getBaseUrl(true) . '/images/Production/'.$this->image;
    }


    public function microMarking()
    {
        return [

            "@type" => "Product",
            "name" => $this->getTitle(),
            "image" => [
                $this->getImagePath()
            ],
            "description" => $this->getDescription(),
            "offers" => [

                "@type" => "Offer",
                "priceCurrency" => "USD",
                "price" =>$this->price_new,
            ]

        ];
    }

    /**
     * @return array relational rules.
     */
    public function behaviors()
    {
        return array('CAdvancedArBehavior' => array(
            'class' => 'application.extensions.CAdvancedArBehavior'));
    }

    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'categoties' => array(self::BELONGS_TO, 'Categoryproduct', 'category'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title_ro' => 'Title Ro',
            'title_ru' => 'Title Ru',
            'title_en' => 'Title En',
            'description_ro' => 'Description Ro',
            'description_ru' => 'Description Ru',
            'description_en' => 'Description En',
            'body_ro' => 'Body Ro',
            'body_ru' => 'Body Ru',
            'body_en' => 'Body En',
            'image' => 'Image',
            'orientation' => 'Orientation',
            'category' => 'Category',
            'price_new' => 'Price New',
            'price_old' => 'Price Old',
            'zakaz' => 'Zakaz',
            'in_stoc' => ' ожидается',
            'tehnic_x' => 'Tehnic X',
            'price_list' => 'Price List',
            'new_colection' => 'New Colection',
            'alias' => 'Alias',
            'meta_title_ro' => 'Meta Title Ro',
            'meta_title_ru' => 'Meta Title Ru',
            'meta_title_en' => 'Meta Title En',
            'meta_description_ro' => 'Meta Description Ro',
            'meta_description_ru' => 'Meta Description Ru',
            'meta_description_en' => 'Meta Description En',
            'meta_keyword_ro' => 'Meta Keyword Ro',
            'meta_keyword_ru' => 'Meta Keyword Ru',
            'meta_keyword_en' => 'Meta Keyword En',
            'articul' => 'articul',
            'size' => 'size',
            'discount' => 'discount',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('title_ro', $this->title_ro, true);
        $criteria->compare('title_ru', $this->title_ru, true);
        $criteria->compare('title_en', $this->title_en, true);
        $criteria->compare('description_ro', $this->description_ro, true);
        $criteria->compare('description_ru', $this->description_ru, true);
        $criteria->compare('description_en', $this->description_en, true);
        $criteria->compare('body_ro', $this->body_ro, true);
        $criteria->compare('body_ru', $this->body_ru, true);
        $criteria->compare('body_en', $this->body_en, true);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('orientation', $this->orientation, true);
        $criteria->compare('category', $this->category);
        $criteria->compare('price_new', $this->price_new, true);
        $criteria->compare('price_old', $this->price_old, true);
        $criteria->compare('in_stoc', $this->in_stoc);
        $criteria->compare('zakaz', $this->zakaz);
        $criteria->compare('tehnic_x', $this->tehnic_x, true);
        $criteria->compare('price_list', $this->price_list, true);
        $criteria->compare('new_colection', $this->new_colection);
        $criteria->compare('alias', $this->alias, true);
        $criteria->compare('h1_ru', $this->h1_ru, true);
        $criteria->compare('meta_title_ro', $this->meta_title_ro, true);
        $criteria->compare('meta_title_ru', $this->meta_title_ru, true);
        $criteria->compare('meta_title_en', $this->meta_title_en, true);
        $criteria->compare('meta_description_ro', $this->meta_description_ro, true);
        $criteria->compare('meta_description_ru', $this->meta_description_ru, true);
        $criteria->compare('meta_description_en', $this->meta_description_en, true);
        $criteria->compare('meta_keyword_ro', $this->meta_keyword_ro, true);
        $criteria->compare('meta_keyword_ru', $this->meta_keyword_ru, true);
        $criteria->compare('meta_keyword_en', $this->meta_keyword_en, true);
        $criteria->compare('articul', $this->articul);
        $criteria->compare('size', $this->size);
        $criteria->compare('discount', $this->discount);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'title_ru ASC',
            )
        ));
    }

    /**
     * @return integer
     */
    public function getId()
    {
       return $this->id;
    }

    /**
     * @return name
     */
    public function getName()
    {
        return $this->getTitle();
    }

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->getMeta_title();
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->getMeta_description();
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->getMeta_keyword();
    }

    /**
     * @return string
     */
    public function getH1()
    {
        $this->h1_ru;
    }
}