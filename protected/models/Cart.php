<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 29.11.2018
 * Time: 15:17
 */

class Cart
{
    /**
     * @var CartStorageInterface
     */
    protected $cartStorage;

    /**
     * CartList constructor.
     * @param CartStorageInterface $cartStorage
     */
    public function __construct(CartStorageInterface $cartStorage)
    {
        $this->cartStorage = $cartStorage;
    }

    public function addItem(CartItemInterface $item){
        if ($this->cartStorage->itemExist($item)){
            $q = $item->getQ() + $this->cartStorage->itemQ($item);
            $item->setQ($q > 0 ? $q : 1);
        }
        $this->cartStorage->saveItem($item);
    }

}