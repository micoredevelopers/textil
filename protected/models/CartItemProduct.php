<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 29.11.2018
 * Time: 16:25
 */

class CartItemProduct implements CartItemInterface
{


    protected $q = 1;

    protected $id;

    protected $price;

    protected $title;

    protected $img;

    /**
     * @return integer
     */
    public function getID()
    {
        return $this->id;
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }



    /**
     * @return integer
     */
    public function getQ()
    {
        return (int)$this->q;
    }

    /**
     * @param integer $q
     */
    public function setQ($q)
    {
        if ((int)$q > 0) {
            $this->q = $q;
        }
    }

    public function loadByProduction(Production $production,$q = 1){
        $this -> q = $q;
        $this -> id = $production->id;
        $this -> price = $production->price_new;
        $this -> title = $production->categoties->getTitle() .' '. $production->getTitle();
        $this -> img = $production->getImagePath();
    }

    /**
     * @param Production $production
     * @param int $q
     * @return CartItemProduct
     */
    public static function instanceByModel(Production $production, $q = 1)
    {
        $instance = new static();
        $instance->loadByProduction($production, $q);
        return $instance;
    }

    public static function instancesByStorage(CartListStorageInterface $storage){
        $idList = $storage->idList();
        $items = [];
     //   dump('id IN ('.implode(',',$idList).')');
        if ($idList && $products = ARFinder::find(Production::model())->condition('id IN ('.implode(',',$idList).')')->all()){
           foreach ($products  as $item){
               $items[] = static::instanceByModel($item);
           }
        }
        return $items;
    }


}