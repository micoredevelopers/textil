<?php

/**
 * This is the model class for table "action".
 *
 * The followings are the available columns in table 'action':
 * @property integer $id
 * @property string $title_ro
 * @property string $title_ru
 * @property string $title_en
 * @property string $body_ro
 * @property string $body_ru
 * @property string $body_en
 * @property string $alias
 * @property string $image
 * @property string $date
 * @property integer $status
 * @property string $meta_titile_ro
 * @property string $meta_titile_ru
 * @property string $meta_titile_en
 * @property string $meta_description_ro
 * @property string $meta_description_ru
 * @property string $meta_description_en
 * @property string $meta_keyword_ro
 * @property string $meta_keyword_ru
 * @property string $meta_keyword_en
 */
class Action extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Action the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'action';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_ro, title_ru, title_en, body_ro, body_ru, body_en, alias, image, date, status, meta_titile_ro, meta_titile_ru, meta_titile_en, meta_description_ro, meta_description_ru, meta_description_en, meta_keyword_ro, meta_keyword_ru, meta_keyword_en,top', 'safe'),
			array('title_ro, title_ru, title_en, alias, meta_titile_ro, meta_titile_ru, meta_titile_en', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title_ro, title_ru, title_en, body_ro, body_ru, body_en, alias, image, date, status, meta_titile_ro, meta_titile_ru, meta_titile_en, meta_description_ro, meta_description_ru, meta_description_en, meta_keyword_ro, meta_keyword_ru, meta_keyword_en,top', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	public function getTitle(){
	  return $this->{'title_'.Yii::app()->GetLanguage()};
	}
	public function setTitle($value){
	  $this->title = $value;
	}
	public function getBody(){
	  return $this->{'body_'.Yii::app()->GetLanguage()};
	}
	public function setBody($value){
	  $this->body = $value;
	}
	public function getMeta_title(){
	  return $this->{'meta_title_'.Yii::app()->GetLanguage()};
	}
	public function setMeta_title($value){
	  $this->meta_title = $value;
	}
	public function getMeta_description(){
	  return $this->{'meta_description_'.Yii::app()->GetLanguage()};
	}
	public function setMeta_description($value){
	  $this->meta_description = $value;
	}
	public function getMeta_keyword(){
	  return $this->{'meta_keyword_'.Yii::app()->GetLanguage()};
	}
	public function setMeta_keyword($value){
	  $this->meta_keyword = $value;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title_ro' => 'Title Ro',
			'title_ru' => 'Title Ru',
			'title_en' => 'Title En',
			'body_ro' => 'Body Ro',
			'body_ru' => 'Body Ru',
			'body_en' => 'Body En',
			'alias' => 'Alias',
			'image' => 'Image',
			'date' => 'Date',
			'status' => 'Status',
			'meta_titile_ro' => 'Meta Titile Ro',
			'meta_titile_ru' => 'Meta Titile Ru',
			'meta_titile_en' => 'Meta Titile En',
			'meta_description_ro' => 'Meta Description Ro',
			'meta_description_ru' => 'Meta Description Ru',
			'meta_description_en' => 'Meta Description En',
			'meta_keyword_ro' => 'Meta Keyword Ro',
			'meta_keyword_ru' => 'Meta Keyword Ru',
			'meta_keyword_en' => 'Meta Keyword En',
			'top' => 'top',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('title_en',$this->title_en,true);
		$criteria->compare('body_ro',$this->body_ro,true);
		$criteria->compare('body_ru',$this->body_ru,true);
		$criteria->compare('body_en',$this->body_en,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('meta_titile_ro',$this->meta_titile_ro,true);
		$criteria->compare('meta_titile_ru',$this->meta_titile_ru,true);
		$criteria->compare('meta_titile_en',$this->meta_titile_en,true);
		$criteria->compare('meta_description_ro',$this->meta_description_ro,true);
		$criteria->compare('meta_description_ru',$this->meta_description_ru,true);
		$criteria->compare('meta_description_en',$this->meta_description_en,true);
		$criteria->compare('meta_keyword_ro',$this->meta_keyword_ro,true);
		$criteria->compare('meta_keyword_ru',$this->meta_keyword_ru,true);
		$criteria->compare('meta_keyword_en',$this->meta_keyword_en,true);
		$criteria->compare('top',$this->top);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}