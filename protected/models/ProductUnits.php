<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 19.02.2019
 * Time: 14:40
 */

class ProductUnits
{

    /**
     * @var array
     */
    private static $data = [
        ['name' => 'за метр',],
        ['name' => 'за кг',]
    ];

    /**
     * @return array
     */
    public static function getData(){
        return self::$data;
    }

    public static function getDataSelect(){
        foreach (static::$data as $k=>$v){
            yield $k=>$v['name'];
        }
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public static function getDataByKey($key){
        if (array_key_exists($key, static::getData())){
            return static::getData()[$key];
        }
        return static::getData()[0];
    }

}