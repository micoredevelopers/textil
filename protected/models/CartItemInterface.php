<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 29.11.2018
 * Time: 15:07
 */

interface CartItemInterface
{

    /**
     * @return integer
     */
    public function getID();

    /**
     * @return integer
     */
    public function getQ();

    /**
     * @param integer $q
     */
    public function setQ($q);

    /**
     * @return mixed
     */
    public function getPrice();


}