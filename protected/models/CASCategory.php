<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 28.11.2018
 * Time: 13:10
 */

class CASCategory extends Categoryproduct implements CASCategoryInterface {


    /**
     * @var CASProductInterface[]
     */
    private $products=[];

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Categoryproduct the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return integer
     */
    public function getCategoryID()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCategoryTitle()
    {
        return $this->getTitle();
    }

    /**
     * @param CASProductInterface $product
     * @return mixed
     */
    public function addProduct(CASProductInterface $product)
    {
        $this->products[$product->getProductID()] = $product;
    }

    /**
     * @return CASProductInterface[]
     */
    public function getProducts()
    {
        return $this->products;
    }
}