<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 28.11.2018
 * Time: 12:56
 */

class CASList
{

    /**
     * @var CASCategoryInterface
     */
    protected $items = [];

    /**
     * @return CASCategoryInterface
     */
    public function getItems(){
        return $this->items;
    }

    /**
     * @param CASCategoryInterface $category
     */
    public function addCategory(CASCategoryInterface $category){
        if (!isset($this->items[$category->getCategoryID()])){
            $this->items[$category->getCategoryID()] = $category;
        }
    }

    /**
     * @param CASCategoryInterface[] $categoryArray
     */
    public function addCategoryArray($categoryArray=[]){
        if ($categoryArray && is_array($categoryArray)){
            foreach ($categoryArray as $item) {
                $this->addCategory($item);
            }
        }
    }

    /**
     * @param CASProductInterface $product
     */
    public function addProduct(CASProductInterface $product){
        if (isset($this->items[$product->getProductCategoryID()])){
            $this->items[$product->getProductCategoryID()]->addProduct($product);
        }
    }

    /**
     * @param array $productArray
     */
    public function addProductArray($productArray = []){
        if ($productArray && is_array($productArray)){
            foreach ($productArray as $item) {
                $this->addProduct($item);
            }
        }
    }

}