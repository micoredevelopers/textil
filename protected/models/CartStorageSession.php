<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 29.11.2018
 * Time: 16:55
 */

class CartStorageSession implements CartStorageInterface,CartListStorageInterface
{

    /**
     * @var string
     */
    protected $key;


    protected $cartList;

    /**
     * @param $key
     */
    public function __construct($key = 'cart')
    {
        Yii::app()->session->open();
        $this->key = $key;
    }

    /**
     * @param CartItemInterface $item
     * @return mixed
     */
    public function saveItem(CartItemInterface $item)
    {
        $_SESSION[$this->key][$item->getID()] = [
            'id' => $item->getID(),
            'q' => $item->getQ()
        ];
    }

    /**
     * @param CartItemInterface $item
     * @return mixed
     */
    public function itemQ(CartItemInterface $item)
    {
        if (isset($_SESSION[$this->key][$item->getID()]['q'])) {
            return (int)$_SESSION[$this->key][$item->getID()]['q'];
        }
        return 1;
    }

    /**
     * @param CartItemInterface $item
     * @return mixed
     */
    public function itemExist(CartItemInterface $item)
    {
        return isset($_SESSION[$this->key][$item->getID()]);
    }

    /**
     * @return array
     */
    public function idList()
    {
        $idList = [];
        if (isset($_SESSION[$this->key])){
            foreach ($_SESSION[$this->key] as $item){
                $idList[] = $item['id'];
            }
        }
        return $idList;
    }

    /**
     * @param CartItemInterface $item
     * @return mixed
     */
    public function deleteItem(CartItemInterface $item)
    {
        if (isset($_SESSION[$this->key][$item->getID()])) {
            unset($_SESSION[$this->key][$item->getID()]);
        }
    }

    /**
     * @return mixed
     */
    public function removeItems()
    {
        if (isset($_SESSION[$this->key])) {
            unset($_SESSION[$this->key]);
        }
    }


}