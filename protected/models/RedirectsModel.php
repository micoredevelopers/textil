<?php

/**
 * This is the model class for table "redirects".
 *
 * The followings are the available columns in table 'redirects':
 * @property integer $id
 * @property string $from_r
 * @property string $to_r
 * @property integer $status
 */
class RedirectsModel extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RedirectsModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'redirects';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('from_r', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('from_r, to_r', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, from_r, to_r, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'from_r' => 'From R',
			'to_r' => 'To R',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('from_r',$this->from_r,true);
		$criteria->compare('to_r',$this->to_r,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function multipleSave($objects,$array){
	    if ($objects && is_array($objects) && is_array($array)){
	        foreach ($objects as $k=>$object){
	            if (isset($array[$k])){
                    $objects[$k]->attributes = $array[$k];
                    $objects[$k]->save();
                }
            }
        }
    }
}