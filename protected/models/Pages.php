<?php

/**
 * This is the model class for table "pages".
 *
 * The followings are the available columns in table 'pages':
 * @property string $id
 * @property string $status
 * @property string $date
 * @property string $link
 * @property string $image
 * @property string $title_ru
 * @property string $meta_title_ru
 * @property string $meta_description_ru
 * @property string $meta_keywords_ru
 * @property string $body_ru
 * @property string $short_body_ru
 * @property string $title_en
 * @property string $meta_title_en
 * @property string $meta_description_en
 * @property string $meta_keywords_en
 * @property string $body_en
 * @property string $short_body_en
 * @property string $title_ro
 * @property string $meta_title_ro
 * @property string $meta_description_ro
 * @property string $meta_keywords_ro
 * @property string $body_ro
 * @property string $short_body_ro
 * @property string $alias
 * @property string $on_menu
 */
class Pages extends CActiveRecord implements ARMetaTagsInterface
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_ru', 'required'),
			array('status', 'length', 'max'=>8),
			array('link, image, alias, h1_ru', 'length', 'max'=>255),
			array('title_ru, meta_title_ru, meta_description_ru, meta_keywords_ru, title_en, meta_title_en, meta_description_en, meta_keywords_en, title_ro, meta_title_ro, meta_description_ro, meta_keywords_ro', 'length', 'max'=>1024),
			array('on_menu', 'length', 'max'=>3),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('image, meta_title_ru, meta_description_ru, meta_keywords_ru, body_ru, short_body_ru, meta_title_en, meta_description_en, meta_keywords_en, body_en, short_body_en, meta_title_ro, meta_description_ro, meta_keywords_ro, body_ro, short_body_ro, alias, on_menu, category', 'safe'),
			array('id, status, date, link, image, title_ru, meta_title_ru, meta_description_ru, meta_keywords_ru, body_ru, short_body_ru, title_en, meta_title_en, meta_description_en, meta_keywords_en, body_en, short_body_en, title_ro, meta_title_ro, meta_description_ro, meta_keywords_ro, body_ro, short_body_ro, alias, on_menu, category', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	public function getTitle(){
	  return $this->{'title_'.Yii::app()->GetLanguage()};
	}
	public function setTitle($value){
	  $this->title = $value;
	}

		public function getBody(){
	  return $this->{'body_'.Yii::app()->GetLanguage()};
	}
	public function setBody($value){
	  $this->body = $value;
	}
	public function getShort_body(){
	  return $this->{'short_body_'.Yii::app()->GetLanguage()};
	}
	public function setShort_body($value){
	  $this->short_body = $value;
	}

	public function getMeta_title(){
	  return $this->{'meta_title_'.Yii::app()->GetLanguage()};
	}
	public function setMeta_title($value){
	  $this->meta_title = $value;
	}
	public function getMeta_description(){
	  return $this->{'meta_description_'.Yii::app()->GetLanguage()};
	}
	public function setMeta_description($value){
	  $this->meta_description = $value;
	}
	public function getMeta_keywords(){
	  return $this->{'meta_keywords_'.Yii::app()->GetLanguage()};
	}
	public function setMeta_keywords($value){
	  $this->meta_keywords = $value;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Status',
			'date' => 'Date',
			'link' => 'Link',
			'image' => 'Image',
			'title_ru' => 'Title Ru',
			'meta_title_ru' => 'Meta Title Ru',
			'meta_description_ru' => 'Meta Description Ru',
			'meta_keywords_ru' => 'Meta Keywords Ru',
			'body_ru' => 'Body Ru',
			'short_body_ru' => 'Short Body Ru',
			'title_en' => 'Title En',
			'meta_title_en' => 'Meta Title En',
			'meta_description_en' => 'Meta Description En',
			'meta_keywords_en' => 'Meta Keywords En',
			'body_en' => 'Body En',
			'short_body_en' => 'Short Body En',
			'title_ro' => 'Title Ro',
			'meta_title_ro' => 'Meta Title Ro',
			'meta_description_ro' => 'Meta Description Ro',
			'meta_keywords_ro' => 'Meta Keywords Ro',
			'body_ro' => 'Body Ro',
			'short_body_ro' => 'Short Body Ro',
			'alias' => 'Alias',
			'on_menu' => 'On Menu',
			'category' => 'Category',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('meta_title_ru',$this->meta_title_ru,true);
		$criteria->compare('meta_description_ru',$this->meta_description_ru,true);
		$criteria->compare('meta_keywords_ru',$this->meta_keywords_ru,true);
		$criteria->compare('body_ru',$this->body_ru,true);
		$criteria->compare('short_body_ru',$this->short_body_ru,true);
		$criteria->compare('title_en',$this->title_en,true);
		$criteria->compare('meta_title_en',$this->meta_title_en,true);
		$criteria->compare('meta_description_en',$this->meta_description_en,true);
		$criteria->compare('meta_keywords_en',$this->meta_keywords_en,true);
		$criteria->compare('body_en',$this->body_en,true);
		$criteria->compare('short_body_en',$this->short_body_en,true);
		$criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('meta_title_ro',$this->meta_title_ro,true);
		$criteria->compare('meta_description_ro',$this->meta_description_ro,true);
		$criteria->compare('meta_keywords_ro',$this->meta_keywords_ro,true);
		$criteria->compare('body_ro',$this->body_ro,true);
		$criteria->compare('short_body_ro',$this->short_body_ro,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('on_menu',$this->on_menu,true);
		$criteria->compare('category',$this->category,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->getMeta_title();
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->getMeta_description();
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->getMeta_keywords();
    }

    /**
     * @return string
     */
    public function getH1()
    {
        return $this->{'h1_'.Yii::app()->GetLanguage()};
    }
}