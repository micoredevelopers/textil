<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 28.11.2018
 * Time: 12:51
 */

interface CASProductInterface
{

    /**
     * @return integer
     */
    public function getProductID();

    /**
     * @return string
     */
    public function getProductTitle();

    /**
     * @return float
     */
    public function getProductPrice();

    /**
     * @return integer
     */
    public function getProductCategoryID();

}