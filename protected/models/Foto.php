<?php

/**
 * This is the model class for table "orientation".
 *
 * The followings are the available columns in table 'orientation':
 * @property integer $id
 * @property string $title_ru
 * @property string $title_ro
 * @property string $title_en
 * @property string $alias
 * @property string $img1
 * @property string $img2
 * @property string $img3
 * @property string $description
 */
class Foto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Orientation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'foto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_ru, title_ro, title_en, alias, img1, img2, img3, link, description', 'safe'),
			array('title_ru, title_ro, title_en, alias,link', 'length', 'max'=>255),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title_ru, title_ro, title_en, alias, description, img1, img2, img3, img4, position,link', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	public function getTitle(){
	  return $this->{'title_'.Yii::app()->GetLanguage()};
	}
	public function setTitle($value){
	  $this->title = $value;
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title_ru' => 'Title Ru',
			'title_ro' => 'Title Ro',
			'title_en' => 'Title En',
			'link' => 'Link',
			'alias' => 'Alias',
			'img1' => 'Img1',
			'img2' => 'Img2',
			'img3' => 'Img3',
			'img4' => 'Img4',
			'position' => 'Position',
            'description' => 'Description'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('title_en',$this->title_en,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('img1',$this->img1,true);
		$criteria->compare('img2',$this->img2,true);
		$criteria->compare('img3',$this->img3,true);
		$criteria->compare('img4',$this->img4,true);
		$criteria->compare('position',$this->position,true);
        $criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}