<?php

/**
 * This is the model class for table "{{users}}".
 *
 * The followings are the available columns in table '{{users}}':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string $details
 * @property string $foto
 * @property string $last_login_time
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 * @property integer $can_news
 * @property integer $can_phones
 * @property integer $can_articles
 * @property integer $admin
 * @property string $status
 */
class User extends CActiveRecord
{
  public $password_repeat;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{users}}';
	}

  protected function beforeValidate(){
    if($this->isNewRecord){
      $this->create_time = $this->update_time = new CDbExpression('NOW()');
      $this->create_user_id = $this->update_user_id = Yii::app()->user->id;
    }else{
      $this->update_time = new CDbExpression('NOW()');
      $this->update_user_id = Yii::app()->user->id;
    }
    return parent::beforeValidate();
  }
  protected function afterValidate(){
    parent::afterValidate();
    if($this->password){
      $this->password = $this->encrypt($this->password); 
    }else{
      unset($this->password);
    }
    
  }
  public function encrypt($value){
    return md5($value);
  }	

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
      array('email', 'required'),
      array('email','unique'),
      array('password', 'compare', 'on'=>'insert'),
      array('password', 'compare', 'on'=>'update'),
      array('password, password_repeat', 'required', 'on'=>'insert'),
      array('create_user_id, update_user_id, admin', 'numerical', 'integerOnly'=>true),
      array('email, password, name', 'length', 'max'=>255),
      array('status', 'length', 'max'=>8),
      array('last_login_time, create_time, update_time, password_repeat', 'safe'),			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, email, password, name, last_login_time, create_time, create_user_id, update_time, update_user_id, admin, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'name' => 'Name ',			
			'last_login_time' => 'Last Login Time',
			'create_time' => 'Create Time',
			'create_user_id' => 'Create User',
			'update_time' => 'Update Time',
			'update_user_id' => 'Update User',			
			'admin' => 'Admin',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('name',$this->name,true);		
		$criteria->compare('last_login_time',$this->last_login_time,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('update_user_id',$this->update_user_id);		
		$criteria->compare('admin',$this->admin);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}