<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 14.12.2018
 * Time: 11:55
 */

class CartListItem
{


    protected $q;

    protected $price;

    protected $item;

    /**
     * @return mixed
     */
    public function getQ()
    {
        return $this->q;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return float|int
     */
    public function getPriceSum()
    {
        return round($this->price * $this->q,2);
    }

    /**
     * @return CartItemInterface
     */
    public function getItem(){
        return $this->item;
    }

    /**
     * CartListItem constructor.
     * @param CartItemInterface $item
     * @param CartListStorageInterface $storage
     */
    public function  __construct(CartItemInterface $item, CartListStorageInterface $storage)
    {
        $this->q = $storage->itemQ($item);
        $this->price = $item->getPrice();
        $this->item = $item;
    }

}