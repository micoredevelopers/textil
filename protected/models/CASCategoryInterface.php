<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 28.11.2018
 * Time: 12:55
 */

interface CASCategoryInterface
{

    /**
     * @return integer
     */
    public function getCategoryID();

    /**
     * @return string
     */
    public function getCategoryTitle();

    /**
     * @param CASProductInterface $product
     * @return mixed
     */
    public function addProduct(CASProductInterface $product);

    /**
     * @return CASProductInterface[]
     */
    public function getProducts();

}