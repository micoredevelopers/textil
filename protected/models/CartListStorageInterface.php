<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 14.12.2018
 * Time: 11:07
 */

interface CartListStorageInterface
{

    /**
     * @param CartItemInterface $item
     * @return integer
     */
    public function itemQ(CartItemInterface $item);

    /**
     * @return array
     */
    public function idList();

}