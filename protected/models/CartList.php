<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 14.12.2018
 * Time: 10:49
 */

class CartList
{


    /**
     * @var  CartListItem[]
     */
    protected $items=[];

    /**
     * @var CartListStorageInterface
     */
    protected $storage;

    public function __construct(CartListStorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return CartListItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return CartListStorageInterface
     */
    public function getStorage(){
       return $this->storage;
    }

    /**
     * @param CartItemInterface $cartItem
     * @return $this
     */
    public function addItem(CartItemInterface $cartItem){
        $this->items[$cartItem->getID()] = new CartListItem($cartItem, $this->getStorage());
        return $this;
    }

    /**
     * @param CartItemInterface[] $items
     * @return $this
     */
    public function addItemList($items=[]){
        if ($items){
            foreach ($items as $cartItem){
                $this->items[$cartItem->getID()] = new CartListItem($cartItem, $this->getStorage());
            }
        }
        return $this;
    }

    /**
     * @param $id
     * @return CartListItem|null
     */
    public function itemByID($id){
        if (isset($this->items[$id])){
           return $this->items[$id];
        }
        return null;
    }

    /**
     * @return int
     */
    public function total(){
        $total = 0;
        if ($items = $this->getItems()){
            foreach ($items as $item){
                $total+=$item->getPriceSum();
            }
        }
        return round($total,2);
    }

    /**
     * @return int
     */
    public function totalUniqueQ(){
        return count($this->getItems());
        $q = 0;
        if ($this->getItems()){
            foreach ($this->getItems() as $item){
                $q += $item->getQ();
            }
        }

        return $q;
    }

}