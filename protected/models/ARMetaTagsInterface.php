<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 13.11.2018
 * Time: 10:04
 */

interface ARMetaTagsInterface
{

    /**
     * @return string
     */
    public function getMetaTitle();

    /**
     * @return string
     */
    public function getMetaDescription();

    /**
     * @return string
     */
    public function getMetaKeywords();

    /**
     * @return string
     */
    public function getH1();

}