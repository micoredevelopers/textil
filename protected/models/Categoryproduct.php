<?php

/**
 * This is the model class for table "categoryproduct".
 *
 * The followings are the available columns in table 'categoryproduct':
 * @property integer $id
 * @property string $title_ru
 * @property string $title_ro
 * @property string $title_en
 * @property string $description_ro
 * @property string $description_ru
 * @property string $description_en
 * @property string $alias
 * @property integer $parent
 * @property integer $status
 */
class Categoryproduct extends CActiveRecord implements ARMetaTagsInterface,BreakCrumbsCategoryInterface
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Categoryproduct the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'categoryproduct';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title_ru, title_ro, title_en, description_ro, description_ru, meta_description_ru, meta_keywords_ru, description_en, alias, parent, status, position,image', 'safe'),
            array('alias', 'unique'),
            array('parent, status', 'numerical', 'integerOnly'=>true),
            array('title_ru, meta_title_ru, title_ro, title_en, alias,image, h1_ru', 'length', 'max'=>255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title_ru, title_ro, title_en, description_ro, description_ru, description_en, alias, parent, status, position,image', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function behaviors(){
        return array( 'CAdvancedArBehavior' => array(
            'class' => 'application.extensions.CAdvancedArBehavior'));
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'produ'=>array(self::HAS_MANY, 'Production', 'category',
//                array(
//                'condition' => 'Production.title_ru ORDER by ASC',
//               // 'params' => array(':type'=>3)
//            )
            ),
        );
    }
    public function getTitle(){
        return $this->{'title_'.Yii::app()->GetLanguage()};
    }
    public function setTitle($value){
        $this->title = $value;
    }
    public function getDescription(){
        return $this->{'description_'.Yii::app()->GetLanguage()};
    }
    public function setDescription($value){
        $this->description = $value;
    }


    public function getMetaTitle(){
        return $this->{'meta_title_'.Yii::app()->GetLanguage()};
    }

    public function getMetaDescription(){
        return $this->{'meta_description_'.Yii::app()->GetLanguage()};
    }

    public function getMetaKeywords(){
        return $this->{'meta_keywords_'.Yii::app()->GetLanguage()};
    }

    public function getH1(){
        return $this->{'h1_'.Yii::app()->GetLanguage()};
    }

    public function getUrl(){
        return Yii::app()->createUrl('catalog/category',['alias' => $this->alias ? $this->alias : $this->id]);
    }

    public function getCASUrl(){
        return Yii::app()->createUrl('catalog/product',['alias' => $this->alias ? $this->alias : $this->id]);
    }

    public function getImagePath(){
        if (!is_file(Yii::getPathOfAlias('root') . '/images/Category/'. $this->image)){
            return Yii::app()->request->getBaseUrl(true) . '/public/static/images/no_name.jpg';
        }
        return Yii::app()->request->getBaseUrl(true) . '/images/Category/'. $this->image;
    }

    /**
     * @return int
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function microMarking()
    {
        if (!$this->produ){
            return [];
        }
        $firstProduct = $this->produ[0];
        return [

            "@type" => "Product",
            "name" => $this->getTitle(),
            "image" => [
                $this->getImagePath()
            ],
            "sku" => $this->id,
            "description" => $this->getDescription(),
            "offers" => [
                "availability" => "inStock",
                "@type" => "Offer",
                "priceCurrency" => "USD",
                "price" =>$firstProduct->price_new,
                "url" => Yii::app()->request->getBaseUrl(true) . $this->getCASUrl(),
            ]

        ];
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title_ru' => 'Title Ru',
            'title_ro' => 'Title Ro',
            'title_en' => 'Title En',
            'description_ro' => 'Description Ro',
            'description_ru' => 'Description Ru',
            'description_en' => 'Description En',
            'alias' => 'Alias',
            'parent' => 'Parent',
            'status' => 'Status',
            'position' => 'Position',
            'image' => 'Image',
        );
    }


    public static function casProducts($criteria=null){
        $instance = static::model()->with(['produ']);
        return $criteria ? $instance->findAll($criteria) : $instance->findAll();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title_ru',$this->title_ru,true);
        $criteria->compare('title_ro',$this->title_ro,true);
        $criteria->compare('title_en',$this->title_en,true);
        $criteria->compare('description_ro',$this->description_ro,true);
        $criteria->compare('description_ru',$this->description_ru,true);
        $criteria->compare('description_en',$this->description_en,true);
        $criteria->compare('alias',$this->alias,true);
        $criteria->compare('image',$this->image,true);
        $criteria->compare('parent',$this->parent);
        $criteria->compare('status',$this->status);
        $criteria->compare('position',$this->position);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

}