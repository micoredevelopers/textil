<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 05.12.2018
 * Time: 13:36
 */

class ARFinder
{
    /**
     * @var CDbCriteria
     */
    protected $criteria;

    protected $pagesInstance;

    /**
     * @var CPagination
     */
    protected $pages;
    /**
     * @var CActiveRecord
     */
    protected $model;

    public function __construct(CActiveRecord $model)
    {
        $this->criteria = new CDbCriteria;
        $this->model = $model;
    }

    /**
     * @param $join
     * @return $this
     */
    public function join($join){
        $this->criteria->join = $join;
        return $this;
    }

    /**
     * @param $limit
     * @return $this
     */
    public function limit($limit)
    {
        $this->criteria->limit = $limit;
        return $this;
    }

    /**
     * @param $order
     * @return $this
     */
    public function order($order)
    {
        $this->criteria->order = $order;
        return $this;
    }

    public function groupBy($groupBy){
        $this->criteria->group = $groupBy;
        return $this;
    }

    /**
     * @param $condition
     * @return $this
     */
    public function condition($condition)
    {
        $this->criteria->condition = $condition;
        return $this;
    }

    /**
     * @param int $size
     * @return $this
     */
    public function pages($size = 10)
    {
        $this->pages = new CPagination((int)$this->count());
        $this->pages -> pageSize = $size;
        $this->pages -> applyLimit($this->criteria);


//        $schema = Yii::app()->db->schema;
//        $builder = $schema->commandBuilder;
//
//        $command = $builder->createFindCommand($schema->getTable('categoryproduct'), $this->criteria);
//        dump($command->text,1);


        return $this;
    }

    /**
     * @return bool|CDbDataReader|mixed|string
     */
    public function count()
    {
        return $this->model->count($this->criteria);
    }

    /**
     * @return CPagination
     */
    public function pagesInstance(){
        return $this->pages;
    }

    /**
     * @return array|Categoryproduct[]|mixed|null
     */
    public function all()
    {
        return $this->model->findAll($this->criteria);
    }

    /**
     * @return array|Categoryproduct|mixed|null
     */
    public function one()
    {
        // $this->model->fi
        return $this->model->find($this->criteria);
    }

    /**
     * @return $this
     */
    public static function find(CActiveRecord $model)
    {
        return new static($model);
    }
}