<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 13.11.2018
 * Time: 15:39
 */

class Routes extends CFormModel
{

    protected $routes;

    public function init()
    {
        parent::init();
        $path = $this->configPath();
        $routes = include $path;
        $this->routes = array_flip($routes);
    }

    public function configPath(){
        return Yii::getPathOfAlias('configDir') . '/user_routes.php';
    }

    /**
     * @return array
     */

    public function getRoutes()
    {
        $arr = [];
        $labels = $this->labels();
        foreach ($this->routes as $route => $url) {
            if (in_array($route, $this->allowed())) {
                $arr[] = [
                    'url' => $url,
                    'route' => $route,
                    'label' => isset($labels[$route]) ? $labels[$route] : $route
                ];
            }
        }
        return $arr;
    }

    /**
     * @param array $routes
     */
    public function setRoutes($routes = [])
    {
         if (is_array($routes) && $routes) {
            foreach ($routes as $route => $url) {
                if (is_string($route) && is_string($url) && in_array($route, $this->allowed())) {
                    $this->routes[$route] = $url;
                }
            }
        }
       // dump($this->routes);
    }

    public function save(){
        $array2string = '<?php '.PHP_EOL.' return ' . var_export(array_flip($this->routes),true).';';
        file_put_contents($this->configPath(), str_replace('\\\\','\\',$array2string));
    }

    /**
     * @return array
     */
    public function allowed()
    {
        return array_keys($this->labels());
    }

    /**
     * @return array
     */
    public function labels()
    {
        return [
            'main/about' => 'О нас',
            'main/addOrder' => 'После оформления заказа',
            'main/sendproduct' => 'Доставка',
            'news/index' => 'Новости/Статьи',
            'main/contact' => 'Контакты',
            'main/partners' => 'Партнеры',
            'product/productnewszakaz' => 'Новые поступления',
            'product/productpodzakaz' => 'Ткани под заказ',
            'product/discount' => 'Распродажа',
            'main/cart' => 'Корзина',
            'main/order' => 'Оформление заказа',
            'main/optom' => 'Оптом',
            'product/search' => 'Поиск',
            'product/productall' => 'Каталог',
        ];
    }

}