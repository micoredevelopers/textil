<?php

/**
 * This is the model class for table "qa".
 *
 * The followings are the available columns in table 'qa':
 * @property integer $id
 * @property string $title_ro
 * @property string $title_ru
 * @property string $title_en
 * @property string $body_ro
 * @property string $body_ru
 * @property string $body_en
 * @property integer $position
 * @property string $date
 */
class Qa extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Qa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'qa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_ro, title_ru, title_en, body_ro, body_ru, body_en, position, date', 'safe'),
			array('position', 'numerical', 'integerOnly'=>true),
			array('title_ro, title_ru, title_en', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title_ro, title_ru, title_en, body_ro, body_ru, body_en, position, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	public function getTitle(){
	  return $this->{'title_'.Yii::app()->GetLanguage()};
	}
	public function setTitle($value){
	  $this->title = $value;
	}
	public function getBody(){
	  return $this->{'body_'.Yii::app()->GetLanguage()};
	}
	public function setBody($value){
	  $this->body = $value;
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title_ro' => 'Title Ro',
			'title_ru' => 'Title Ru',
			'title_en' => 'Title En',
			'body_ro' => 'Body Ro',
			'body_ru' => 'Body Ru',
			'body_en' => 'Body En',
			'position' => 'Position',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('title_en',$this->title_en,true);
		$criteria->compare('body_ro',$this->body_ro,true);
		$criteria->compare('body_ru',$this->body_ru,true);
		$criteria->compare('body_en',$this->body_en,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}