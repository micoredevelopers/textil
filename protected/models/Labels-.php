<?php

/**
 * This is the model class for table "labels".
 *
 * The followings are the available columns in table 'labels':
 * @property integer $id
 * @property string $category
 * @property string $alias
 * @property string $title_ru
 * @property string $title_en
 */
class Labels extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Labels the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'labels';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category, alias, title_ru, title_en, title_ro', 'required'),
			array('category, alias', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category, alias, title_ru, title_en, title_ro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => 'Category',
			'alias' => 'Alias',
			'title_ru' => 'Title Ru',
			'title_en' => 'Title En',
			'title_ro' => 'Title Ro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('title_en',$this->title_en,true);
		$criteria->compare('title_ro',$this->title_en,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	protected function afterSave()
	{
	    parent::afterSave();
	    $documentRoot=Yii::getPathOfAlias('webroot');
	    
	    $labels=Labels::model()->findAll();

	    foreach (Yii::app()->params['languages1'] as $key2 => $value2) {

		    foreach ($labels as $key => $value) {
		    	
			    if (!empty($value->{"title_{$value2}"})) {
			    	$arr[$value2][$value->category][] = ('"'.str_replace('"', '\"', $value->alias).'" => "'.str_replace('"', '\"', $value->{"title_{$value2}"}).'"'); 
			    }else{
			    	$arr[$value2][$value->category][] = ('"'.str_replace('"', '\"', $value->alias).'" => "'.str_replace('"', '\"', $value->alias).'"'); 
			    }

				foreach ($arr[$value2] as $key3 => $value3) {

						$value3 = implode(',', $value3);
					    $text = "<?php
								return array(";
						$text .= $value3;
						$text .= ");
								?>";
						
						$file_path = $documentRoot."/protected/messages/{$value2}/{$key3}.php";
						$dir_path = $documentRoot."/protected/messages/{$value2}/";

						if (!is_dir($dir_path)) {
							mkdir($dir_path);
						}
						file_put_contents($file_path, $text);

				}
	    	}
		}
	}
}