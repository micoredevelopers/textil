<?php

/**
 * This is the model class for table "carousel".
 *
 * The followings are the available columns in table 'carousel':
 * @property integer $id
 * @property string $status
 * @property string $date
 * @property string $image
 * @property string $title_en
 * @property string $title_ru
 * @property string $title_ro
 * @property string $short_body_en
 * @property string $short_body_ru
 * @property string $short_body_ro
 * @property string $alias
 */
class Carousel extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Carousel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'carousel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('title_ru', 'required'),
			array('status', 'length', 'max'=>8),
			array('image, link, title_en, title_ru, title_ro, short_body_en, short_body_ru, short_body_ro, alias', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, status, link, date, image, title_en, title_ru, title_ro, short_body_en, short_body_ru, short_body_ro, alias', 'safe', 'on'=>'search'),
			array('id, status, link, date, image, title_en, title_ru, title_ro, short_body_en, short_body_ru, short_body_ro, alias, meta_description_ru, meta_description_en, meta_description_ro', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
public function getTitle(){
	  return $this->{'title_'.Yii::app()->GetLanguage()};
	}
	public function setTitle($value){
	  $this->title = $value;
	}
	// public function getLink(){
	//   return $this->{'link_'.Yii::app()->GetLanguage()};
	// }
	// public function setLink($value){
	//   $this->link = $value;
	// }
	public function getShort_body(){
	  return $this->{'short_body_'.Yii::app()->GetLanguage()};
	}
	public function setShort_body($value){
	  $this->short_body = $value;
	}
	public function getMeta_description(){
	  return $this->{'meta_description_'.Yii::app()->GetLanguage()};
	}
	public function setMeta_description($value){
	  $this->meta_description = $value;
	}
	public function getMeta_keywords(){
	  return $this->{'meta_keywords_'.Yii::app()->GetLanguage()};
	}
	public function setMeta_keywords($value){
	  $this->meta_keywords = $value;
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Status',
			'link' => 'Link',
			'date' => 'Date',
			'image' => 'Image',
			'title_en' => 'Title En',
			'title_ru' => 'Title Ru',
			'title_ro' => 'Title Ro',
			'short_body_en' => 'Short Body En',
			'short_body_ru' => 'Short Body Ru',
			'short_body_ro' => 'Short Body Ro',
			'alias' => 'Alias',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('title_en',$this->title_en,true);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('short_body_en',$this->short_body_en,true);
		$criteria->compare('short_body_ru',$this->short_body_ru,true);
		$criteria->compare('short_body_ro',$this->short_body_ro,true);
		$criteria->compare('alias',$this->alias,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}