<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 29.11.2018
 * Time: 15:20
 */

interface CartStorageInterface
{

    /**
     * @param CartItemInterface $item
     * @return mixed
     */
    public function saveItem(CartItemInterface $item);

    /**
     * @param CartItemInterface $item
     * @return mixed
     */
    public function itemQ(CartItemInterface $item);
    /**
     * @param CartItemInterface $item
     * @return mixed
     */
    public function itemExist (CartItemInterface $item);

    /**
     * @param CartItemInterface $item
     * @return mixed
     */
    public function deleteItem (CartItemInterface $item);

    /**
     * @return mixed
     */
    public function removeItems();

}