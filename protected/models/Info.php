<?php

/**
 * This is the model class for table "info".
 *
 * The followings are the available columns in table 'info':
 * @property integer $id
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_ro
 * @property string $image
 * @property integer $position
 * @property string $body_ru
 * @property string $body_en
 * @property string $body_ro
 * @property string $meta_title_ru
 * @property string $meta_title_ro
 * @property string $meta_title_en
 * @property string $meta_description_ru
 * @property string $meta_description_ro
 * @property string $meta_description_en
 * @property string $meta_keyword_ru
 * @property string $meta_keyword_ro
 * @property string $meta_keyword_en
 * @property string $alias
 * @property string $created
 */
class Info extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Info the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_ru, title_en, title_ro, image, position, body_ru, body_en, body_ro, meta_title_ru, meta_title_ro, meta_title_en, meta_description_ru, meta_description_ro, meta_description_en, meta_keyword_ru, meta_keyword_ro, meta_keyword_en, alias, created', 'safe'),
			array('position', 'numerical', 'integerOnly'=>true),
			array('title_ru, title_en, title_ro, image, meta_title_ru, meta_title_ro, meta_title_en, meta_keyword_ru, meta_keyword_ro, meta_keyword_en, alias', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title_ru, title_en, title_ro, image, position, body_ru, body_en, body_ro, meta_title_ru, meta_title_ro, meta_title_en, meta_description_ru, meta_description_ro, meta_description_en, meta_keyword_ru, meta_keyword_ro, meta_keyword_en, alias, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	public function getTitle(){
	  return $this->{'title_'.Yii::app()->GetLanguage()};
	}
	public function setTitle($value){
	  $this->title = $value;
	}

		public function getBody(){
	  return $this->{'body_'.Yii::app()->GetLanguage()};
	}
	public function setBody($value){
	  $this->body = $value;
	}

	public function getMeta_title(){
	  return $this->{'meta_title_'.Yii::app()->GetLanguage()};
	}
	public function setMeta_title($value){
	  $this->meta_title = $value;
	}
	public function getMeta_description(){
	  return $this->{'meta_description_'.Yii::app()->GetLanguage()};
	}
	public function setMeta_description($value){
	  $this->meta_description = $value;
	}
	public function getMeta_keyword(){
	  return $this->{'meta_keyword_'.Yii::app()->GetLanguage()};
	}
	public function setMeta_keyword($value){
	  $this->meta_keyword = $value;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title_ru' => 'Title Ru',
			'title_en' => 'Title En',
			'title_ro' => 'Title Ro',
			'image' => 'Image',
			'position' => 'Position',
			'body_ru' => 'Body Ru',
			'body_en' => 'Body En',
			'body_ro' => 'Body Ro',
			'meta_title_ru' => 'Meta Title Ru',
			'meta_title_ro' => 'Meta Title Ro',
			'meta_title_en' => 'Meta Title En',
			'meta_description_ru' => 'Meta Description Ru',
			'meta_description_ro' => 'Meta Description Ro',
			'meta_description_en' => 'Meta Description En',
			'meta_keyword_ru' => 'Meta Keyword Ru',
			'meta_keyword_ro' => 'Meta Keyword Ro',
			'meta_keyword_en' => 'Meta Keyword En',
			'alias' => 'Alias',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('title_en',$this->title_en,true);
		$criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('body_ru',$this->body_ru,true);
		$criteria->compare('body_en',$this->body_en,true);
		$criteria->compare('body_ro',$this->body_ro,true);
		$criteria->compare('meta_title_ru',$this->meta_title_ru,true);
		$criteria->compare('meta_title_ro',$this->meta_title_ro,true);
		$criteria->compare('meta_title_en',$this->meta_title_en,true);
		$criteria->compare('meta_description_ru',$this->meta_description_ru,true);
		$criteria->compare('meta_description_ro',$this->meta_description_ro,true);
		$criteria->compare('meta_description_en',$this->meta_description_en,true);
		$criteria->compare('meta_keyword_ru',$this->meta_keyword_ru,true);
		$criteria->compare('meta_keyword_ro',$this->meta_keyword_ro,true);
		$criteria->compare('meta_keyword_en',$this->meta_keyword_en,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}