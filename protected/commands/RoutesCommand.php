<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 29.11.2018
 * Time: 09:54
 */

class RoutesCommand extends CConsoleCommand
{

    public function actionIndex($url=null) {

        $config = [
            Yii::app()->basePath.'/config/routes.php',
            Yii::app()->basePath.'/config/user_routes.php'
        ];
        $configArr = [];
        foreach ($config as $path){

            $configArr = array_merge($configArr, include $path);
        }

        foreach ($configArr as $k=>$item){
            if (($url && strstr($k,$url)) || !$url) {
                echo $k . ' --> ' . $item . PHP_EOL;
            }
        }
        return 0;

    }

}