<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
Yii::setPathOfAlias('editable', dirname(__FILE__).'/../extensions/x-editable');

return array(
   'timeZone' => 'Europe/Bucharest',
  'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
  'theme'=>'bootstrap',
  'name'=>'fiisanatos.md',
  
  // preloading 'log' component
  'preload'=>array('log'),
  // user language (for Locale)
  'language'=>'ro', //$_SERVER['SERVER_NAME']=='engsmpress.istoria.md' ? 'en' : 
  
  //language for messages and views
  'sourceLanguage'=>'fr',
  // autoloading model and component classes
  'import'=>array(
    'application.models.*',
    'application.components.*',
    'application.extensions.upload.Upload',
    'application.extensions.Time',
    // 'application.extensions.T',
    'editable.*',
  ),

  

  'modules'=>array(
    // uncomment the following to enable the Gii tool
    'gii'=>array(
      'class'=>'system.gii.GiiModule',
     'generatorPaths'=>array(
        'bootstrap.gii',
      ),

      'password'=>'ussdsms',
      // If removed, Gii defaults to localhost only. Edit carefully to taste.
      'ipFilters'=>array(
        '89.28.66.222',
        '95.65.124.146',
        '212.56.194.134',
        //'::1'
      ),
    ),
    'admin',
    
  ),

  // application components
  'components'=>array(
    'phpThumb'=>array(
    'class'=>'ext.EPhpThumb.EPhpThumb',
    ),
    'messages' => array(
      'onMissingTranslation' => array('T','t1'),
      // 'class' => 'CDbMessageSource',
      // 'sourceMessageTable' => 'SourceMessage',
      // 'translatedMessageTable' => 'Message'
      // config for db message source here, see http://www.yiiframework.com/doc/api/CDbMessageSource
      ),
    'editable' => array(
      'class'     => 'editable.EditableConfig',
      'form'      => 'bootstrap',        //form style: 'bootstrap', 'jqueryui', 'plain' 
      'mode'      => 'popup',            //mode: 'popup' or 'inline'  
      'defaults'  => array(              //default settings for all editable elements
         'emptytext' => 'Click to edit'
        )
    ), 
    'request' => array(
       'class' => 'HttpRequest',
       'enableCsrfValidation' => false,
    ),
    'bootstrap'=>array(
      'class'=>'bootstrap.components.Bootstrap',
    ),
    // 't'=>array(
    //   'class'=>'T',
    // ),
    'user'=>array(
      'class' => 'WebUser',
      // enable cookie-based authentication
      'allowAutoLogin'=>true,
    ),
    // uncomment the following to enable URLs in path-format
    
    'urlManager'=>array(
        'class'=>'application.components.UrlManager',
      'urlFormat'=>'path',
      'showScriptName'=>false,
      'rules'=>array(
        '' => 'main/index', 
        '<lang:(ru|ro)>' => 'main/index', 
        // 'about' => 'main/about',
        'newsitem/<alias:.*>.html' => 'news/item',

        'articlesitem/<alias:.*>.html' => 'articles/item',
        'newscategory/<alias:.*>.html' => 'news/ByCatAlias',
        'articlescategory/<alias:.*>.html' => 'articles/AllArticlesByCatAlias',
        '<lang:(ru|ro)>/portfolio.html' => 'portfolio',
        '<lang:(ru|ro)>/domenii.html' => 'portfolio/index',
        '<lang:(ru|ro)>/portfolio/<id:\d+>.html'=>'portfolio/view',
        '<lang:(ru|ro)>/service.html' => 'service',
        '<lang:(ru|ro)>/service/<id:\d+>.html'=>'service/view',
        '<lang:(ru|ro)>/staff.html' => 'staff',
        '<lang:(ru|ro)>/ads.html' => 'ads/index',
        '<lang:(ru|ro)>/ads/<id:\d+>.html'=>'ads/view',
        '<lang:(ru|ro)>/curs.html' => 'curs/index',
        '<lang:(ru|ro)>/curs_categ.html' => 'curs/categ',
        '<lang:(ru|ro)>/curs_categ_video.html' => 'curs/categ_video',
        '<lang:(ru|ro)>/curs/<id:\d+>.html'=>'curs/view',
        '<lang:(ru|ro)>/curs_video.html' => 'curs/video',
        '<lang:(ru|ro)>/curs_video/<id:\d+>.html'=>'curs/view_video',

        '<lang:(ru|ro)>/materials.html' => 'main/autorizare',
        '<lang:(ru|ro)>/blog.html' => 'blog',
        '<lang:(ru|ro)>/blog/<id:\d+>.html'=>'blog/view',
        '<lang:(ru|ro)>/about.html' => 'main/about',
        '<lang:(ru|ro)>/activity.html' => 'main/activity',
     
        '<lang:(ru|ro)>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>/<id>',
        '<lang:(ru|ro)>/<controller:\w+>/<action:\w+>'=>'<controller>/<action>',  
        '<lang:(ru|ro)>/<controller:\w+>'=>'<controller>',  

        //'/en/main/'=>'',

        '<controller:\w+>'=>'<controller>',  
        '<controller:\w+>/<id:\d+>'=>'<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
        '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
  
        
      ),
    ),
    
#   'db'=>array(
#     'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
#   ),
    // uncomment the following to use a MySQL database
    
    'db'=>array(
       'connectionString' => 'mysql:host=localhost;dbname=fiisanat_site',
          'emulatePrepare' => true,
          'username' => 'fiisanat_site',
          'password' => '1*ka!o_9Gf}a',
          'charset' => 'utf8',
          'tablePrefix'=>'fiisanatos_',
          'enableProfiling' =>YII_DEBUG_PROFILING,
    ),
    
    'errorHandler'=>array(
      // use 'site/error' action to display errors
      'errorAction'=>'site/error',
    ),
    'langArr'=>array(
      // use 'site/error' action to display errors
      'ru'=>'fiisanatos.md/ru',
      'en'=>'fiisanatos.md/en',
      'ro'=>'fiisanatos.md/ro',
    ),
    'log'=>array(
      'class'=>'CLogRouter',
      'enabled'=>YII_DEBUG,
      'routes'=>array(
          #...
          array(
              'class'=>'CFileLogRoute',
              'levels'=>'error, warning',
          ),
          // array(
          //     'class'=>'application.extensions.yii-debug-toolbar.YiiDebugToolbarRoute',
          //     'ipFilters'=>array('95.65.124.146'),
          // ),
      ),
    ),
  ),

  // application-level parameters that can be accessed
  // using Yii::app()->params['paramName']
  'params'=>array(
    'languages'=>array('ro'=>'RO', 'ru'=>'RU'),
    // this is used in contact page
    'Email2'=>'superdev@web-studio.md',
    'months'=>array("ian","feb","mar","apr","mai","iun","iul","aug","sep","oct","noie","dec"),
    'statuses' => array('active'=>'Activ','inactive'=>'Inactiv','trash'=>'Trash'),
    'user_statuses' => array('active'=>'Activ','inactive'=>'Inactiv'),
    'banners' => array('active'=>'Active','inactive'=>'Inactive'),
     'status' => array('active'=>'Active','inactive'=>'Inactive'),
     'sex' => array('feminin'=>'Feminin','masculin'=>'Masculin'),
     'of_home' => array('1'=>'Yes','0'=>'No'),
     'type' => array(''=>'Alege tipul','news'=>'Noutate','even'=>'Eveniment'),
     'type_stii' => array('nu'=>'Nu','da'=>'Da'),
     'type_curs' => array('curs'=>'Curs','video'=>'Curs Video'),
     'position' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7'=>'7', '8'=>'8', '9'=>'9', '10'=>'10', '11'=>'11', '12'=>'12', '13'=>'13', '14'=>'14', '15'=>'15'),
     'site' => array('1' => 'Yes', '0' => 'No'),
     'gdesign' => array('1' => 'Yes', '0' => 'No'),
     'web' => array('1' => 'Yes', '0' => 'No'),
     'languages1'=>array('ru'=>'ru', 'ro'=>'ro'),
    'per_page_news'=>10,
    'listPerPage'=>12,
    'listPerPage_curs'=>10,
     'type_users' => array('medic'=>'Medic','user'=>'User'),
  ),
);
