<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');
Yii::setPathOfAlias('editable', dirname(__FILE__) . '/../extensions/x-editable');
Yii::setPathOfAlias('configDir', dirname(__FILE__));
Yii::setPathOfAlias('root', dirname(__FILE__).'/../..');
$routes = array_merge(include dirname(__FILE__).'/user_routes.php',include dirname(__FILE__).'/routes.php');
//$db = array(
//
//    'connectionString' => 'mysql:host=127.0.0.1;dbname=textil',
//    'emulatePrepare' => true,
//    'username' => 'root',
//    'password' => 'q27031605w',
//    'charset' => 'utf8',
//    'tablePrefix' => '',
//);

$db = include_once 'db.php';

return array(
    'timeZone' => 'Europe/Bucharest',
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'theme' => 'bootstrap',
    'name' => 'TEXTIL',

    // preloading 'log' component
    'preload' => array('log', 'redirects'),
    // user language (for Locale)
  //  'language' => 'ru',

    //language for messages and views
    'sourceLanguage' => 'fr',
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.upload.Upload',
        'application.extensions.Time',
        'application.extensions.liqpay.LiqPay',
        'application.extensions.select2.Select2',
        // 'application.extensions.T',
        'editable.*',
    ),


    'modules' => array(
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'generatorPaths' => array(
                'bootstrap.gii',
            ),

            'password' => 'ussdsms',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array(
                '*',
                // '127.0.0.1',
                // '31.22.4.23',
                // '89.28.66.222',
                // '95.65.124.146',
                // '212.56.194.134',
                // '192.168.0.116'
                // '::1'
            ),
        ),
        'admin',

    ),

    // application components
    'components' => array(
        'ECommerce' => array(
            'class' => 'ECommerce'
        ),
        'redirects' => array(
            'class' => 'Redirects'
        ),
        'noIndexRoutes' => array(
            'class' => 'NoIndexRoutes'
        ),

        'microMarking' => array(
            'class' => 'MicroMarking'
        ),

        'canonical' => array(
            'class' => 'Canonical'
        ),
        'breakCrumbs' => array (
            'class' => 'BreakCrumbs'
        ),
        'metaTags' => array (
            'class' => 'MetaTags'
        ),
        'phpThumb' => array(
            'class' => 'ext.EPhpThumb.EPhpThumb',
        ),
        'messages' => array(
            'onMissingTranslation' => array('T', 't1'),
            // 'class' => 'CDbMessageSource',
            // 'sourceMessageTable' => 'SourceMessage',
            // 'translatedMessageTable' => 'Message'
            // config for db message source here, see http://www.yiiframework.com/doc/api/CDbMessageSource
        ),
        'editable' => array(
            'class' => 'editable.EditableConfig',
            'form' => 'bootstrap',        //form style: 'bootstrap', 'jqueryui', 'plain'
            'mode' => 'popup',            //mode: 'popup' or 'inline'
            'defaults' => array(              //default settings for all editable elements
                'emptytext' => 'Click to edit'
            )
        ),
        'request' => array(
            'class' => 'HttpRequest',
            'enableCsrfValidation' => false,
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
        't' => array(
            'class' => 'T',
        ),
        'user' => array(
            'class' => 'WebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
//        'session' => array (
//            'autoStart' => true,
//            'class' => 'CHttpSession'
//        ),
        'urlManager' => array(
            'class' => 'application.components.UrlManager',
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => $routes,
            'urlSuffix'=>'/',
        ),


        'db' => $db,

        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
//        'langArr' => array(
//            // use 'site/error' action to display errors
//            'ru', 'ro', 'en',
//
//        ),
        'log' => array(
            'class' => 'CLogRouter',
            'enabled' => YII_DEBUG,
            'routes' => array(
                #...
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // array(
                //     'class'=>'application.extensions.yii-debug-toolbar.YiiDebugToolbarRoute',
                //     'ipFilters'=>array('95.65.124.146'),
                // ),
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        //'adminEmail'=>'',
        'adminEmail' => 'textile-international@ukr.net',
        // 'months'=>array("ian","feb","mar","apr","mai","iun","iul","aug","sep","oct","noie","dec"),
        'status' => array('active' => 'Activ', 'inactive' => 'Inactiv'),
        'on_menu' => array('yes' => 'Yes', 'no' => 'No'),
        // 'orderStatus' => array('new'=>'New','hold'=>'Hold','confirmed'=>'Confirmed','canceled'=>'Canceled'),
        'user_statuses' => array('active' => 'Activ', 'inactive' => 'Inactiv'),
        'cat_status' => array('1' => 'Activ', '0' => 'Inactiv'),
        // 'widget_lang' => array('ru'=>'Русский','en'=>'English'),
        'categorys' => array('0' => 'Решения', '1' => 'Изделья', '2' => 'Продукция'),
        'languages' => array('ru' => 'Рус', 'ro' => 'Rom', 'en' => 'Eng'),
        'languages1' => array('ru' => 'ru', 'ro' => 'ro', 'en' => 'en'),
        // 'per_page_news'=>10,
    ),
);