<?php
return array(
    '' => 'main/index',

    'product/<alias:.*>' => 'catalog/product',
    'products-cat/<alias:.*>' => 'catalog/category',

    'send_email' => 'send_email/index',
    'articol-item/<id:.*>' => 'news/view',
    'sitemap.xml' => 'sitemap/xml',

    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
    '<controller:\w+>' => '<controller>',
    '<controller:\w+>/<id:\d+>' => '<controller>/view',
    '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
);

//bengalin--kozha
