<?php


return [
    'about' => 'pages/about',
    'contact' => 'pages/contact',
    //'partners' => 'pages/partners',
    'delivery' => 'pages/delivery',
   // 'podzakaz' => 'pages/under_order',
   // 'optom' => 'pages/wholesale',

  //  'thanks' => 'order/completed',
  //  'order' => 'order/index',
    'cart' => 'cart/index',

   // 'newstock' => 'catalog/new_products',
   // 'discount' => 'catalog/discount',
    'search' => 'catalog/search',
    'products' => 'catalog/index',

   // 'news' => 'news/index',

];