
<?php

Yii::app()->breakCrumbs->setCrumbsByPage(new BreakCrumbsMainPage());
Yii::app()->breakCrumbs->addCrumbsByArray([['label' => 'Корзина', 'url' =>'']]);

$this->breadcrumbs = Yii::app()->breakCrumbs->linksToWidget();
$this->params['scripts'] = ['/public/static/js/cart.js'];

?>

<section class="ourMagazine products-cart-page">
    <div class="container">
        <!-- <h1 class="title">Оформление заказа</h1> -->

        <div class="col-md-12 cart-inner">

            <?php if ($items) : ?>
                <div class="col-md-6 order-products if-cart-is-not-empty">
                    <span class="title">Детали покупки</span>

                    <div class="details-header">
                        <span class="line"></span>
                        <div class="headerTitles">
                            <div class="gTitle">Товар</div>
                            <div class="gPrice">Цена</div>
                        </div>
                        <span class="line"></span>
                    </div>
                    <?php foreach ($items as $item) : ?>
                        <div class="cart-product" data-product_cart="<?=$item->getItem()->getID()?>">
                            <div class="product_info">
                                <img src="<?=$item->getItem()->getImg()?>" alt="<?=$item->getItem()->getTitle()?>"/>
                                <span><?=$item->getItem()->getTitle()?></span>
                            </div>

                            <div class="productInfoCart">
                                <div class="calculate">
                                    <button class="minus"  onclick="changeQCart(<?=$item->getItem()->getID()?>,-1)"></button>
                                    <span class="itemCount"><b data-product_cart="q"><?=$item->getQ()?></b>рул.</span>
                                    <button class="plus" onclick="changeQCart(<?=$item->getItem()->getID()?>,1)"></button>
                                </div>
                                <span class="price"><span data-product_cart="price"><?=$item->getPriceSum()?></span>$</span>
                                <i class="removeItemIcon delete-from-cart" onclick="deleteFromCart(<?=$item->getItem()->getID()?>)"></i>
                            </div>
                        </div>
                    <?php endforeach; ?>
	                <div class="sum_all">
		                <p class="sum_text">
			                Итого: <span class="price-total-cart"> <?=$total?></span>$
		                </p>
	                </div>
                </div>

                <div class="col-md-6 buyer-info if-cart-is-not-empty">
                    <span class="title">Информация о доставке</span>

                    <input  type="text" placeholder="Фио*" data-type="username"/>
                    <input type="text" placeholder="Телефон*" data-type="phone"/>
                    <input type="email" placeholder="Почта" data-type="email"/>
                    <input type="text" placeholder="Город" data-type="city"/>
                    <textarea placeholder="Комментарий" data-type="comment"></textarea>

                    <div class="order-buttons">
                        <a href="<?=$this->createUrl('catalog/index')?>" class="backButton">К покупкам</a>
                        <button class="buyButton" type="submit">Оформить заказ</button>
                    </div>
                </div>
            <?php endif; ?>
            <h2 class="if-cart-is-empty" <?=$items ? 'style="display:none"':''?>>Нет товаров добавленных в корзину</h2>

        </div>
    </div>
</section>

<div class="feedModal modal fade" id="order-save-success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Спасибо за Заказ!</h5>
            </div>
            <div class="modal-body">
                Мы перезвоним вам в ближайшее время
            </div>
            <div class="modal-footer">
                <a href="/"><span>Окей</span></a>
            </div>
        </div>
    </div>
</div>