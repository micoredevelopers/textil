<?php

Yii::app()->breakCrumbs->setCrumbsByPage(new BreakCrumbsMainPage());
Yii::app()->breakCrumbs->addCrumbsByArray([['label' => 'Каталог тканей', 'url' => $this->createUrl('catalog/index')]]);
Yii::app()->breakCrumbs->addCrumbsByPage(new BreakCrumbsCategory($categoryInfo));
Yii::app()->microMarking->addMarking($categoryInfo->microMarking());
$this->breadcrumbs = Yii::app()->breakCrumbs->linksToWidget();
$this->params['scripts'] = ['/public/static/js/product.js'];
$this->params['categories'] = $allParentsCategories;

?>

    <section class="product-page">
        <div class="container">
            <div class="col-md-12 product-inner">
                <div class="col-md-6 col-sm-12 col-xs-12 productImageSlider">
                    <div id="productCarousel" class="carousel slide" data-interval="false">
	                    <div class="slider-for carousel-inner">
                            <?php foreach ($categoryInfo->produ as $k => $product) : ?>
			                    <div class="slick-slide <?= $k == 0 ? 'slick-active ' : '' ?>item carousel-item"
			                         data-slide-number="<?= $k ?>" data-id="<?= $product->id ?>"
			                         data-title="<?=$product->getTitle()?> <?=$product->getColorDescription() ? '('.$product->getColorDescription().')' : ''?>" data-options="<?=$product->articul?>"
			                         data-id="<?=$product->id?>"
                                     data-price="<?=$product->price_new?>$ <?=$product->getUnit()['name']?>"
                                     data-price_old = "<?=$product->price_new < $product->price_old ? $product->price_old.'$ '.$product->getUnit()['name'] : ''?>"
			                         data-stock="<?=$product->in_stoc == 0 ? 'Ожидается' : 'В наличии'?>">
				                    <img src="<?= $product->getImagePath() ?>" class="img-fluid" alt="<?= $product->getTitle() ?>">
			                    </div>
                            <?php endforeach; ?>
	                    </div>
	                    <div class="slider-nav thumbs" data-slider_selector="#productCarousel">
                            <?php foreach ($categoryInfo->produ as $k => $product) : ?>
			                    <div class="slick-slide carousel-indicators list-inline">
				                    <div class="list-inline-item<?= $k == 0 ? ' slick-active' : '' ?>">
					                    <a class="selected" data-slide-to="<?= $k ?>" data-target="#productCarousel">
						                    <img src="<?= $product->getImagePath() ?>" class="img-fluid" alt="<?= $product->getTitle() ?>">
					                    </a>
				                    </div>
			                    </div>
                            <?php endforeach; ?>
	                    </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12 product-stats">
                    <h1 class="product-title"><?= Yii::app()->metaTags->getH1(); ?></h1>
                    <div class="product-info">
                        <span class="article">Артикул - <span><?= $categoryInfo->produ[0]->getTitle() ?> <?=$categoryInfo->produ[0]->getColorDescription() ? '('.$categoryInfo->produ[0]->getColorDescription().')' : ''?></span></span>
                        <s><span class="price-old"><?=$categoryInfo->produ[0]->price_new < $categoryInfo->produ[0]->price_old ? $categoryInfo->produ[0]->price_old.'$ '.$categoryInfo->produ[0]->getUnit()['name'] : ''?></span></s>
                        <span class="price"><?= $categoryInfo->produ[0]->price_new ?>$ <?=$categoryInfo->produ[0]->getUnit()['name']?></span>
                    </div>

                    <p>
                        <span class="product-options"><?= $categoryInfo->produ[0]->articul ?></span>
                    </p>

                    <p>
                        <?= $categoryInfo->produ[0]->getDescription() ?>
                    </p>

                    <div class="product-actions status d-block d-xl-none">
                        <span><?=$product->in_stoc == 0 ? 'Ожидается' : 'В наличии'?></span>
                    </div>

                    <div class="calculate d-none d-xl-flex">
                            <button class="minus" onclick="productItemCounter(this)"></button>
                            <span class="itemCount"><b>1</b>рул.</span>
                            <button class="plus" onclick="productItemCounter(this)"></button>
                        </div>

                    <div class="product-actions">
                        <div class="btn_container">
                            <button class="add-to-cart" data-slider_selector="#productCarousel" data-count_selector=".calculate .itemCount b">Купить</button>
                            <span class="d-none d-xl-block"><?=$product->in_stoc == 0 ? 'Ожидается' : 'В наличии'?></span>
                        </div>
                        <div class="calculate d-flex d-xl-none">
                            <button class="minus" onclick="productItemCounter(this)"></button>
                            <span class="itemCount"><b>1</b>рул.</span>
                            <button class="plus" onclick="productItemCounter(this)"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content deliveryPage" style="background-color: #f9f9f9;">
        <div class="container">
            <div class="title">Доставка и оплата</div>

            <div class="col-md-12 deliveryPageInfo row">
                <div class="col-md-6">
                    <h3 class="title delMethodss">
                        <i></i> Способы доставки
                    </h3>

                    <p>
                        Самовывоз<br>
                        Отправка компанией "Новая Пошта" (на следующий день после получения оплаты)<br>
                        Отправка компанией "Интайм" (на следующий день после получения оплаты)<br><br>

                        Любая другая удобная для Вас компания-перевозчик<br>
                    </p>
                </div>

                <div class="col-md-6">
                    <h3 class="title payment">
                        <i></i> Оплата
                    </h3>

                    <p>
                        Произвести оплату можно в любом отделении Приват Банка: в кассе, через терминал
                        самообслуживания, или через систему Privat24. <br>
                        При этом в зависимости от типа операции взимается комиссия банка от 0,5% до 1%. <br> <br>

                        Мы будем благодарны, если после перевода денег Вы сообщите нам об оплате по телефону. <br>
                        Так Вы существенно ускорите оформление Вашего заказа и отгрузку заказанного товара.
                    </p>
                </div>
            </div>

            <div class="col-md-12 returnProduct pb-5">
                <h3 class="title">
                    <i></i> Возврат товара
                </h3>

                <p>
                    Согласно законодательства Украины, возврат или обмен тканей возможен только при обнаружении
                    производственных дефектов в течение 14 дней с момента приобретения (На основании Перечня товаров
                    надлежащего качества, которые не подлежат обмену (возврату) (приложение № 3 к Постановлению Кабинета
                    Министров Украины от 19.03.94 г. № 172) ткани возврату или обмену не подлежат).<br><br>

                    В посылке при отправке обязательно должна присутствовать записка с указанием причины возврата или
                    обмена.<br><br>

                    Ткань должна быть отправлена: в изначальной упаковке, с прикрепленной биркой, без каких-либо
                    повреждений, характерных запахов и признаков, нарушающих изначальный товарный вид (в соответствии с
                    требованием, предусмотренным ст.9 Закона (Постановление КМУ от 19 марта 1994г. №172 должен быть
                    сохранен товарный вид, ярлыки, пломбы)).<br>
                </p>
            </div>
        </div>
    </section>

<?php if ($newProducts) : ?>
    <section class="content">
        <div class="container">
            <div class="title">Новинки</div>

            <div class="col-md-12 row">
                <?php foreach ($newProducts as $categoryProduct) : ?>
                    <?php $this->widget('application.widgets.ProductItem', ['categoryProduct' => $categoryProduct]); ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>