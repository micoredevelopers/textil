<?php

$this->breadcrumbs = null;

?>

<section class="goods-catalog-header">
    <div class="col-md-12 goods-catalog-inner">
        <div class="col-md-5 lefSide">
           <?= $pageInfo->getBody(); ?>
            <a class="ourMagazineButton" href="<?= $this->createUrl('pages/contact') ?>">Как найти наш магазин?</a>
        </div>
        <div class="col-md-7 rightSide">
            <img src="/public/static/images/catalog-header-image.jpg" alt="">
        </div>
    </div>
</section>
<section class="catalogBread breadCrumbs">
    <div class="container">
        <ul>
            <li><a href="<?= $this->createUrl('main/index') ?>">Ткани оптом</a></li>
            <li><a href="">Каталог</a></li>
        </ul>
    </div>
</section>
<section class="goods-catalog">
    <div class="container">
        <h1 class="big-title">
            <?= Yii::app()->metaTags->getH1() ?>
        </h1>
        <?php if ($categories) : ?>
            <div class="row m-0">
                <?php foreach ($categories as $item) : ?>
                    <div class="col-md-3 catalogGoods">
                        <a href="<?= $item->getUrl() ?>">
                            <img src="<?= $item->getImagePath() ?>" alt="<?= $item->getTitle() ?>"/>

                            <div class="catGoodInfo">
                                <?= $item->getTitle() ?>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>

            </div>
        <?php endif; ?>
    </div>
</section>