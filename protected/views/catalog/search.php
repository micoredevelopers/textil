
<section class="goods-search">
    <div class="container">
        <form method="get" action="<?=$this->createUrl('catalog/search')?>">
            <input value="<?=$searchString?>" type="search" name="q" placeholder="Костюмная ткань Демисезонная" />
            <button class="searchButton" type="submit">Искать</button>
        </form>
    </div>
</section>

<section class="content searchPage">
    <div class="container">
        <div class="title">Результаты поиска по запросу:<br/>
            <?=$searchString?></div>

        <div class="row col-md-12">
            <?php if ($categories) :?>
            <?php foreach ($categories as $categoryProduct) : ?>
                <?php $this->widget('application.widgets.ProductItem', ['categoryProduct' => $categoryProduct]); ?>
            <?php endforeach; ?>
            <?php else :?>
        <div class="content">
            <h2>Результатов не найдено!</h2>
        </div>
            <?php endif; ?>
        </div>
        <?php if ($pageLinks) :?>
        <div class="pagination_container">
            <?php if ($previousUrl) :?>
            <a href="<?=$previousUrl?>" class="pag_arrow left"></a>
            <?php endif; ?>
            <ul class="pagination_row">
                <?php foreach ($pageLinks as $page) :?>
                <li<?=$page['isCurrent'] ? ' class="active"' : ''?>>
                    <a <?=$page['url'] ? 'href="'.$page['url'].'"' : '';?> class="page_num"><?=$page['num'];?></a>
                </li>
               <?php endforeach; ?>
            </ul>
            <?php if ($nextUrl) :?>
            <a href="<?=$nextUrl?>" class="pag_arrow right"></a>
            <?php endif; ?>
        </div>
        <?php endif; ?>
    </div>
</section>