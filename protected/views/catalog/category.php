<?php

    Yii::app()->breakCrumbs->setCrumbsByPage(new BreakCrumbsMainPage());
    Yii::app()->breakCrumbs->addCrumbsByArray([['label'=>'Каталог тканей','url'=>$this->createUrl('catalog/index')]]);
    Yii::app()->breakCrumbs->addCrumbsByPage(new BreakCrumbsCategory($category));
    $this->breadcrumbs = Yii::app()->breakCrumbs->linksToWidget();
    $this->params['scripts'] = ['/public/static/js/categoryProducts.js'];
    $this->params['categories'] = $allParentsCategories;
?>


<section class="content category">
    <div class="container">
        <h1 class="title"><?=Yii::app()->metaTags->getH1()?></h1>
        <button class="filters" data-toggle="sidebar">Фильтры <i></i></button>
        <?php if ($childrenCategory) :?>
        <div class="col-md-12 row">
            <?php foreach ($childrenCategory as $categoryProduct) : ?>
                <?php $this->widget('application.widgets.ProductItem', ['categoryProduct' => $categoryProduct]); ?>
            <?php endforeach; ?>

        </div>
        <?php else :?>
            <span>Товаров не найдено</span>
        <?php endif; ?>
    </div>
</section>

<?php if ($category->getDescription()) :?>
<section class="ourInfo">
    <div class="title"><?=$category->getTitle()?></div>
    <div class="container">

   <?= $category->getDescription()?>
  </div>

    <div class="btn_show_more">
        <button class="custom_btn">Подробнее</button>
    </div>
</section>
<?php endif; ?>

<button class="filters-mobile" data-toggle="sidebar">
    <i></i>
</button>