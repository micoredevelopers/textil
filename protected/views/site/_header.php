			<div class="c3">
				<div class="c4 fl">
					<ul>
						<?
						$menu_top=array(
								array('title'=>'Despre','id'=>'0','url'=>'textpages'),
								array('title'=>'Publicitate','id'=>'1','url'=>'textpages'),
								array('title'=>'Doneaza','id'=>'2','url'=>'textpages'),
								array('title'=>'Contacte','id'=>'3','url'=>'textpages'),
								array('title'=>'Abonează-te','id'=>'4','url'=>'aboneaza'),
								);
						foreach ($menu_top as $key => $m) {?>
						<li class="fl">
							<a href="<?php echo Yii::app()->createUrl("/site/$m[url]", $params = array()); ?>"><?=$m['title']?></a>
						</li>	
						<?}?>
						<li class="clear"></li>
						</ul>
				</div>
				<div class="clear"></div>
				<div class="c5">
					<div class="c6 fl"><a href="/">Moldova Creștină</a></div>
					<div class="c6a fl"></div>
					<div class="c7 fl">
						<div class="c8">Portal multimedia creștin din Moldova</div>
						<div class="c9">
							<div class="c10 fl"><input type="text"/></div>
							<div class="c11 fl"><a href="#"><img src="/images/x.gif" alt="image"></a></div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="c12 fl">
						<div class="c12a fl">
							<a target="_blank" href="https://www.facebook.com/preot.vasile.filat"><img src="/images/x.gif" alt="image"></a>
						</div>
						<div class="c12f fl">
							<a target="_blank" href="https://twitter.com/moldovacrestina"><img src="/images/x.gif" alt="image"></a>
						</div>
						<div class="c12b fl">
							<a target="_blank" href="http://www.odnoklassniki.ru/profile/557986429631"><img src="/images/x.gif" alt="image"></a>
						</div>
						<div class="c12g fl">
							<a href="#"><img src="/images/x.gif" alt="image"></a>
						</div><div class="c12c fl">
							<a href="#"><img src="/images/x.gif" alt="image"></a>
						</div>
						<div class="c12d fl">
							<a href="#"><img src="/images/x.gif" alt="image"></a>
						</div>
						<div class="c12e fl">
							<a href="#"><img src="/images/x.gif" alt="image"></a>
						</div>
						
						
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			
			<div class="">
				<div class="c14 fl">
					<div class="c15">
						<div class="c15a">
							<div class="c16 " id="zone-bar" style="<?= isset($this->activemenu) && $this->activemenu =='bible_answers'? '' : (!isset($this->activemenu) ? '' : 'background:#e2c0bf;') ?>">
								<ul ><li>
									<span>
										<?= CHtml::link("Răspunsuri din biblie", array('/BibleAnswers'),array('class'=>'c41 fl'))?>
										<em class="opener-technology fr">
											<div class="c16a"><a href="javascript:;">&#9660;</a></div>
										</em>
										<div class="clear"></div>
									</span>
									<!-- <div class="section vertical"  style="display:none;"> -->
									<ul class="tabs technologysublist section vertical " style="display:none;<?= isset($this->activemenu) && $this->activemenu =='bible_answers'? '' : (!isset($this->activemenu) ? '' : 'background:#e2c0bf;') ?>">
										<?//,'Toate'
										$submenu = array(
												array('url'=>Yii::app()->createUrl("/bibleAnswers/intreaba"),'name'=>'întreabă'),
												array('url'=>Yii::app()->createUrl("/bibleAnswers/marturisirea"),'name'=>'mărturisire'),
												array('url'=>Yii::app()->createUrl("/bibleAnswers/sondaje"),'name'=>'sondaje'),
												array('url'=>Yii::app()->createUrl("/bibleAnswers/chat"),'name'=>'live chat'),
												array('url'=>Yii::app()->createUrl("/bibleAnswers/moderator"),'name'=>' moderator'),
												);
										foreach ($submenu as $key => $m) { ?>
											<li><a style="<?= isset($this->activemenu) && $this->activemenu =='bible_answers'? '' : (!isset($this->activemenu) ? '' : 'color:rgb(129, 60, 60);') ?>" href="<?=$m['url']?>"><?=$m['name']?></a></li>
										<?}?>
									</li>
									</ul>	
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="c14 fl">
					<div class="c15">
						<div class="c15b">
							<div class="c31" id="zone-bar1" style="<?= isset($this->activemenu) && $this->activemenu =='news'? '' : (!isset($this->activemenu) ? '' : 'background:#bfd5c9!important;') ?>">
								<ul>
									<li>
										<span>
											<?= CHtml::link("Știri", array('/news'),array('class'=>'c41 c41news fl')) ?>
											<em class="opener-technology fr">
												<div class="c16b"><a href="javascript:;">&#9660;</a></div>
											</em>
										</span>
										<div class="clear"></div>
								<ul class="tabs technologysublist section vertical " style="<?= isset($this->activemenu) && $this->activemenu =='news'? '' : (!isset($this->activemenu) ? '' : 'background:#bfd5c9!important;') ?>display:none;background: #01582d;margin-top: 0px;">
									<?
									$submenu = array(
												array('url'=>Yii::app()->createUrl("/news/agenda"),'name'=>'Agenda Evenimentelor'),
												array('url'=>Yii::app()->createUrl("/news/message"),'name'=>'trimite o stiri'),
												array('url'=>Yii::app()->createUrl("/news/sondaje"),'name'=>'sondaje '),
												);
									foreach ($submenu as $key => $m) {?>
											 <li><a style="<?= isset($this->activemenu) && $this->activemenu =='news'? '' : (!isset($this->activemenu) ? '' : 'color:rgb(81, 167, 77)!important;') ?>"href="<?=$m['url']?>"><?=$m['name']?></a></li> 
									<?}?>
								</ul>
								</li>
								</ul>
							</div>
						</div>
					</div>
					<script>
						$('#zone-bar').hover(function(){
								$(this).find('.c16a').addClass('c16c');
							},function(){
								$(this).find('.c16a').removeClass('c16c');
						});
					</script>
					<script>
						$('#zone-bar1').hover(function(){
								$(this).find('.c16b').addClass('c16d');
							},function(){
								$(this).find('.c16b').removeClass('c16d');
						});
					</script>

				</div>
				<div class="c14 fl">
					<div class="c15">
						<div class="c34" >
							<?= CHtml::link("Moldova Creștină TV", array('/video'),array('class'=>'c41 c41mctv','style'=>''.isset($this->activemenu) && $this->activemenu =='video'? '' : (!isset($this->activemenu) ? '' : 'background:#bfd6de!important;'.''))) ?>
							
						</div>  <!-- c34 end -->
					</div>
				</div>
				<div class="c14a fl">
					<div class="c36">
						<?= CHtml::link("Blogosfera", array('/blogosfera'),array('class'=>'c41 c41blogosfera','style'=>''.isset($this->activemenu) && $this->activemenu =='blog'? '' : (!isset($this->activemenu) ? '' : 'background:#dcd2c9!important;'.''))) ?>
						
					</div> <!-- end c36 -->

				</div>
				<div class="clear"></div>
			</div>	
			
<script>
  function chageOpencalendar(item){
    $(item).parent().find('.c120').toggle('fast');
    $(item).parent().toggleClass('active');
  }
</script>
<script>
  function chageOpencalendar1(item){
    <?/*$('#calendar').toggle().css('height',$('body').css('height')); */?>
    $(item).parent().toggle('fast');
    $(item).parent().parent().next().toggle('fast');
  }
</script>	
<script>
		$(document).ready(function(){
		   $("#zone-bar li em").click(function() {
		   		var hidden = $(this).parents("li").children("ul").is(":hidden");
		   		
				$("#zone-bar>ul>li>ul").hide()        
			   	$("#zone-bar>ul>li>a").removeClass();
			   		
			   	if (hidden) {
			   		$(this)
				   		.parents("li").children("ul").toggle()
				   		.parents("li").children("a").addClass("zoneCur");
				   	} 
			   });
		});
	</script>
	<script>
		$(document).ready(function(){
		   $("#zone-bar1 li em").click(function() {
		   		var hidden = $(this).parents("li").children("ul").is(":hidden");
		   		
				$("#zone-bar1>ul>li>ul").hide()        
			   	$("#zone-bar1>ul>li>a").removeClass();
			   		
			   	if (hidden) {
			   		$(this)
				   		.parents("li").children("ul").toggle()
				   		.parents("li").children("a").addClass("zoneCur");
				   	} 
			   });
		});
	</script>		
