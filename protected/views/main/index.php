<?php

$this->breadcrumbs = null;
$this->params['showSlider'] = true;
?>

<?php if ($newProducts) : ?>
    <section class="content">
        <div class="container">
            <div class="title">Новинки</div>


            <div class="row col-md-12 goodList mobileList">
                <div class="col-md-12 goodsItem">
                    <div id="myCarouselMobile_1" class="carousel slide" data-interval="false">
                        <div class="carousel-inner">
                            <?php foreach ($newProducts as $categoryProduct) : ?>
                                <?php $this->widget('application.widgets.ProductItem', ['carouselID' => 'product-carousel-mobile', 'categoryProduct' => $categoryProduct, 'viewFile' => 'product_item_mobile']); ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="thumbs">
                            <a class="carousel-control left pt-3" href="#myCarouselMobile_1" data-slide="prev"><i class="goods-arrow"></i></a>
                            <ol class="carousel-indicators list-inline">
                                 <?php for ($i = 0; $i < count($newProducts); $i++) : ?>
                                   <li data-target="#myCarouselMobile_1" data-slide-to="<?= $i ?>" <?= $i == 0 ? 'class="active"' : '' ?>></li>
                                 <?php endfor; ?>
                            </ol>
                            <a class="carousel-control right pt-3" href="#myCarouselMobile_1" data-slide="next"><i class="goods-arrow"></i></a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row col-md-12 goodsList">
                <?php foreach ($newProducts as $categoryProduct) : ?>
                    <?php $this->widget('application.widgets.ProductItem', ['categoryProduct' => $categoryProduct]); ?>
                <?php endforeach; ?>

            </div>
        </div>
    </section>
<?php endif; ?>

<?php if ($photos) : ?>
    <section class="ourAdvatages">
        <div class="container">
            <div class="title">Преимущества</div>

            <div class="col-md-12 ourAdvatageContent">
                <?php foreach ($photos as $item) : ?>
                    <div class="col-md-4">
                        <img src="/images/Foto/<?= $item->img1 ?>" alt="<?= $item->title ?>">
                        <div class="title"><?= $item->title ?></div>
                        <div class="info">
                            <?= $item->description ?>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>

        </div>
    </section>
<?php endif; ?>
<?php if ($contacts) : ?>
    <section class="ourMagazine">
        <div class="container">
            <div class="title">Наши магазины</div>
        </div>

        <div class="col-md-12">
            <ul class="nav nav-pills nav-fill navtop">
                <?php foreach ($contacts as $k => $contact) : ?>
                    <li class="nav-item">
                        <a class="nav-link<?= $k == 0 ? ' active' : '' ?>" href="#menu<?= $k ?>"
                           data-toggle="tab"><?= $contact->getCity() ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div class="tab-content">
                <?php foreach ($contacts as $k => $contact) : ?>
                    <div class="tab-pane<?= $k == 0 ? ' active' : '' ?>" role="tabpanel" id="menu<?= $k ?>">
                        <div id="carouselInTab<?= $k ?>" class="carousel slide" data-ride="carousel">

                            <?php if ($slider) : ?>
                                <div class="carousel-inner">
                                    <?php foreach ($slider as $kSlider => $itemSlider) : ?>
                                        <div class="carousel-item<?= $kSlider == 0 ? ' active' : '' ?>">
                                            <img class="d-block w-100"
                                                 src="/images/Carousel_main_small/<?= $itemSlider->image; ?>"
                                                 alt="Первый слайд">
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                            <div class="carouseIndi">
                                <div class="sliderAddress">
<!--                                    <a href="tel:--><?//= $contact->getPhone() ?><!--">--><?//= $contact->getFullAddress()?><!--</a>-->
                                    <a><?= $contact->getFullAddress()?></a>
                                </div>
                                <?php if ($slider) : ?>
                                <div class="carouseIndi-actions">
                                    <a class="carousel-control-prev" href="#carouselInTab<?= $k ?>" role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <ol class="carousel-indicators">
                                        <?php foreach ($slider as $kSlider => $itemSlider) : ?>
                                        <li data-target="#carouselInTab<?= $k ?>" data-slide-to="<?= $kSlider ?>" <?= $kSlider == 0 ? ' class="active"' : '' ?>></li>
                                        <?php endforeach; ?>
                                    </ol>
                                    <a class="carousel-control-next" href="#carouselInTab<?= $k ?>" role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<section class="ourInfo">
    <div class="container">
        <h1 class="title"><?= Yii::app()->metaTags->getH1(); ?></h1>
        <?= $page->body; ?>
    </div>
    <div class="btn_show_more">
        <button class="custom_btn">Подробнее</button>
    </div>
</section>