<section class="ourMagazine contactsPage">
    <div class="container">
        <h1 class="title"><?=Yii::app()->metaTags->getH1()?></h1>
    </div>

    <div class="col-md-12">
        <?php if ($contacts) :?>
        <ul class="nav nav-pills nav-fill navtop">
            <?php foreach ($contacts as $k=>$item) :?>
            <li class="nav-item">
                <a class="nav-link<?= $k==0 ? ' active' : ''?>" href="#menu<?=$k?>" data-toggle="tab"><?=$item->getCity()?></a>
            </li>
            <?php endforeach ;?>
        </ul>


        <div class="tab-content">
            <?php foreach ($contacts as $k=>$item) :?>
            <div class="tab-pane<?= $k==0 ? ' active' : ''?>" role="tabpanel" id="menu<?=$k?>">
                <div class="contactsMap">
                    <iframe src="<?=$item->getCoordinates()?>" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

                <p><?= $item->getFullAddress()?></p>
                <?php if ($slider) :?>
                <h3 class="title">Наши магазины</h3>
                <div id="carouselInTab<?=$k?>" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner">
                        <?php foreach ($slider as $kSlider => $itemSlider) :?>
                        <div class="carousel-item<?= $kSlider==0 ? ' active' : ''?>">
                            <img class="d-block w-100"  style="height: 615px" src="/images/Carousel_main_small/<?= $itemSlider->image; ?>" alt="Первый слайд">
                        </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="carouseIndi">
                        <a class="carousel-control-prev" href="#carouselInTab<?=$k?>" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <ol class="carousel-indicators">
                            <?php foreach ($slider as $kSlider => $itemSlider) :?>
                            <li data-target="#carouselInTab<?=$k?>" data-slide-to="<?=$kSlider?>" <?= $kSlider==0 ? 'class="active"' : ''?>></li>
                            <?php endforeach; ?>
                        </ol>
                        <a class="carousel-control-next" href="#carouselInTab<?=$k?>" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <?php endforeach ;?>
        </div>
        <?php endif;?>
    </div>
</section>