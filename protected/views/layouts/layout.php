<?php


if (!$this->breadcrumbs && $this->breadcrumbs !== null) {
    Yii::app()->breakCrumbs->setCrumbsByPage(new BreakCrumbsMainPage());
    Yii::app()->breakCrumbs->addCrumbsByPage(new BreakCrumbsStatic());
    $this->breadcrumbs = Yii::app()->breakCrumbs->linksToWidget();
}
$showSlider = isset($this->params['showSlider']) ? $this->params['showSlider'] : false;

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title><?= $this->metatitle; ?></title>
	<meta charset="utf-8">
    <meta name="description" content="<?= $this->metadescription; ?>"/>
    <meta name="keywords" content="<?= $this->metakeywords; ?>"/>
    <?= Yii::app()->noIndexRoutes->metaTag(); ?>
    <?php Yii::app()->canonical->metaTag(); ?>
    <link rel="icon" href="/favicon.png" type="image/png"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/public/static/css/index.css?v=14"/>
    <link rel="stylesheet" href="/public/static/css/media.css"/>
    <link rel="stylesheet" href="/public/static/css/slick.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <?= TextFileScripts::instanceByFile('end_head')->read(); ?>
</head>
<body>
<?= TextFileScripts::instanceByFile('start_body')->read(); ?>
<?php if (isset($this->params['categories']) && $this->params['categories']) : ?>
    <section id="wrapper" class="toggled">
        <div id="sidebar-wrapper">
        <span class="side-title">
            Категория
  
            <i class="sideBarClose"></i>
        </span>
            <ul class="sidebar-nav">
                <?php foreach ($this->params['categories'] as $category) : ?>
                    <li>
                        <a href="<?= $category->getUrl(); ?>"><?= $category->getTitle(); ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>
<?php endif; ?>

<section class="header">
    <?php $this->widget('application.widgets.Header'); ?>
    <?php if ($showSlider) {
    $this->widget('application.widgets.MainSlider');
} ?>
</section>

<?php if ($this->breadcrumbs) : ?>

    <section class="breadCrumbs bigOne">
        <ul>
            <?php foreach ($this->breadcrumbs as $key => $item) : ?>
                <li><a href="<?= $item['url']; ?>"><?= $item['label']; ?></a></li>
            <?php endforeach; ?>
        </ul>
    </section>
<?php endif; ?>

<?= $content; ?>

<?php $this->widget('application.widgets.Footer'); ?>

<div class="feedModal modal fade" id="feedBackModal" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Спасибо за обращение!</div>
            </div>
            <div class="modal-body">
                Мы перезвоним вам в ближайшее время
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Окей</button>
            </div>
        </div>
    </div>
</div>

<?= TextFileScripts::instanceByFile('end_body')->read(); ?>

<script src="/public/static/js/jquery.slim.min.js" defer></script>
<script src="/public/static/js/jquery.mask.min.js" defer></script>
<script src="/public/static/js/popper.min.js" defer></script>
<script src="/public/static/js/bootstrap.min.js" defer></script>
<script src="/public/static/js/slick.min.js" defer></script>
<script src="/public/static/js/main.js?v=14" defer></script>
<script src="/public/static/js/mask.js" defer></script>
<script src="/public/static/js/layout.js?v=14" defer></script>

<?php if (isset($this->params['scripts'])) : ?>
    <?php foreach ($this->params['scripts'] as $item) : ?>
        <script src="<?= $item; ?>" defer></script>
    <?php endforeach; ?>
<?php endif; ?>

<?= Yii::app()->microMarking->string(); ?>
<?= Yii::app()->ECommerce->getScripts(); ?>
</body>
</html>
