<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 13.11.2018
 * Time: 16:30
 */

class RoutesController extends Controller
{

    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'users'=>array('@'),
                'expression' => 'Yii::app()->user->isAdmin()',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex(){
        $routes = new Routes();
        if (Yii::app()->request->getPost('routes')){
            $routes->setRoutes(Yii::app()->request->getPost('routes'));
            $routes->setRoutes(Yii::app()->request->getPost('routes'));
            $routes->save();
            return Yii::app()->request->redirect('/admin/routes');
        }
        return $this->render('index',array(
            'routes'=>$routes
        ));
    }

}