<?php

class PrezentationController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'users'=>array('@'), 
				'expression' => 'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionAdmin()
	{	

                if (isset($_FILES['file'])) {
                $Upload = new Upload( (isset($_FILES['file']) ? $_FILES['file'] : null) );
                $destPath = Yii::app()->getBasePath().'/../upload/prezentation/';
                $files = glob($destPath.'*');
                foreach($files as $file){ // iterate files
				  if(is_file($file))
				    unlink($file); // delete file
				}
                // unlink($destPath.'prezentation.ppt');
                if ($Upload->uploaded) {                 
                     		$Upload->file_new_name_body = 'prezentation';
                     $Upload->process($destPath);
                         
                     // if was processed
                     if ($Upload->processed) {
                         $destName = $Upload->file_dst_name;    
                     
                         // write image filename on table
                         // $model->fullname_file = $destName;
                         // create the thumb  
                         unset($Upload);                      
                               
                     } else {
                         // echo($Upload->error);
                        // $result['error'] = $Upload->error.$destPath;
                     }
               }else {
                //   echo('Select a file to send');
                    // $result['error'] = 'Select a file to send';
               }
               // $model->fullname_status = 1 ;
               //  if($model->save()){
               //      $result['success'] = 1;
               //  }
           }

		$this->render('admin',array(
		));
	}

}
?>