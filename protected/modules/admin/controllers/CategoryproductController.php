<?php

class CategoryproductController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'users'=>array('@'), 
				'expression' => 'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Categoryproduct;
$imagename = microtime(true);
		 	$Upload = new Upload( (isset($_FILES['image']) ? $_FILES['image'] : null) );
			$Upload->jpeg_quality     = 100;

			$Upload->image_resize          = false;

      // some vars
			$newName  = $imagename;
			$destPath = Yii::app()->getBasePath().'/../images/Category/';
			$destName = '';
      // var_dump($destPath);exit();
      // verify if was uploaded
			if ($Upload->uploaded) {
				$Upload->file_new_name_body = $newName;                     
				$Upload->process($destPath);
              
              // if was processed
				if ($Upload->processed) {
					$destName = $Upload->file_dst_name;    
	              
	                  // write image filename on table
					$model->image = $destName;
	                  // create the thumb  
					unset($Upload);                       
	                        
				} else {
	                  // echo($Upload->error);
				}
			}else {
			//   echo('Select a file to send');
			}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		// if(isset($_POST['Carousel']))
		// {
		// 	$model->attributes=$_POST['Carousel'];
       		if (isset($_POST['delImg'])) {
				if($_POST['delImg']==1){					
					unlink(Yii::app()->basePath."/../images/Category/".$model->image);
					$model->image='';
				}
			}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Categoryproduct']))
		{
			$model->attributes=$_POST['Categoryproduct'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
$imagename = microtime(true);
		 	$Upload = new Upload( (isset($_FILES['image']) ? $_FILES['image'] : null) );
			$Upload->jpeg_quality     = 100;

			$Upload->image_resize          = false;

      // some vars
			$newName  = $imagename;
			$destPath = Yii::app()->getBasePath().'/../images/Category/';
			$destName = '';
      // var_dump($destPath);exit();
      // verify if was uploaded
			if ($Upload->uploaded) {
				$Upload->file_new_name_body = $newName;                     
				$Upload->process($destPath);
              
              // if was processed
				if ($Upload->processed) {
					$destName = $Upload->file_dst_name;    
	              
	                  // write image filename on table
					$model->image = $destName;
	                  // create the thumb  
					unset($Upload);                       
	                        
				} else {
	                  // echo($Upload->error);
				}
			}else {
			//   echo('Select a file to send');
			}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		// if(isset($_POST['Carousel']))
		// {
		// 	$model->attributes=$_POST['Carousel'];
       		if (isset($_POST['delImg'])) {
				if($_POST['delImg']==1){					
					unlink(Yii::app()->basePath."/../images/Category/".$model->image);
					$model->image='';
				}
			}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Categoryproduct']))
		{
			$model->attributes=$_POST['Categoryproduct'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Categoryproduct');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Categoryproduct('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Categoryproduct']))
			$model->attributes=$_GET['Categoryproduct'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Categoryproduct::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='categoryproduct-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
