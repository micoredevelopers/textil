<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 14.11.2018
 * Time: 09:44
 */

class Text_filesController extends Controller
{

    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'users' => array('@'),
                'expression' => 'Yii::app()->user->isAdmin()',
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

//    public function actionIndex(){
//        return $this->actionRobots();
//    }

    public function actionRobots()
    {
        $model = new TextFileRobots();
        if ($text = Yii::app()->request->getPost('write')) {
            $model->write($text);
            return Yii::app()->request->redirect('/admin/text_files/robots/');
        }
        return $this->render('robots', array(
            'model' => $model
        ));
    }

    public function actionScripts(){
        $files = [
            'end_head' => [
                'label' => ' В конце head',
            ],
            'start_body' => [
                'label' => 'В начале body',
            ],

            'end_body' => [
                'label' => 'В конце body',
            ],
        ];
        $models = [];
        foreach ($files as $file=>$info){
            $models[$file]=TextFileScripts::instanceByFile($file);
            if ($text = Yii::app()->request->getPost($file)){
                $models[$file]->write($text);
            }
        }
        if (Yii::app()->request->isPostRequest){
            return Yii::app()->request->redirect('/admin/text_files/scripts/');
        }
        return $this->render('scripts', array(
            'models' => $models,
            'files' => $files,
        ));
    }

    public function actionRedirects(){
        $redirectsObject = new TextFileRedirects();

        $this->saveRedirects('save',$redirectsObject);
        $redirects = $redirectsObject->read();
        $this->saveRedirects('add',$redirectsObject,$redirects);
        return $this->render('RedirectsModel', array(
            'models' => $redirects,
        ));
    }

    public function saveRedirects($postKey,TextFileRedirects $redirectsObject,$redirects=[]){
        $data = Yii::app()->request->getPost($postKey);
        if (is_array($data) && TextFileRedirects::validationRedirects($data)){
            $redirectsObject->write(array_merge($data,$redirects));
            Yii::app()->request->redirect('/admin/text_files/redirects/')->send();
        }

    }

}