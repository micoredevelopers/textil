<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 14.11.2018
 * Time: 12:17
 */

class RedirectsController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'users'=>array('@'),
                'expression' => 'Yii::app()->user->isAdmin()',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex(){

        $newModel = new RedirectsModel();
        $models = RedirectsModel::model()->findAll();
        if ($post = Yii::app()->request->getPost('RedirectsModel')){
            if (isset($post['new'])){
                $newModel->attributes = $post['new'];
                if ($newModel->save()){
                    return Yii::app()->request->redirect('/admin/redirects');
                }
            } else {
                RedirectsModel::multipleSave($models,$post);
            }
        }
        return $this->render('index',array(
            'newModel'=>$newModel,
            'models' => $models
        ));
    }

    public function actionDelete($id){
       if ($model=RedirectsModel::model()->findByPk($id)){
           $model->delete();
       }
        return Yii::app()->request->redirect('/admin/redirects');
    }

}