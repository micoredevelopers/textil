<?php
$this->breadcrumbs=array(
	'Заказы'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Заказы','url'=>array('index')),
	array('label'=>'Create Заказы','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('orders-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Заказы</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'orders-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'date',
		'ip',
		'address',
		// 'order',
		
		'price',
		'status',
		'phone_contacts',
		'paymant',
		'comment_user',
		'user_email',
		
		 // array(
   //                  'name'=>'order',
   //                  'value'=>$model->order,
   //                  'htmlOptions'=>array('width'=>'40'),
   //                  'type' => 'html',
   //              ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
