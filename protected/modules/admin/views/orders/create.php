<?php
$this->breadcrumbs=array(
	'Заказы'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Заказы','url'=>array('index')),
	array('label'=>'Manage Заказы','url'=>array('admin')),
);
?>

<h1>Create Заказы</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>