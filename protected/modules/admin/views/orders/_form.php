<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'orders-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'date',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'ip',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'address',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<p><br><br>Были заказаны:</p>
	<?php 

	$r =  unserialize($model->order) ;
	$tabel = '<table><tr  style="background-color: #C7C7C7;">';
	$tabel .= '<td>Товар</td><td>ID Товара</td><td>Количество</td><td>Сумма</td></tr>';
	foreach ($r as $key => $value) {
		$tabel .= '<tr>';
		
		$tabel .= '<td style="padding: 12px;">'.$value["title"].'</td>';
		$tabel .= '<td style="padding: 12px;">'.$value["id"].'</td>';
		$tabel .= '<td style="padding: 12px;">'.$value["count"].'</td>';
		$tabel .= '<td style="padding: 12px;">'.$value["price"].'</td>';
		// echo ( $value["id"]);
		// echo ( $value["title"]);
		// echo ( $value["price"]);
		// echo ( $value["count"]);
		$tabel .= '</tr>';

	}
	$tabel .= '</table><br><br>';
	echo $tabel;
	?>

	<?php echo $form->textFieldRow($model,'price',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'phone_contacts',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'paymant',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'comment_user',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'user_email',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
