<?php
$this->breadcrumbs=array(
	'Labels',
);

$this->menu=array(
	array('label'=>'Create Labels','url'=>array('create')),
	array('label'=>'Manage Labels','url'=>array('admin')),
);
?>

<h1>Labels</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
