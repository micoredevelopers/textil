<?php
$this->breadcrumbs=array(
	'Labels'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Labels','url'=>array('index')),
	array('label'=>'Create Labels','url'=>array('create')),
	array('label'=>'Update Labels','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Labels','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Labels','url'=>array('admin')),
);
?>

<h1>View Labels #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'category',
		'alias',
		// 'title_en',
		'title_ru',
		// 'title_ro',
	),
)); ?>
