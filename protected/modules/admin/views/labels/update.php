<?php
$this->breadcrumbs=array(
	'Labels'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Labels','url'=>array('index')),
	array('label'=>'Create Labels','url'=>array('create')),
	array('label'=>'View Labels','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Labels','url'=>array('admin')),
);
?>

<h1>Update Labels <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>