<?php
$this->breadcrumbs=array(
	'Labels'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Labels','url'=>array('index')),
	array('label'=>'Manage Labels','url'=>array('admin')),
);
?>

<h1>Create Labels</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>