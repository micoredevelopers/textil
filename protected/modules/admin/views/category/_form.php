<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'category-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype' => 'multipart/form-data'),
)); ?>
<?
	$criteria = new CDbCriteria;
	if (isset($model->id)) {
	$criteria->condition = "parent = 0 AND id <> ".$model->id;
	}else{
	$criteria->condition = "parent = 0";
	}
	$parent_model = Category::model()->findAll($criteria);
	if ($model) {
		$parent_list = array();
		$parent_list[0] = 'Category';
		foreach ($parent_model as $key => $value) {
			$parent_list[$value->id] = $value->title;
		}
	}
?>
	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'title_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'title_en',array('class'=>'span5','maxlength'=>255)); ?>

	<h3>Description Ru</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'description_ru',
		      // Optional config
		      //'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>

		<h3>Description Ro</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'description_ro',
		      // Optional config
		      //'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>
		
			<h3>Description En</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'description_en',
		      // Optional config
		      'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>

	<?php echo $form->textFieldRow($model,'alias',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php echo $form->textFieldRow($model,'position',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->dropDownListRow($model,'parent',$parent_list); ?>

	<?php //echo $form->textFieldRow($model,'parent',array('class'=>'span5')); ?>

	<?php echo $form->dropDownListRow($model,'status',Yii::app()->params['cat_status']); ?>
	

	<?php //echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
