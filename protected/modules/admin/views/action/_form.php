<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'action-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'title_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'title_en',array('class'=>'span5','maxlength'=>255)); ?>
	
	<h3>On main page</h3>
    <?php echo $form->checkboxRow($model,'top'); ?>


	<h3>Body Ru</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'body_ru',
		      // Optional config
		      //'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>

		<h3>Body Ro</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'body_ro',
		      // Optional config
		      //'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>
		
			<h3>Body En</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'body_en',
		      // Optional config
		      'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>

	<?php echo $form->textFieldRow($model,'alias',array('class'=>'span5','maxlength'=>255)); ?>

	<? if($model->image){ ?>
    <label><input type="checkbox" name="delImg" value="1"> Delete image</label>
    <div class="list-view">
      <div class="items">
        <ul class="thumbnails">
          <li class="span3">
            <a href="#" class="thumbnail" rel="tooltip" data-title="news" data-toggle='modal' data-target='#myModal'>
                <img data-src="holder.js/300x200" src="<?php echo Yii::app()->request->baseUrl; ?>/images/Action/<?= $model->image ?>" alt="">
            </a>
          </li>
        </ul>
      </div>
    </div>
    <? } ?>
    <?php echo $form->fileFieldRow($model,'image',array('name'=>'image')); ?>

	<h5>Date:</h5>
  	<?
  	$model->date = ($model->date && $model->date!='0000-00-00 00:00:00') ?$model->date:date('Y-m-d H:i:s');
  	 Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
            $this->widget ('CJuiDateTimePicker',  
                  array (
                      'language'=> 'ru',
                      'attribute'=>'date',
                      'model'=>$model,
                      // 'value' => date('Y-m-d H:i:s'),
                      'htmlOptions' => array(
                                          'class'=>'input-small',
                                          'style'=>'margin:0;width: 220px;',
                                          ),
                      'options'=>array (
                          'dateFormat' => 'yy-mm-dd',
                          'timeFormat' => 'hh:mm:ss',//' tt'
                          'showSecond'=>false,
                      ), 
                  )   
              );?>
              <br>

	<?php echo $form->dropDownListRow($model,'status',Yii::app()->params['status']); ?>

	<br>
	<a href="javascript:;" onclick="$('#meta').toggle();">
		Meta
	</a>
	<div id="meta" style="display:none;">

	<?php echo $form->textFieldRow($model,'meta_titile_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_titile_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_titile_en',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'meta_description_ro',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'meta_description_ru',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'meta_description_en',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'meta_keyword_ro',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'meta_keyword_ru',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'meta_keyword_en',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
