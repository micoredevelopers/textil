<?php
$this->breadcrumbs=array(
	'Actions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Action','url'=>array('index')),
	array('label'=>'Create Action','url'=>array('create')),
	array('label'=>'Update Action','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Action','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Action','url'=>array('admin')),
);
?>

<h1>View Action #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title_ro',
		'title_ru',
		'title_en',
		'body_ro',
		'body_ru',
		'body_en',
		'alias',
		'image',
		'date',
		'status',
		'meta_titile_ro',
		'meta_titile_ru',
		'meta_titile_en',
		'meta_description_ro',
		'meta_description_ru',
		'meta_description_en',
		'meta_keyword_ro',
		'meta_keyword_ru',
		'meta_keyword_en',
	),
)); ?>
