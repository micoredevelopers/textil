<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_ro')); ?>:</b>
	<?php echo CHtml::encode($data->title_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_ru')); ?>:</b>
	<?php echo CHtml::encode($data->title_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_en')); ?>:</b>
	<?php echo CHtml::encode($data->title_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body_ro')); ?>:</b>
	<?php echo CHtml::encode($data->body_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body_ru')); ?>:</b>
	<?php echo CHtml::encode($data->body_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body_en')); ?>:</b>
	<?php echo CHtml::encode($data->body_en); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('alias')); ?>:</b>
	<?php echo CHtml::encode($data->alias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_titile_ro')); ?>:</b>
	<?php echo CHtml::encode($data->meta_titile_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_titile_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_titile_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_titile_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_titile_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_description_ro')); ?>:</b>
	<?php echo CHtml::encode($data->meta_description_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_description_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_description_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_description_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_description_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keyword_ro')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keyword_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keyword_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keyword_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keyword_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keyword_en); ?>
	<br />

	*/ ?>

</div>