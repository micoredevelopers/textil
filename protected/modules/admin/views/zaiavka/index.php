<?php
$this->breadcrumbs=array(
	'Zaiavkas',
);

$this->menu=array(
	array('label'=>'Create Zaiavka','url'=>array('create')),
	array('label'=>'Manage Zaiavka','url'=>array('admin')),
);
?>

<h1>Zaiavkas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
