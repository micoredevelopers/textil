<?php
$this->breadcrumbs=array(
	'Zaiavkas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Zaiavka','url'=>array('index')),
	array('label'=>'Manage Zaiavka','url'=>array('admin')),
);
?>

<h1>Create Zaiavka</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>