<?php
$this->breadcrumbs=array(
	'Zaiavkas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Zaiavka','url'=>array('index')),
	array('label'=>'Create Zaiavka','url'=>array('create')),
	array('label'=>'View Zaiavka','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Zaiavka','url'=>array('admin')),
);
?>

<h1>Update Zaiavka <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>