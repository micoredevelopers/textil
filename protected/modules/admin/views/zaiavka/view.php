<?php
$this->breadcrumbs=array(
	'Zaiavkas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Zaiavka','url'=>array('index')),
	array('label'=>'Create Zaiavka','url'=>array('create')),
	array('label'=>'Update Zaiavka','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Zaiavka','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Zaiavka','url'=>array('admin')),
);
?>

<h1>View Zaiavka #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'mesage',
		'ip',
		'created',
	),
)); ?>
