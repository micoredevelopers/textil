<style type="text/css">
h3{
	font-size:14px;
	font-weight:100;
}
</style>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'carousel-form',
	'enableAjaxValidation'=>false,
  'htmlOptions'=>array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListRow($model,'status',Yii::app()->params['status']); ?>

	<?php echo $form->textFieldRow($model,'title_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'title_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'title_en',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>50)); ?>

	<h5>Date:</h5>
  	<?
  	$model->date = ($model->date && $model->date!='0000-00-00 00:00:00') ?$model->date:date('Y-m-d H:i:s');
  	 Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
            $this->widget ('CJuiDateTimePicker',  
                  array (
                      'language'=> 'ru',
                      'attribute'=>'date',
                      'model'=>$model,
                      // 'value' => date('Y-m-d H:i:s'),
                      'htmlOptions' => array(
                                          'class'=>'input-small',
                                          'style'=>'margin:0;width: 220px;',
                                          ),
                      'options'=>array (
                          'dateFormat' => 'yy-mm-dd',
                          'timeFormat' => 'hh:mm:ss',//' tt'
                          'showSecond'=>false,
                      ), 
                  )   
              );?>
              <br>

	<? if($model->image){ ?>
    <label><input type="checkbox" name="delImg" value="1"> Delete image</label>
    <div class="list-view">
      <div class="items">
        <ul class="thumbnails">
          <li class="span3">
            <a href="#" class="thumbnail" rel="tooltip" data-title="news" data-toggle='modal' data-target='#myModal'>
                <img data-src="holder.js/300x200" src="<?php echo Yii::app()->request->baseUrl; ?>/images/Carousel_header/<?= $model->image ?>" alt="">
            </a>
          </li>
        </ul>
      </div>
    </div>
    <? } ?>
    <?php echo $form->fileFieldRow($model,'image',array('name'=>'image')); ?>

	<?php //echo $form->textFieldRow($model,'short_body_en',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'short_body_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'short_body_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_description_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'meta_description_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'meta_description_en',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_keywords_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'meta_keywords_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'meta_keywords_en',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'alias',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
