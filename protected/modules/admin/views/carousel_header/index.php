<?php
$this->breadcrumbs=array(
	'Carousels',
);

$this->menu=array(
	array('label'=>'Create Carousel','url'=>array('create')),
	array('label'=>'Manage Carousel','url'=>array('admin')),
);
?>

<h1>Carousels</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
