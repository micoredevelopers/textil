<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'orientation-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'title_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>255)); ?>

  <?php echo $form->textFieldRow($model,'alias',array('class'=>'span5','maxlength'=>255)); ?>
  
   <?php echo $form->textFieldRow($model,'position',array('class'=>'span5','maxlength'=>255)); ?>

<?=$form->label($model,'description');?>
   <?php echo $form->textArea($model,'description',array('class'=>'span5','maxlength'=>255)); ?>

	<? if($model->img1){ ?>
    <label><input type="checkbox" name="delImg1" value="1"> Delete image</label>
    <div class="list-view">
      <div class="items">
        <ul class="thumbnails">
          <li class="span3">
            <a href="#" class="thumbnail" rel="tooltip" data-title="news" data-toggle='modal' data-target='#myModal'>
                <img data-src="holder.js/300x200" src="<?php echo Yii::app()->request->baseUrl; ?>/images/Foto/<?= $model->img1 ?>" alt="">
            </a>
          </li>
        </ul>
      </div>
    </div>
    <? } ?>
    <?php echo $form->fileFieldRow($model,'img1',array('name'=>'img1')); ?>


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
