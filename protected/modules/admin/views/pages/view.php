<?php
$this->breadcrumbs=array(
	'Pages'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Pages','url'=>array('index')),
	array('label'=>'Create Pages','url'=>array('create')),
	array('label'=>'Update Pages','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Pages','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pages','url'=>array('admin')),
);
?>

<h1>View Pages #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'status',
		'date',
		'link',
		'image',
		'title_ru',
		'meta_title_ru',
		'meta_description_ru',
		'meta_keywords_ru',
		'body_ru',
		'short_body_ru',
		// 'title_en',
		// 'meta_title_en',
		// 'meta_description_en',
		// 'meta_keywords_en',
		// 'body_en',
		// 'short_body_en',
		// 'title_ro',
		// 'meta_title_ro',
		// 'meta_description_ro',
		// 'meta_keywords_ro',
		// 'body_ro',
		// 'short_body_ro',
		'alias',
		'on_menu',
	),
)); ?>
