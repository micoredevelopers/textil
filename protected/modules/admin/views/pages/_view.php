<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('link')); ?>:</b>
	<?php echo CHtml::encode($data->link); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_ru')); ?>:</b>
	<?php echo CHtml::encode($data->title_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_title_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_title_ru); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_description_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_description_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keywords_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keywords_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body_ru')); ?>:</b>
	<?php echo CHtml::encode($data->body_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('short_body_ru')); ?>:</b>
	<?php echo CHtml::encode($data->short_body_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_en')); ?>:</b>
	<?php echo CHtml::encode($data->title_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_title_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_title_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_description_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_description_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keywords_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keywords_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body_en')); ?>:</b>
	<?php echo CHtml::encode($data->body_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('short_body_en')); ?>:</b>
	<?php echo CHtml::encode($data->short_body_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_ro')); ?>:</b>
	<?php echo CHtml::encode($data->title_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_title_ro')); ?>:</b>
	<?php echo CHtml::encode($data->meta_title_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_description_ro')); ?>:</b>
	<?php echo CHtml::encode($data->meta_description_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keywords_ro')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keywords_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body_ro')); ?>:</b>
	<?php echo CHtml::encode($data->body_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('short_body_ro')); ?>:</b>
	<?php echo CHtml::encode($data->short_body_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alias')); ?>:</b>
	<?php echo CHtml::encode($data->alias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('on_menu')); ?>:</b>
	<?php echo CHtml::encode($data->on_menu); ?>
	<br />

	*/ ?>

</div>