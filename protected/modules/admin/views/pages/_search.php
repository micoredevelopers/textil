<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'status',array('class'=>'span5','maxlength'=>8)); ?>

	<?php echo $form->textFieldRow($model,'date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'image',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'title_ru',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php echo $form->textFieldRow($model,'meta_title_ru',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php echo $form->textFieldRow($model,'meta_description_ru',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php echo $form->textFieldRow($model,'meta_keywords_ru',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php echo $form->textAreaRow($model,'body_ru',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'short_body_ru',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php //echo $form->textFieldRow($model,'title_en',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php //echo $form->textFieldRow($model,'meta_title_en',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php //echo $form->textFieldRow($model,'meta_description_en',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php //echo $form->textFieldRow($model,'meta_keywords_en',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php //echo $form->textAreaRow($model,'body_en',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php //echo $form->textAreaRow($model,'short_body_en',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php // echo $form->textFieldRow($model,'title_ro',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php //echo $form->textFieldRow($model,'meta_title_ro',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php //echo $form->textFieldRow($model,'meta_description_ro',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php //echo $form->textFieldRow($model,'meta_keywords_ro',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php //echo $form->textAreaRow($model,'body_ro',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php //echo $form->textAreaRow($model,'short_body_ro',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'alias',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'on_menu',array('class'=>'span5','maxlength'=>3)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
