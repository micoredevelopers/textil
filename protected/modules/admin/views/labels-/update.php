<?php
$this->breadcrumbs=array(
	' Фразы сайта'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Редактировать',
);

$this->menu=array(
	array('label'=>'Администрировать','url'=>array('admin')),
	array('label'=>'Создать','url'=>array('create')),
	 array('label'=>'Редактировать','url'=>array('update','id'=>$model->id), 'active'=>true),
	//array('label'=>'Вид','url'=>array('view','id'=>$model->id)),
	array('label'=>'Удалить','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>



<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>