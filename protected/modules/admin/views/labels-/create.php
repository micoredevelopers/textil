<?php
$this->breadcrumbs=array(
	// 'homeLink'=>array('Home'=>array('site/index')),
	' Фразы сайта'=>array('admin'),
	'Создать',
);
$this->menu=array(
	array('label'=>'Администрировать','url'=>array('admin')),
	array('label'=>'Создать','url'=>array('create'), 'active'=>true),
	// array('label'=>'List Pages','url'=>array('index')),
);
?>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>