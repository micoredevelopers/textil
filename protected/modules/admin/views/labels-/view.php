<?php
$this->breadcrumbs=array(
	' Фразы сайта'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Администрировать','url'=>array('admin')),
        array('label'=>'Создать','url'=>array('create')),
        array('label'=>'Редактировать','url'=>array('update','id'=>$model->id)),
        array('label'=>'Вид','url'=>array('view','id'=>$model->id), 'active'=>true),
        array('label'=>'Удалить','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        // array('label'=>'List Pages','url'=>array('index')),
);
?>

<h1>View Labels #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'category',
		'alias',
		'title_ru',
		'title_en',
		'title_ro',
	),
)); ?>
