<?php
$this->breadcrumbs=array(
	// 'homeLink'=>array('Home'=>array('site/index')),
	' Фразы сайта'=>array('admin'),
	'Администрировать',
);

$this->menu=array(
	array('label'=>'Администрировать','url'=>array('admin'), 'active'=>true ),
	array('label'=>'Создать','url'=>array('create')),
	// array('label'=>'List Pages','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'labels-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'category',
		'alias',
		'title_ru',
		'title_en',
		'title_ro',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
