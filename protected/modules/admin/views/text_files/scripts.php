<?php

$this->breadcrumbs = array(
    'robots' => array('robots'),
    'Manage',
);

?>
<div class="span12">
    <h1>Скрипты</h1>


    <form method="post">
        <div class="row">
            <?php foreach ($files as $file=>$info) :?>
            <div class="form-group col">
                <label><?=$info['label']?></label>
                <textarea style="width: 100%; height: 500px" name="<?=$file?>"><?=$models[$file]->read()?></textarea>
            </div>
            <?php endforeach; ?>
        </div>

        <div class="form-group">
            <button class="btn btn-success">Сохранить</button>
        </div>

    </form>
</div>
