<?php

$this->breadcrumbs = array(
    'robots' => array('robots'),
    'Manage',
);

?>
<div class="span12">
    <h1>Robots.txt</h1>


    <form method="post">
        <div class="row">
            <div class="form-group col">
               <textarea style="width: 100%; height: 500px" name="write"><?=$model->read()?></textarea>
            </div>
        </div>

        <div class="form-group">
            <button class="btn btn-success">Сохранить</button>
        </div>

    </form>
</div>
