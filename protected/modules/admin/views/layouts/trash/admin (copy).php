<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title>Admin module - <?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<div class="span-5 fl">
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Compartementele site-ului',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>array(

					array(
					  'label'=>'Raspunsuri din biblie(ro)',
					  'url'=>array('/admin/answers/admin'),
					  'active' => Yii::app()->controller->id == 'amswers',
					  'visible'=> Yii::app()->user->isAdmin() || Yii::app()->user->isAutorRaspunsuri(),
					  ),
					array(
					  'label'=>'Raspunsuri din biblie(ru)',
					  'url'=>array('/admin/answersru/admin'),
					  'active' => Yii::app()->controller->id == 'amswersru',
					  'visible'=> Yii::app()->user->isAdmin() || Yii::app()->user->isAutorRaspunsuri(),
					  ),
					array(
					  'label'=>'Raspunsuri din biblie(en)',
					  'url'=>array('/admin/answersen/admin'),
					  'active' => Yii::app()->controller->id == 'amswersen',
					  'visible'=> Yii::app()->user->isAdmin() || Yii::app()->user->isAutorRaspunsuri(),
					  ),
					array(
					  'label'=>'Raspunsuri din biblie(fr)',
					  'url'=>array('/admin/answersfr/admin'),
					  'active' => Yii::app()->controller->id == 'amswersfr',
					  'visible'=> Yii::app()->user->isAdmin() || Yii::app()->user->isAutorRaspunsuri(),
					  ),
				  array(
					  'label'=>'Raspunsuri din biblie(sp)',
					  'url'=>array('/admin/answerssp/admin'),
					  'active' => Yii::app()->controller->id == 'amswerssp',
					  'visible'=> Yii::app()->user->isAdmin() || Yii::app()->user->isAutorRaspunsuri(),
					  ),
					array(
					  'label'=>'Știri creștine',
					  'url'=>'/admin/news/admin',
					  'active' => Yii::app()->controller->id == 'news',
					  'visible'=> Yii::app()->user->isAdmin() || Yii::app()->user->isAutorStiri(),
					  ),
					array(
  					'label'=>'Televiziunea',
  					'url'=>array('/admin/video/admin'),
  					'active' => Yii::app()->controller->id == 'video',
					  'visible'=> Yii::app()->user->isAdmin() || Yii::app()->user->isAutorTv(),
  					),
					array(
  					'label'=>'Bloguri',
  					'url'=>array('/admin/blog/admin'),
  					'active' => Yii::app()->controller->id == 'blog',
					  'visible'=> Yii::app()->user->isAdmin() || Yii::app()->user->isAutorBlog(),
  					),
					array(
					  'label'=>'Utilizatori',
					  'url'=>array('/admin/user/admin'),
					  'active' => Yii::app()->controller->id == 'user',
					  'visible'=> Yii::app()->user->isAdmin(),
					  ),
					array(
					  'label'=>'Profilul personal',
					  'url'=>array('/admin/user/profile'),
					  'active' => Yii::app()->controller->id == 'user',
					  'visible'=> !Yii::app()->user->isAdmin(),
					  ),
					array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
				),
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
	?>
</div>
<div class="containerright" id="page">
	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php
		$this->widget('zii.widgets.CMenu',array(
			'items'=>$this->menu
				/*array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)

			),*/
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
<div id="content">
	<?php echo $content; ?>
</div><!-- content -->
	<?php // echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by Moldovacrestina<br/>
		All Rights Reserved.<br/>
		<?php //echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
