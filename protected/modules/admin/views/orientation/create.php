<?php
$this->breadcrumbs=array(
	'Orientations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Orientation','url'=>array('index')),
	array('label'=>'Manage Orientation','url'=>array('admin')),
);
?>

<h1>Create Orientation</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>