<?php
$this->breadcrumbs=array(
	'Orientations',
);

$this->menu=array(
	array('label'=>'Create Orientation','url'=>array('create')),
	array('label'=>'Manage Orientation','url'=>array('admin')),
);
?>

<h1>Orientations</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
