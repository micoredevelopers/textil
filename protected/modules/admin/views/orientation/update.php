<?php
$this->breadcrumbs=array(
	'Orientations'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Orientation','url'=>array('index')),
	array('label'=>'Create Orientation','url'=>array('create')),
	array('label'=>'View Orientation','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Orientation','url'=>array('admin')),
);
?>

<h1>Update Orientation <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>