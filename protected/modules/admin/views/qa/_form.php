<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'qa-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'title_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'title_en',array('class'=>'span5','maxlength'=>255)); ?>

	<h3>Body Ru</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'body_ru',
		      // Optional config
		      //'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>

		<h3>Body Ro</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'body_ro',
		      // Optional config
		      //'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>
		
			<h3>Body En</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'body_en',
		      // Optional config
		      'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>
	<?php echo $form->textFieldRow($model,'position',array('class'=>'span5')); ?>

	<h5>Date:</h5>
  	<?
  	$model->date = ($model->date && $model->date!='0000-00-00 00:00:00') ?$model->date:date('Y-m-d H:i:s');
  	 Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
            $this->widget ('CJuiDateTimePicker',  
                  array (
                      'language'=> 'ru',
                      'attribute'=>'date',
                      'model'=>$model,
                      // 'value' => date('Y-m-d H:i:s'),
                      'htmlOptions' => array(
                                          'class'=>'input-small',
                                          'style'=>'margin:0;width: 220px;',
                                          ),
                      'options'=>array (
                          'dateFormat' => 'yy-mm-dd',
                          'timeFormat' => 'hh:mm:ss',//' tt'
                          'showSecond'=>false,
                      ), 
                  )   
              );?>
              <br>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
