<?php
$this->breadcrumbs=array(
	'Production'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Production','url'=>array('index')),
	array('label'=>'Create Production','url'=>array('create')),
);
 
	// $desc = Categoryproduct::model()->findAll();
	// foreach($desc as $a){
	//   $filter_desc[$a->id] = $a->title_ru;
	// }

$cats = Categoryproduct::model()->findAll(array('condition'=>'status = 1','order'=>'position ASC'));
$filter_cats = array('Select Category');
$bufer = array();
$parentCategory = array();
	foreach ($cats as $key=>$val) {
		if(in_array($val->id, $bufer)) continue;
		if($val->parent) continue;
		$bufer[] = $val->id;
		$filter_cats[$val->id] = $val->title;
	     foreach ($cats as $k => $v) {
	        if($val->id!=$v->parent) continue;
	        $filter_cats[$v->id] = ' - '.$v->title;
	        $parentCategory[$val->id] = array('disabled'=>'disabled'); //disable parent category who have subcategories
		}
	}




Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('Production-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Production</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

 <?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'Production-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,

	'columns'=>array(
		'id',
		// 'title_ro',
		'title_ru',
		// 'category',

  		array(
	        'name'=>'category',
	        'header'=>'Категория',
	        'type'=>'raw',
	        'value'=>'$data->categoties["title_ru"]',
	        'filter' => $filter_cats ,
	        'htmlOptions'=>array('class'=>'span3') ,
  		),
		// 'title_en',
		// 'description_ro',
		'description_ru',
		// array('name'=>'studentRegNo',  'value'=>$data->categ->id),
		// array('name'=>'category',  'value'=>$data->categ->id), 
		/*
		'description_en',
		'body_ro',
		'body_ru',
		'body_en',
		'image',
		'category'
		'price_new',
		'price_old',
		'in_stoc',
		'tehnic_x',
		'price_list',
		'new_colection',
		'alias',
		'meta_title_ro',
		'meta_title_ru',
		'meta_title_en',
		'meta_description_ro',
		'meta_description_ru',
		'meta_description_en',
		'meta_keyword_ro',
		'meta_keyword_ru',
		'meta_keyword_en',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
