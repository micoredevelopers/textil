<?php
$this->breadcrumbs=array(
	'Production'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Production','url'=>array('index')),
	array('label'=>'Create Production','url'=>array('create')),
	array('label'=>'Update Production','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Production','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Production','url'=>array('admin')),
);
?>

<h1>View Production #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		// 'title_ro',
		'title_ru',
		// 'title_en',
		// 'description_ro',
		'description_ru',
		// 'description_en',
		// 'body_ro',
		'body_ru',
		// 'body_en',
		'image',
		'price_new',
		// 'price_old',
		'in_stoc',
		'zakaz',
		// 'tehnic_x',
		// 'price_list',
		'new_colection',
		'alias',
		// 'meta_title_ro',
		'meta_title_ru',
		// 'meta_title_en',
		// 'meta_description_ro',
		'meta_description_ru',
		// 'meta_description_en',
		// 'meta_keyword_ro',
		'meta_keyword_ru',
		// 'meta_keyword_en',
	),
)); ?>
