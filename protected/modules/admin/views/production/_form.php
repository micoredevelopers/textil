<?php
$arr_category = array();
$criteria = new CDbCriteria;
$criteria->condition = "parent = 0";
$cat = Categoryproduct::model()->findAll($criteria);
foreach ($cat as $key => $value) {
	$arr_category[$value->id] = $value->title;
	$criteria = new CDbCriteria;
	$criteria->condition = "parent = '$value->id'";
	if (Categoryproduct::model()->exists($criteria)) {
		$sub_cat = Categoryproduct::model()->findAll($criteria);
		foreach ($sub_cat as $kk => $vv) {
			$arr_category[$vv->id] = '- '.$vv->title;
		}
	}
}
$arr_orientation = array();
$ori = Orientation::model()->findAll();
foreach ($ori as $k => $v) {
	$arr_orientation[$v->id] = $v->title;
}


$prod_propp = array();
$prod_proppp = Production::model()->findAll();
foreach ($prod_proppp as $k => $v) {
	$prod_propp[$v->id] = $v->title;
}
?>


<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'Production-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype' => 'multipart/form-data'),
)); ?>
<?php
$filter_cats = array();
	$cats = Colors::model()->findAll();
	foreach($cats as $a){
	  $filter_cats[$a->id] = $a->colors;
	}
?>

<?php
$filter_size = array();
	$size = Size::model()->findAll();
	foreach($size as $a){
	  $filter_size[$a->id] = $a->size;
	}
?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php //echo $form->textFieldRow($model,'title_ro',array('class'=>'span5','maxlength'=>255,'labelOptions' => array('label' => 'Название Ro'))); ?>

	<?php echo $form->textFieldRow($model,'title_ru',array('class'=>'span5','maxlength'=>255,'labelOptions' => array('label' => 'Название Ru'))); ?>

	<?php //echo $form->textFieldRow($model,'title_en',array('class'=>'span5','maxlength'=>255,'labelOptions' => array('label' => 'Название En'))); ?>

	<!-- <label>Цвета </label> -->
	<?php /*echo Select2::activeMultiSelect($model,'colors',$filter_cats,
			array(
			'style'                =>'width:40%',
			'select2Options'       => array(
			'placeholder'          => 'Выберите свойства:',
			'maximumSelectionSize' => 8,
			),

        )
    ); */?>

    	<!-- <label>Размер </label> -->
	<?php /*echo Select2::activeMultiSelect($model,'size',$filter_size,
			array(
			'style'                =>'width:40%',
			'select2Options'       => array(
			'placeholder'          => 'Выберите свойства:',
			'maximumSelectionSize' => 8,
			),

        )
    );*/ ?>

        	<!-- <label>Produse Propuse </label> -->
	<?php /*echo Select2::activeMultiSelect($model,'prod_propus',$prod_propp,
			array(
			'style'                =>'width:40%',
			'select2Options'       => array(
			'placeholder'          => 'Выберите свойства:',
			'maximumSelectionSize' => 8,
			),

        )
    ); */?>


<h3>Краткое описание Ru</h3>



<?
$this->widget('ext.tinymce.TinyMce', array(
    'model' => $model,
    'attribute' => 'description_ru',
    // Optional config
    //'compressorRoute' => 'tinyMce/compressor',
    //'spellcheckerUrl' => array('tinyMce/spellchecker'),
    // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
    'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
    'fileManager' => array(
        'class' => 'ext.elFinder.TinyMceElFinder',
        'connectorRoute'=>'elfinder/connector',
    ),
));
?>


<h3>Описание Ru</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'body_ru',
		     'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>

<h3>Описание  цвета</h3>
<?= $form->textArea($model, 'color_description') ?>


	<? if($model->image){ ?>
    <label><input type="checkbox" name="delImg" value="1"> Удалить фото</label>
    <div class="list-view">
      <div class="items">
        <ul class="thumbnails">
          <li class="span3">
            <a href="#" class="thumbnail" rel="tooltip" data-title="news" data-toggle='modal' data-target='#myModal'>
                <img data-src="holder.js/300x200" src="<?php echo Yii::app()->request->baseUrl; ?>/images/Production/<?= $model->image ?>" alt="">
            </a>
          </li>
        </ul>
      </div>
    </div>
    <? } ?>
    <?php echo $form->fileFieldRow($model,'image',array('name'=>'image','labelOptions' => array('label' => 'Главная картинка'))); ?>

	<?php if ($model->id) {	?>
		<??>
			<h5>Фото товара:</h5>
		  	<iframe src="/blueimp/admin_version.php?id=<?= $model->id ?>&up_folder=/../_/i/Production_foto"
		    width="1000px" height="250" style="border: 0;">
		    </iframe>
	    <??>
    <? } ?>

     <?php echo $form->dropDownListRow($model,'category',$arr_category,array('labelOptions' => array('label' => 'Категория'))); ?>

	<?php echo $form->textFieldRow($model,'price_old',array('class'=>'span5','maxlength'=>255,'labelOptions' => array('label' => 'Старая цена'))); ?>
	<?php echo $form->textFieldRow($model,'price_new',array('class'=>'span5','maxlength'=>255,'labelOptions' => array('label' => 'Цена'))); ?>
<?php echo $form->dropDownListRow($model,'unitID',ProductUnits::getDataSelect(),array('labelOptions' => array('label' => 'Единица измерения'))); ?>
	<?php //echo $form->textFieldRow($model,'price_old',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo '<br>В наличии'.$form->checkboxRow($model,'in_stoc',array('value'=>0, 'uncheckValue'=>1 )); ?>


	<?php echo '<br>Под заказ'.$form->checkboxRow($model,'zakaz'); ?>

	<?php echo $form->textFieldRow($model,'articul'); ?>
	<?php echo $form->textFieldRow($model,'size'); ?>

	<?php echo '<br>'.$form->textFieldRow($model,'alias',array('class'=>'span5','maxlength'=>255)); ?>

	<?//------------------------META?>
	<br>
	<a href="javascript:;" onclick="$('#meta').toggle();">
		Meta
	</a>
	<div id="meta" style="display:none;">

	<?php //echo $form->textFieldRow($model,'meta_title_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_title_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'meta_title_en',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'meta_description_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_description_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'meta_description_en',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'meta_keyword_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_keyword_ru',array('class'=>'span5','maxlength'=>255)); ?>



	<?php //echo $form->textFieldRow($model,'meta_keyword_en',array('class'=>'span5','maxlength'=>255)); ?>
	</div>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
