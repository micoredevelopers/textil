<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<?php /* ?><b><?php echo CHtml::encode($data->getAttributeLabel('title_ro')); ?>:</b>
	<?php echo CHtml::encode($data->title_ro); ?><?php */ ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_ru')); ?>:</b>
	<?php echo CHtml::encode($data->title_ru); ?>
	<br />

	<?php /* ?><b><?php echo CHtml::encode($data->getAttributeLabel('title_en')); ?>:</b>
	<?php echo CHtml::encode($data->title_en); ?><?php */ ?>
	<br />

	<?php /* ?><b><?php echo CHtml::encode($data->getAttributeLabel('description_ro')); ?>:</b>
	<?php echo CHtml::encode($data->description_ro); ?><?php */ ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description_ru')); ?>:</b>
	<?php echo CHtml::encode($data->description_ru); ?>
	<br />

	<?php /* ?><b><?php echo CHtml::encode($data->getAttributeLabel('description_en')); ?>:</b>
	<?php echo CHtml::encode($data->description_en); ?><?php */ ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('body_ro')); ?>:</b>
	<?php echo CHtml::encode($data->body_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body_ru')); ?>:</b>
	<?php echo CHtml::encode($data->body_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body_en')); ?>:</b>
	<?php echo CHtml::encode($data->body_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price_new')); ?>:</b>
	<?php echo CHtml::encode($data->price_new); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price_old')); ?>:</b>
	<?php echo CHtml::encode($data->price_old); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('in_stoc')); ?>:</b>
	<?php echo CHtml::encode($data->in_stoc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tehnic_x')); ?>:</b>
	<?php echo CHtml::encode($data->tehnic_x); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price_list')); ?>:</b>
	<?php echo CHtml::encode($data->price_list); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('new_colection')); ?>:</b>
	<?php echo CHtml::encode($data->new_colection); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alias')); ?>:</b>
	<?php echo CHtml::encode($data->alias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_title_ro')); ?>:</b>
	<?php echo CHtml::encode($data->meta_title_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_title_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_title_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_title_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_title_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_description_ro')); ?>:</b>
	<?php echo CHtml::encode($data->meta_description_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_description_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_description_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_description_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_description_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keyword_ro')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keyword_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keyword_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keyword_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keyword_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keyword_en); ?>
	<br />

	*/ ?>

</div>