<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<?php /* ?><b><?php echo CHtml::encode($data->getAttributeLabel('title_ro')); ?>:</b>
	<?php echo CHtml::encode($data->title_ro); ?><?php */ ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_ru')); ?>:</b>
	<?php echo CHtml::encode($data->title_ru); ?>
	<br />

	<?php /* ?><b><?php echo CHtml::encode($data->getAttributeLabel('title_en')); ?>:</b>
	<?php echo CHtml::encode($data->title_en); ?><?php */ ?>
	<br />

	<?php /* ?><b><?php echo CHtml::encode($data->getAttributeLabel('body_ro')); ?>:</b>
	<?php echo CHtml::encode($data->body_ro); ?><?php */ ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body_ru')); ?>:</b>
	<?php echo CHtml::encode($data->body_ru); ?>
	<br />

	<?php /* ?><b><?php echo CHtml::encode($data->getAttributeLabel('body_en')); ?>:</b>
	<?php echo CHtml::encode($data->body_en); ?><?php */ ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alias')); ?>:</b>
	<?php echo CHtml::encode($data->alias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	*/ ?>

</div>