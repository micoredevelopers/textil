<?php
$this->breadcrumbs=array(
	'Articols'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Articols','url'=>array('index')),
	array('label'=>'Create Articols','url'=>array('create')),
	array('label'=>'View Articols','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Articols','url'=>array('admin')),
);
?>

<h1>Update Articols <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>