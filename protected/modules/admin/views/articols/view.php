<?php
$this->breadcrumbs=array(
	'Articols'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Articols','url'=>array('index')),
	array('label'=>'Create Articols','url'=>array('create')),
	array('label'=>'Update Articols','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Articols','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Articols','url'=>array('admin')),
);
?>

<h1>View Articols #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title_ro',
		'title_ru',
		'title_en',
		'description_ro',
		'description_ru',
		'description_en',
		'body_ro',
		'body_ru',
		'body_en',
		'image',
		'price_new',
		'price_old',
		'in_stoc',
		'tehnic_x',
		'price_list',
		'new_colection',
		'alias',
		'meta_title_ro',
		'meta_title_ru',
		'meta_title_en',
		'meta_description_ro',
		'meta_description_ru',
		'meta_description_en',
		'meta_keyword_ro',
		'meta_keyword_ru',
		'meta_keyword_en',
	),
)); ?>
