<?php
$this->breadcrumbs=array(
	'Articols'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Articols','url'=>array('index')),
	array('label'=>'Manage Articols','url'=>array('admin')),
);
?>

<h1>Create Articols</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>