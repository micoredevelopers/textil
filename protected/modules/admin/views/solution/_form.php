<?php
$arr_category = array();
$criteria = new CDbCriteria;
$criteria->condition = "parent = 0";
$cat = Categorysolution::model()->findAll($criteria);
foreach ($cat as $key => $value) {
	$arr_category[$value->id] = $value->title;
	$criteria = new CDbCriteria;
	$criteria->condition = "parent = '$value->id'";
	if (Categorysolution::model()->exists($criteria)) {
		$sub_cat = Categorysolution::model()->findAll($criteria);
		foreach ($sub_cat as $kk => $vv) {
			$arr_category[$vv->id] = '- '.$vv->title;
		}
	}
}
$arr_orientation = array();
$ori = Orientation::model()->findAll();
foreach ($ori as $k => $v) {
	$arr_orientation[$v->id] = $v->title;
}
?>


<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'Solution-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'title_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'title_en',array('class'=>'span5','maxlength'=>255)); ?>

<h3>Description Ru</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'description_ru',
		      // Optional config
		      //'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>

		<h3>Description Ro</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'description_ro',
		      // Optional config
		      //'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>
		
			<h3>Description En</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'description_en',
		      // Optional config
		      'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>

<h3>Body Ru</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'body_ru',
		      // Optional config
		      //'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>

		<h3>Body Ro</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'body_ro',
		      // Optional config
		      //'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>
		
			<h3>Body En</h3>
		<?
		  $this->widget('ext.tinymce.TinyMce', array(
		      'model' => $model,
		      'attribute' => 'body_en',
		      // Optional config
		      'compressorRoute' => 'tinyMce/compressor',
		      //'spellcheckerUrl' => array('tinyMce/spellchecker'),
		      // or use yandex spell: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
		      'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
		      'fileManager' => array(
		          'class' => 'ext.elFinder.TinyMceElFinder',
		          'connectorRoute'=>'elfinder/connector',
		      ),
		  ));
		?>

	<? if($model->image){ ?>
    <label><input type="checkbox" name="delImg" value="1"> Delete image</label>
    <div class="list-view">
      <div class="items">
        <ul class="thumbnails">
          <li class="span3">
            <a href="#" class="thumbnail" rel="tooltip" data-title="news" data-toggle='modal' data-target='#myModal'>
                <img data-src="holder.js/300x200" src="<?php echo Yii::app()->request->baseUrl; ?>/images/Solution/<?= $model->image ?>" alt="">
            </a>
          </li>
        </ul>
      </div>
    </div>
    <? } ?>
    <?php echo $form->fileFieldRow($model,'image',array('name'=>'image')); ?>

	<?php if ($model->id) {	?>
		<??>
			<h5>Photo of Museum:</h5>
		  	<iframe src="/blueimp/admin_version.php?id=<?= $model->id ?>&up_folder=solution" width="100%" height="250" style="border: 0; float:left">
		    </iframe>
	    <??>
    <? } ?>

    <?php echo $form->dropDownListRow($model,'orientation',$arr_orientation); ?>

    <?php echo $form->dropDownListRow($model,'category',$arr_category); ?>

	<?php echo $form->textFieldRow($model,'price_new',array('class'=>'span5','maxlength'=>255)); ?>

	<a href="javascript:;" onclick="$('#Solution_price_old').val($('#Solution_price_new').val()); $('#Solution_price_new').val('');">Add New Price</a>

	<?php echo $form->textFieldRow($model,'price_old',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->checkboxRow($model,'in_stoc'); ?>
	
	<?php echo $form->checkboxRow($model,'main_page'); ?>
	<?php //echo $form->textFieldRow($model,'in_stoc',array('class'=>'span5')); ?>

	<?php echo $form->checkboxRow($model,'new_colection'); ?>
	<?php //echo $form->textFieldRow($model,'new_colection',array('class'=>'span5')); ?>

	<? if($model->tehnic_x){ ?>
    <label><input type="checkbox" name="deltehnic_x" value="1"> Delete tehnic_x</label>
    <div class="list-view">
      <div class="items">
        <ul class="thumbnails">
          <li class="span3">
            <a href="#" class="thumbnail" rel="tooltip" data-title="news" data-toggle='modal' data-target='#myModal'>
                <img data-src="holder.js/300x200" src="<?php echo Yii::app()->request->baseUrl; ?>/upload/Solution/<?= $model->tehnic_x ?>" alt="">
            </a>
          </li>
        </ul>
      </div>
    </div>
    <? } ?>
    <?php echo $form->fileFieldRow($model,'tehnic_x',array('name'=>'tehnic_x')); ?>

	<?php //echo $form->textAreaRow($model,'tehnic_x',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<? if($model->price_list){ ?>
    <label><input type="checkbox" name="delprice_list" value="1"> Delete price_list</label>
    <div class="list-view">
      <div class="items">
        <ul class="thumbnails">
          <li class="span3">
            <a href="#" class="thumbnail" rel="tooltip" data-title="news" data-toggle='modal' data-target='#myModal'>
                <img data-src="holder.js/300x200" src="<?php echo Yii::app()->request->baseUrl; ?>/upload/Solution/<?= $model->price_list ?>" alt="">
            </a>
          </li>
        </ul>
      </div>
    </div>
    <? } ?>
    <?php echo $form->fileFieldRow($model,'price_list',array('name'=>'price_list')); ?>

	<?php //echo $form->textAreaRow($model,'price_list',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'alias',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?//------------------------META?>
	<br>
	<a href="javascript:;" onclick="$('#meta').toggle();">
		Meta
	</a>
	<div id="meta" style="display:none;">

	<?php echo $form->textFieldRow($model,'meta_title_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_title_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_title_en',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_description_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_description_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_description_en',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_keyword_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_keyword_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_keyword_en',array('class'=>'span5','maxlength'=>255)); ?>
	</div>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
