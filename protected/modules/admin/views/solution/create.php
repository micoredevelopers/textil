<?php
$this->breadcrumbs=array(
	'Solution'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Solution','url'=>array('index')),
	array('label'=>'Manage Solution','url'=>array('admin')),
);
?>

<h1>Create Solution</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>