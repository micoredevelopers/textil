<?php
$this->breadcrumbs=array(
	'Solution'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Solution','url'=>array('index')),
	array('label'=>'Create Solution','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('Solution-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Solution</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'solution-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'title_ro',
		'title_ru',
		'title_en',
		'description_ro',
		'description_ru',
		/*
		'description_en',
		'body_ro',
		'body_ru',
		'body_en',
		'image',
		'price_new',
		'price_old',
		'in_stoc',
		'tehnic_x',
		'price_list',
		'new_colection',
		'alias',
		'meta_title_ro',
		'meta_title_ru',
		'meta_title_en',
		'meta_description_ro',
		'meta_description_ru',
		'meta_description_en',
		'meta_keyword_ro',
		'meta_keyword_ru',
		'meta_keyword_en',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
