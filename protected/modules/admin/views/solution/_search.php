<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'title_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'title_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'title_en',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'description_ro',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'description_ru',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'description_en',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'body_ro',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'body_ru',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'body_en',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'image',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'price_new',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'price_old',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'in_stoc',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'tehnic_x',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'price_list',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'new_colection',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'alias',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_title_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_title_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_title_en',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_description_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_description_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_description_en',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_keyword_ro',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_keyword_ru',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meta_keyword_en',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
