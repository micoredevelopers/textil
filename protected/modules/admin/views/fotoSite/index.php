<?php
$this->breadcrumbs=array(
	'Foto Sites',
);

$this->menu=array(
	array('label'=>'Create FotoSite','url'=>array('create')),
	array('label'=>'Manage FotoSite','url'=>array('admin')),
);
?>

<h1>Foto Sites</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
