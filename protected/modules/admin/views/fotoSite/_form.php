<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'foto-site-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

	
	<? if($model->image){ ?>
    <label><input type="checkbox" name="delimage" value="1"> Delete image</label>
    <div class="list-view">
      <div class="items">
        <ul class="thumbnails">
          <li class="span3">
            <a href="#" class="thumbnail" rel="tooltip" data-title="news" data-toggle='modal' data-target='#myModal'>
                <img data-src="holder.js/300x200" src="<?php echo Yii::app()->request->baseUrl; ?>/images/FotoSite/<?= $model->image ?>" alt="">
            </a>
          </li>
        </ul>
      </div>
    </div>
    <? } ?>
<?php echo $form->fileFieldRow($model,'image',array('name'=>'image')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
