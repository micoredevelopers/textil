<?php
$this->breadcrumbs=array(
	'Foto Sites'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FotoSite','url'=>array('index')),
	array('label'=>'Manage FotoSite','url'=>array('admin')),
);
?>

<h1>Create FotoSite</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>