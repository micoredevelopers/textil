<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<?php /* ?><b><?php echo CHtml::encode($data->getAttributeLabel('title_en')); ?>:</b>
	<?php echo CHtml::encode($data->title_en); ?>
	<br /><?php */ ?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_ru')); ?>:</b>
	<?php echo CHtml::encode($data->title_ru); ?>
	<br />

	<?php /* ?><b><?php echo CHtml::encode($data->getAttributeLabel('title_ro')); ?>:</b>
	<?php echo CHtml::encode($data->title_ro); ?><?php */ ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('short_body_en')); ?>:</b>
	<?php echo CHtml::encode($data->short_body_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('short_body_ru')); ?>:</b>
	<?php echo CHtml::encode($data->short_body_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('short_body_ro')); ?>:</b>
	<?php echo CHtml::encode($data->short_body_ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alias')); ?>:</b>
	<?php echo CHtml::encode($data->alias); ?>
	<br />

	*/ ?>

</div>