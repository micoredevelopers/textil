<?php
$this->breadcrumbs=array(
	'Categoryproducts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Categoryproduct','url'=>array('index')),
	array('label'=>'Manage Categoryproduct','url'=>array('admin')),
);
?>

<h1>Create Categoryproduct</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>