<?php
$this->breadcrumbs=array(
	'Categoryproducts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Categoryproduct','url'=>array('index')),
	array('label'=>'Create Categoryproduct','url'=>array('create')),
	array('label'=>'View Categoryproduct','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Categoryproduct','url'=>array('admin')),
);
?>

<h1>Update Categoryproduct <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>