<?php
$this->breadcrumbs=array(
	'Categoryproducts',
);

$this->menu=array(
	array('label'=>'Create Categoryproduct','url'=>array('create')),
	array('label'=>'Manage Categoryproduct','url'=>array('admin')),
);
?>

<h1>Categoryproducts</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
