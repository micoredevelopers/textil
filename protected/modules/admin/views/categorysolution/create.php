<?php
$this->breadcrumbs=array(
	'Categorysolutions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Categorysolution','url'=>array('index')),
	array('label'=>'Manage Categorysolution','url'=>array('admin')),
);
?>

<h1>Create Categorysolution</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>