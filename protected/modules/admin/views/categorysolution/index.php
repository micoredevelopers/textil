<?php
$this->breadcrumbs=array(
	'Categorysolutions',
);

$this->menu=array(
	array('label'=>'Create Categorysolution','url'=>array('create')),
	array('label'=>'Manage Categorysolution','url'=>array('admin')),
);
?>

<h1>Categorysolutions</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
