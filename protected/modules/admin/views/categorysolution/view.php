<?php
$this->breadcrumbs=array(
	'Categorysolutions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Categorysolution','url'=>array('index')),
	array('label'=>'Create Categorysolution','url'=>array('create')),
	array('label'=>'Update Categorysolution','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Categorysolution','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Categorysolution','url'=>array('admin')),
);
?>

<h1>View Categorysolution #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title_ru',
		'title_ro',
		'title_en',
		'description_ro',
		'description_ru',
		'description_en',
		'alias',
		'parent',
		'status',
	),
)); ?>
