<?php
$this->breadcrumbs=array(
	'Categorysolutions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Categorysolution','url'=>array('index')),
	array('label'=>'Create Categorysolution','url'=>array('create')),
	array('label'=>'View Categorysolution','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Categorysolution','url'=>array('admin')),
);
?>

<h1>Update Categorysolution <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>