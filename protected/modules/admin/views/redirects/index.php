<?php

$this->breadcrumbs = array(
    'robots' => array('robots'),
    'Manage',
);

?>
<div class="span12">
    <h1>Редиректы</h1>

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'new-redirect-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype' => 'multipart/form-data'),
    )); ?>
<table class="table">
    <tr>
        <td>Переадресация с</td>
        <td>Переадресация на</td>
        <td>Статус</td>
        <td></td>
    </tr>
    <tr>
        <td>
            <?= $form->textArea($newModel,'[new]from_r',['labelOptions' => ["label" => false]]); ?>
        </td>
        <td>
            <?= $form->textArea($newModel,'[new]to_r',['labelOptions' => ["label" => false]]); ?>
        </td>
        <td>
            <?= $form->textArea($newModel,'[new]status',['labelOptions' => ["label" => false]]); ?>
        </td>
        <td>
          <div class="form-group">
            <button class="btn btn-success">Добавить</button>
          </div>
        </td>
    </tr>
</table>

    <?php $this->endWidget(); ?>

    <?php if ($models) :?>
        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
            'id'=>'update-redirect-form',
            'enableAjaxValidation'=>false,
            'htmlOptions'=>array('enctype' => 'multipart/form-data'),
        )); ?>


        <table class="table">
            <?php foreach ($models as $k=>$model) :?>

            <tr>
                <td>
                    <?= $form->textArea($model,'['.$k.']from_r',['labelOptions' => ["label" => false]]); ?>
                </td>
                <td>
                    <?= $form->textArea($model,'['.$k.']to_r',['labelOptions' => ["label" => false]]); ?>
                </td>
                <td>
                    <?= $form->textArea($model,'['.$k.']status',['labelOptions' => ["label" => false]]); ?>
                </td>
                <td>
                    <div class="form-group">
                        <a href="/admin/redirects/delete/?id=<?=$model->id?>"><button type="button" class="btn btn-danger">Удалить</button></a>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <div class="form-group">
            <button class="btn btn-success">Сохранить</button>
        </div>

        <?php $this->endWidget(); ?>
    <?php endif;?>

</div>
