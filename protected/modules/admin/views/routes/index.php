<?php

$this->breadcrumbs=array(
    'Routes'=>array('routes'),
    'Manage',
);

?>
<div class="span12">
<h1>Manage Pages</h1>


<form method="post">
    <div class="row">
    <?php foreach ($routes->getRoutes() as $item) :?>

    <div class="form-group col">
        <label><?=$item['label']?></label>
        <input class="form-control" type="text" name="routes[<?=$item['route']?>]" value="<?=$item['url']?>">
    </div>

    <?php endforeach; ?>
    </div>

    <div class="form-group">
        <button class="btn btn-success">Сохранить</button>
    </div>

</form>
</div>
