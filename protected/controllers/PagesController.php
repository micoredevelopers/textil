<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 04.12.2018
 * Time: 16:27
 */

class PagesController extends Controller
{

//    public function actionMain(){
//        $newProducts = CASFinder::find()->limit(8)->order('id DESC')->all();
//
//        $criteria = new CDbCriteria;
//        $criteria->condition = "status = 1";
//        $slider = Carousel_main_small::model()->findAll($criteria);
//
//        $criteria->condition = 'id IN (13,14,15,16,17)';
//        $photos = Foto::model()->findAll($criteria);
//
//        $contacts = ContactItem::findAll();
//        $data = [
//            'slider'=>$slider,
//            'newProducts' => $newProducts,
//            'photos' => $photos,
//            'contacts' => $contacts
//        ];
//        $this->_showPageInfo(31,'index');
//    }

    public function actionAbout(){
        $this->_showPageInfo(15);
    }

    public function actionDelivery(){
        $this->_showPageInfo(21);
    }

    public function actionContact(){
        $contacts = ContactItem::findAll();
        $criteria = new CDbCriteria;
        $criteria->condition = "status = 1";
        $slider = Carousel_main_small::model()->findAll($criteria);
        $this->_showPageInfo(24,['contacts'=>$contacts,'slider'=>$slider]);
    }


}