<?php

class MainController extends Controller
{


	public function actionIndex()
	{
	    $mainPageInfo = Pages::model()->findByPk(31);
        Yii::app()->metaTags->set($this,new MetaTagPage($mainPageInfo));

        $newProducts = CASFinder::find()->limit(8)->order('id DESC')->all();

        $criteria = new CDbCriteria;
        $criteria->condition = "status = 1";
        $slider = Carousel_main_small::model()->findAll($criteria);

        $criteria->condition = 'id IN (13,14,15,16,17,23)';
        $photos = Foto::model()->findAll($criteria);

        $contacts = ContactItem::findAll();

		$this->render('index', array(
			'slider'=>$slider,
            'page'=>$mainPageInfo,
            'newProducts' => $newProducts,
            'photos' => $photos,
            'contacts' => $contacts
			));
	}
}