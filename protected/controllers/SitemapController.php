<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 20.12.2018
 * Time: 10:26
 */

class SitemapController extends Controller
{

    public function actionXml(){
        header("Content-type: text/xml;charset=utf-8");
        $xmlCreator = new XmlCreator();
        $xmlCreator->addXmlArray(['loc'=>'','changefreq'=>'weekly','priority' => '0.5']);
        $xmlCreator->addXmlItemsList(new XmlItemStaticInterface());
        $xmlCreator->addXmlItemsList(new XmlItemCategory());
        $xmlCreator->addXmlItemsList(new XmlItemProduct());
        echo $xmlCreator->getXmlString();
        die();
    }

}