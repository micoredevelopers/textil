<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 05.12.2018
 * Time: 13:05
 */

class CatalogController extends Controller
{

    public function actionIndex()
    {
        $categories = ARFinder::find(Categoryproduct::model())->condition('parent=0 AND status=1')->order('title_ru ASC')->all();
        $this->_showPageInfo(18, ['categories' => $categories]);
    }

    public function actionCategory($alias)
    {
        $category = null;
        if (is_numeric($alias)) {
            $category = Categoryproduct::model()->findByPk($alias);
            if ($category && $category->alias) {
                return $this->redirect($this->createUrl('catalog/category', ['alias' => $category->alias]), 301);
            }
        } else {
            $category = ARFinder::find(Categoryproduct::model())->condition('alias="' . $alias . '"')->one();
        }
        if (!$category) {
            throw new CHttpException(404, 'К сожалению, такой страницы не существует');
        }
        Yii::app()->metaTags->set($this, new MetaTagCategory($category));
        $childrenCategory = CASFinder::find()->order('t.title_ru ASC')->condition('parent=' . $category->id)->all();
        if ($category->produ) {
            $childrenCategory[] = $category;
        }
        $allParentsCategories = ARFinder::find(Categoryproduct::model())->condition('parent=0 AND status=1')->order('id DESC')->all();
        $this->render('category', [
            'category' => $category,
            'childrenCategory' => $childrenCategory,
            'allParentsCategories' => $allParentsCategories
        ]);
    }


    public function actionProduct($alias)
    {
		 if (is_numeric($alias)) {
            $categoryInfo = CASFinder::find()->condition('`t`.`id`=' . (int)$alias)->one();
			if ($categoryInfo && $categoryInfo->alias) {
                return $this->redirect($this->createUrl('catalog/product', ['alias' => $categoryInfo->alias]), 301);
            }
        } else {
            $categoryInfo = CASFinder::find()->condition('`t`.`alias`="' . $alias . '"')->one();
        }
        if (!$categoryInfo || !$categoryInfo->produ) {
            throw new CHttpException(404, 'К сожалению, такой страницы не существует');
        }

        Yii::app()->metaTags->set($this, new MetaTagCategoryProduct($categoryInfo));
        $allParentsCategories = ARFinder::find(Categoryproduct::model())->condition('parent=0 AND status=1')->order('position ASC')->all();
        $newProducts = CASFinder::find()->limit(8)->order('id DESC')->all();
        $this->render('product', [
            'categoryInfo' => $categoryInfo,
            'allParentsCategories' => $allParentsCategories,
            'newProducts' => $newProducts
        ]);
    }

    public function actionSearch()
    {
        $searchString = Yii::app()->request->getParam('q');
        if (!is_string($searchString)) {
            throw new CHttpException(404, 'К сожалению, такой страницы не существует');
        }
        Yii::app()->metaTags->set($this,new MetaTagStatic());
        $searchCondition = SearchConditionBuilder::instance($searchString, [
            'title_ru', 'title_ro', 'title_en', 'description_ro', 'description_ru', 'description_en', 'produ.title_ru'
        ])->setAlias('t.')->getCondition();
        $finder = CASFinder::find(Categoryproduct::model())->condition('(' . $searchCondition . ') AND status=1 AND produ.id IS NOT NULL');
        $categories = $finder->all();
        $pages = new CPagination(count($categories));
        $pages->pageSize = 8;
        $paginator = new \JasonGrimes\Paginator(count($categories), 8, ($pages->getCurrentPage()+1), Yii::app()->createUrl('catalog/search') . '?q=' . $searchString.'&'.$pages->pageVar.'=(:num)');
        $paginator->setMaxPagesToShow(5);
        $categories = array_splice($categories, $pages->getOffset(), $pages->getLimit());
      //  dump($categories); die();
        $this->render('search', [
            'searchString' => $searchString,
            'categories' => $categories,
            'pageLinks' => $paginator->getPages(),
            'nextUrl' => $paginator->getNextUrl(),
            'previousUrl' => $paginator->getPrevUrl()
        ]);
    }


}