<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 13.12.2018
 * Time: 13:23
 */

class CartController extends Controller
{


    public function actionIndex(){


       $cart = $this->cart();
       Yii::app()->metaTags->set($this,new MetaTagStatic());
       return $this->render('index',[
           'items' => $cart->getItems(),
           'total' => $cart->total()
       ]);
    }

    public function actionAdd(){
        if (!Yii::app()->request->getIsAjaxRequest() || !Yii::app()->request->getIsPostRequest()){
            throw new CHttpException(404,'К сожалению, такой страницы не существует');
        }
        $product = $this->findProductByID();
        $cartItemProduct = CartItemProduct::instanceByModel($product,$this->productQ());
        $cart = new Cart(new CartStorageSession());
        $cart->addItem($cartItemProduct);

        $this->sendJson('success', $this->cartProduct());

    }

    public function actionAdd_order(){
        if (!Yii::app()->request->getIsAjaxRequest() || !Yii::app()->request->getIsPostRequest()){
            throw new CHttpException(404,'К сожалению, такой страницы не существует');
        }
        $cartInstance = $this->cart();
        $cart = $this->cartItemsByArray($cartInstance->getItems());
        $products = serialize($cart);
        $pricetotal = $cartInstance->total();
        $telefon = $_POST['phone'];
        $email = $_POST['email'];
        $adres = $_POST['city'];
        $name = $_POST['username'];

       
        if ($_POST['comment'] == null) {
            $comment = 'no comment';
        }else{
            $comment = $_POST['comment'];
        }
        $model = new Orders;
        $model->name = $name;
        $model->date = new CDbExpression('NOW()');
        $model->ip = Yii::app()->request->getUserHostAddress();
        $model->address = $adres;
        $model->order = $products;
        $model->price = $pricetotal;
        $model->status = 0;

        $model->phone_contacts = $telefon;
        $model->paymant = 'not_paid';
        $model->comment_user = $comment;
        $model->user_email = $email;
        $ecommerce = [
            'productCart' => [],
            'actions' => []
        ];
        if ($model->save()) {



            if ($cart) {
                foreach ($cart as $item) {
                    $ecommerceProductCartInstance = new ECommerceProductCart($item);
                    $ecommerce['productCart'][] = [
                        'type' => $ecommerceProductCartInstance->getType(),
                        'data' => $ecommerceProductCartInstance->getData()
                    ];
                }
                $ecommerce['actions'][] = [
                    'action' => 'purchase',
                    'data' => [
                        'id' => $model->id
                    ]
                ];
              //  Yii::app()->ECommerce->addAction('purchase', ['id' => $model->id]);
            }

            $to = 'textile-international@ukr.net';
            //$to2 = 'site-uah@mail.ru';
            // $to = 'serbenco@febian.md';
            // $to2 = 'serbenco@febian.md';
            $subject = Yii::t('mail', 'Новыи заказ');
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html;  charset=UTF-8' . "\r\n";
            $headers .= 'From: TEXTIL SRL' . " \r\n";
            $headers .= 'X-Mailer: PHP' . phpversion();

            $message = '<html><head></head><body><table><tr style="background-color: #C7C7C7;">';
            $message .= '<td></td><td>Товар</td><td>ID Товара</td><td>Количество</td><td>Сумма</td></tr>';
            foreach ($cart as $key => $value) {
                $crit1 = new CDbCriteria();
                $crit1->condition = 'id = :code';
                $crit1->params = array(':code' => $value['id']);
                $item = Production::model()->find($crit1);
                $message .= '<tr>';

                $message .= '<td style="border: 1px solid;border-spacing:0;text-aligh:center"><img src="' . Yii::app()->getBaseUrl(true) . '/images/Production/' . $item->image . '" width="80" height="80"/></td>';

                $message .= '<td style="border: 1px solid;border-spacing:0;text-aligh:center">' . $value['title'] . '</td>';
                $message .= '<td style="border: 1px solid;border-spacing:0;text-aligh:center">' . $value['id'] . '</td>';
                $message .= '<td style="border: 1px solid;border-spacing:0;text-aligh:center">' . $value['count'] . '</td>';
                $message .= '<td style="border: 1px solid;border-spacing:0;text-aligh:center">' . ($value['price'] * $value['count']) . ' usd</td>';


                $message .= '</tr>';
            }
            $message .= '<tr style="background-color: #C7C7C7;"><td>Total</td><td></td><td></td><td></td><td>' . $pricetotal . ' usd</td></tr>';
            $message .= '</table></body></html>';


            $message .= '<table>
			<tr>
				<td>
					Имя:
				</td>
				<td>
				' . $name . '
				</td>
			</tr>
			<tr>
				<td>
					Телефон:
				</td>
				<td>
				' . $telefon . '
				</td>
			</tr>
			<tr>
				<td>
					Email:
				</td>
				<td>
				' . $email . '
				</td>
			</tr>
			<tr>
				<td>
					Коментарии:
				</td>
				<td>
				' . $comment . '
				</td>
			</tr>


			</table>';
            Yii::import('application.extensions.phpmailer.JPhpMailer');
       
     $mail = new JPhpMailer;
            // $mail->IsSMTP();
            $mail->CharSet = 'UTF-8';
            // $mail->Host = "mail.ukraine.com.ua";
            // $mail->Host = "smtp.mail.ru";
            $mail->SMTPAuth = false;
            // $mail->Username = 'textil.international@mail.ru';
            $mail->Username = 'textile-international@ukr.net';
            // $mail->Password = 'N-Lq_B9.DP=aTABh';
            $mail->Password = 'N-Lq_B9.DP=aTABh';
            $mail->SMTPSecure = 'ssl';
            // $mail->Port = 465;
            $mail->SetFrom('textile-international@ukr.net', 'Textil');
            // $mail->SetFrom('textil.international@mail.ru', 'Textil');
            $mail->Subject = 'Новыи заказ';
            $mail->MsgHTML($message);
            $mail->AddAddress($to, $name);
            // $mail->AddAddress($to2,$name);
            $mail->Send();

            $mail = new JPhpMailer;
            // $mail->IsSMTP();
            $mail->CharSet = 'UTF-8';
            // $mail->Host = "mail.ukraine.com.ua";
            // $mail->Host = "smtp.mail.ru";
            $mail->SMTPAuth = false;
            // $mail->Username = 'textil.international@mail.ru';
            $mail->Username = 'textile-international@ukr.net';
            // $mail->Password = 'N-Lq_B9.DP=aTABh';
            $mail->Password = 'N-Lq_B9.DP=aTABh';
            $mail->SMTPSecure = 'ssl';
            // $mail->Port = 465;
            $mail->SetFrom('textile-international@ukr.net', 'Textil');
            // $mail->SetFrom('textil.international@mail.ru', 'Textil');
            $mail->Subject = 'Новыи заказ';
            $mail->MsgHTML($message);
            // $mail->AddAddress($to,$name);
            $mail->AddAddress($to, $name);
            $mail->Send();
            mail($to, $subject, $message, $headers);
            //mail($to2, $subject, $message, $headers);
            //mail to user
            $to = $email;
            $subject = Yii::t('mail', 'Вы заказали');
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= "Content-type: text/html; charset=UTF-8" . " \r\n";
            $headers .= "From: TEXTIL SRL" . " \r\n";
            $message = '<table><tr style="background-color: #C7C7C7;">';
            $message .= '<td></td><td>Товар</td><td>ID Товара</td><td>Количество</td><td>Сумма</td></tr>';
            foreach ($cart as $key => $value) {
                $crit1 = new CDbCriteria();
                $crit1->condition = 'id = :code';
                $crit1->params = array(':code' => $value['id']);
                $item = Production::model()->find($crit1);
                $message .= '<tr>';

                $message .= '<td style="border: 1px solid;border-spacing:0;text-aligh:center"><img src="' . Yii::app()->getBaseUrl(true) . '/images/Production/' . $item->image . '" width="80" height="80"/></td>';
                $message .= '<td style="border: 1px solid;border-spacing:0;text-aligh:center">' . $value['title'] . '</td>';
                $message .= '<td style="border: 1px solid;border-spacing:0;text-aligh:center">' . $value['id'] . '</td>';
                $message .= '<td style="border: 1px solid;border-spacing:0;text-aligh:center">' . $value['count'] . '</td>';
                $message .= '<td style="border: 1px solid;border-spacing:0;text-aligh:center">' . ($value['price'] * $value['count']) . ' usd</td>';
                $message .= '</tr>';
            }
            $message .= '<tr style="background-color: #C7C7C7;"><td>Total</td><td></td><td></td><td></td><td>' . $pricetotal . ' usd</td></tr>';
            $message .= '</table>';
            $message .= '<table>
			<tr>
				<td>
					Имя:
				</td>
				<td>
				' . $name . '
				</td>
			</tr>
			<tr>
				<td>
					Телефон:
				</td>
				<td>
				' . $telefon . '
				</td>
			</tr>
			<tr>
				<td>
					Email:
				</td>
				<td>
				' . $email . '
				</td>
			</tr>
			<tr>
				<td>
					Коментарии:
				</td>
				<td>
				' . $comment . '
				</td>
			</tr>


			</table>';

// Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            // $mail->IsSMTP();
            $mail->CharSet = 'UTF-8';
            // $mail->Host = "smtp.mail.ru";
            $mail->SMTPAuth = false;
            $mail->Username = 'textile-international@ukr.net';
            $mail->Password = 'N-Lq_B9.DP=aTABh';
            // $mail->SMTPSecure = 'ssl';
            // $mail->Port = 465;
            $mail->SetFrom('textile-international@ukr.net', 'Textil');
            $mail->Subject = 'Вы заказали';
            $mail->MsgHTML($message);
            $mail->AddAddress($to, $name);
            $mail->Send();

            (new CartStorageSession())->removeItems();
            mail($to, $subject, $message, $headers);
        }
        $this->sendJson('success',['ecommerce'=>$ecommerce]);

    }


    public function actionDelete(){
        if (!Yii::app()->request->getIsAjaxRequest() || !Yii::app()->request->getIsPostRequest()){
            throw new CHttpException(404,'К сожалению, такой страницы не существует');
        }

        $product = $this->findProductByID();
        $cartItemProduct = CartItemProduct::instanceByModel($product);
        (new CartStorageSession())->deleteItem($cartItemProduct);
        $this->sendJson('success', $this->cartProduct());
    }

    /**
     * @param CartListItem[] $cartItems
     * @return array
     */
    public function cartItemsByArray($cartItems=[]){
        $result = [];
        if ($cartItems){
            foreach ($cartItems as $item){
                $result[]=[
                    'id' => $item->getItem()->getID(),
                    'count' => $item->getQ(),
                    'price' => $item->getPriceSum(),
                    'title' => $item->getItem()->getTitle()
                ];
            }
        }
        return $result;
    }

    public function cartProduct(){
        $cartList = $this->cart();
        $cartProduct = $cartList -> itemByID($this->productID());
        if (!$cartProduct){
            $product = $this->findProductByID();
            $cartItemProduct = CartItemProduct::instanceByModel($product,1);
            $cartProduct = new CartListItem($cartItemProduct, new CartStorageSession());
        }
        return [
            'id' => $cartProduct->getItem()->getID(),
            'title' =>  $cartProduct->getItem()->getTitle(),
            'img' => $cartProduct->getItem()->getImg(),
            'price' => $cartProduct->getPrice(),
            'q' => $cartProduct->getQ(),
            'priceSum' => $cartProduct->getPriceSum(),
            'total' => $cartList->total(),
            'totalUniqueQ' => $cartList->totalUniqueQ()
        ];
    }


    public function cart(){
        $sessionStorage = new CartStorageSession();
        $products = CartItemProduct::instancesByStorage($sessionStorage);
        return (new CartList($sessionStorage))->addItemList($products);
    }

    /**
     * @return int
     */
    public function productID(){
        return (int)Yii::app()->request->getPost('id');
    }

    /**
     * @return int
     */
    public function productQ(){
     //   $q = (int) Yii::app()->request->getPost('count',1);
        return (int) Yii::app()->request->getPost('count',1);
    }

    /**
     * @return CActiveRecord
     */
    public function findProductByID(){
        if (!$product = Production::model()->findByPk($this->productID())){
            $this->sendJson('error',[],'Product is not exist!');
        }
        return $product;
    }

    /**
     * @param $status
     * @param array $data
     * @param string $msq
     * @return false|string
     */
    public function sendJson($status, $data = [], $msq=''){
         echo json_encode([
             'status' => $status,
             'data' => $data,
             'msq' => $msq
         ],JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
         die();
    }

}
