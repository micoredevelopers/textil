<?php

class m190116_140717_update_catalog_description extends CDbMigration
{



    protected $descriptionNEW = '<h2 class="goods-catalog-header-title">Наши товары</h2><p>Вы всегда можете пощупать у нас в магазине</p><p>Предлагаем заказать текстиль оптом, который понравится не только по стоимости, но и качеству. Сотрудники компании придирчиво относятся к выбору продукции и строго изучают каждый образец. Перед отправкой покупателю товар проверяется на наличие брака. Вот почему гарантируем высокое качество продукции.</p>';

    protected $descriptionOLD = '<h2 class="goods-catalog-header-title">Наши товары</h2>
            <p>Вы всегда можете пощумать у нас в магазине</p>
            <p>
                Buiten het reguliere assortiment op Schiphol vind je bij See Buy Fly ook een selectie aan speciale producten, gemaakt voor de reiziger. Dit zijn producten die net even anders zijn dan wat je in de winkelstraat vindt. Met nu €5,- extra shopkorting! Download jouw korting hieronder.
            </p>';

	public function safeUp()
	{
	    $this->update('pages',['body_ru'=>$this->descriptionNEW],'id=18');
	}

	public function safeDown()
	{
        $this->update('pages',['body_ru'=>$this->descriptionOLD],'id=18');
	}

}