<?php

class m190219_122019_add_columns_to_production_table extends CDbMigration
{

//
//    public function columns(){
//        return [
//            'color_description', ['text','null'],
//        ];
//    }

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	    $this->addColumn('production', 'color_description','text null');
        $this->addColumn('production', 'unitID','integer null');
	}

	public function safeDown()
	{
        $this->dropColumn('production', 'color_description');
        $this->dropColumn('production', 'unitID');
	}

}