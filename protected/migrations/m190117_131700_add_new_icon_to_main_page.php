<?php

class m190117_131700_add_new_icon_to_main_page extends CDbMigration
{
	public function up()
	{
	    $this->delete('foto','id=23');
	    $this->insert('foto',[
	        'id' => 23,
            'title_ru' => '25 лет на рынке',
            'description' => 'Доверие клиентов на протяжении многих лет'
        ]);
	}

	public function down()
	{
		echo "m190117_131700_add_new_icon_to_main_page does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}