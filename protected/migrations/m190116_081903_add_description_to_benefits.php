<?php

class m190116_081903_add_description_to_benefits extends CDbMigration
{
//	public function up()
//	{
//	    $data = [
//	        [
//	            'id' => 13,
//                'description' => 'Наша компания гарантирует Вам качество товара, а так же у нас возможно сделать возврат товара.'
//            ],
//            [
//                'id' => 14,
//                'description' => 'Мы отправляем ткань по территории всей Украины Новой Почтой, а так же Интайм.'
//            ],
//            [
//                'id' => 15,
//                'description' => 'Текстиль Интернешнл доставляет товар в обговоренные с клиентом сроки.'
//            ],
//            [
//                'id' => 16,
//                'description' => 'На данный момент у нас самый большой ассортимент ткани в Украине.'
//            ],
//            [
//                'id' => 17,
//                'description' => 'Большой опыт работы в сфере ткани.'
//            ],
//            [
//                'id' => 23,
//                'description' => ''
//            ]
//        ];
//	    foreach ($data as $item){
//	        $this->update('foto',$item,'id='.$item['id']);
//        }
//	}
//
//	public function down()
//	{
//		echo "m190116_081903_add_description_to_benefits does not support migration down.\n";
//		return false;
//	}


	public function safeUp()
	{
        $data = [
            [
                'id' => 13,
                'description' => 'Наша компания гарантирует Вам качество товара, а так же у нас возможно сделать возврат товара.'
            ],
            [
                'id' => 14,
                'description' => 'Мы отправляем ткань по территории всей Украины Новой Почтой, а так же Интайм.'
            ],
            [
                'id' => 15,
                'description' => 'Текстиль Интернешнл доставляет товар в обговоренные с клиентом сроки.'
            ],
            [
                'id' => 16,
                'description' => 'Большой опыт работы в сфере ткани.'
            ],
            [
                'id' => 17,
                'description' => 'На данный момент у нас самый большой ассортимент ткани в Украине.'
            ],
            [
                'id' => 23,
                'description' => 'Доверие клиентов на протяжении многих лет'
            ]
        ];
        foreach ($data as $item){
            $this->update('foto',$item,'id='.$item['id']);
        }
	}

	public function safeDown()
	{
	}

}