<?php
 
// this file must be stored in:
// protected/components/WebUser.php
 
class WebUser extends CWebUser {
 
  // Store model to not repeat query.
  private $_model;
  // Return first name.
  // access it by Yii::app()->user->first_name
#  function getFirst_Name(){
#    $user = $this->loadUser(Yii::app()->user->id);
#    return $user->first_name;
#  }
 
  // This is a function that checks the field 'role'
  // in the User model to be equal to 1, that means it's admin
  // access it by Yii::app()->user->isAdmin()
  function isActiv(){
    if(!Yii::app()->user->id) return false;
    $user = $this->loadUser(Yii::app()->user->id);
    return $user->status == 'active';
  }
  function isAdmin(){
    if(!Yii::app()->user->id) return false;
    $user = $this->loadUser(Yii::app()->user->id);
    return intval($user->admin) == 1;
  }
  function isCanNews(){
    if(!Yii::app()->user->id) return false;
    $user = $this->loadUser(Yii::app()->user->id);
    return intval($user->can_news) == 1;
  }
  function isCanArticles(){
    if(!Yii::app()->user->id) return false;
    $user = $this->loadUser(Yii::app()->user->id);
    return intval($user->can_articles) == 1;
  }
  function isCanCatalog(){
    if(!Yii::app()->user->id) return false;
    $user = $this->loadUser(Yii::app()->user->id);
    return intval($user->can_phones) == 1;
  }
  // Load user model.
  protected function loadUser($id=null)
    {
        if($this->_model===null)
        {
            if($id!==null)
                $this->_model=User::model()->findByPk($id);
        }
        return $this->_model;
    }
}
?>
