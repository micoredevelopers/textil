<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 04.12.2018
 * Time: 13:40
 */

interface NavigationMenuItemInterface
{

    /**
     * @return string
     */
    public function getLabel();

    /**
     * @return string
     */
    public function getUrl();

    /**
     * @return bool
     */
    public function inFooter();

    /**
     * @return bool
     */
    public function inHeader();

}