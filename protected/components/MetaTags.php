<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 12.11.2018
 * Time: 11:27
 */

class MetaTags
{

    public function init(){}
    /**
     * @var Controller
     */
    protected $controllerInstance;

    /**
     * @var MetaTagInterface
     */
    protected $metaTagInstance;

    /**
     * s
     */
    protected function _set(){

        foreach (['title','description','keywords'] as $attr){
            $method = 'get'.$attr;
            if (!$this->controllerInstance->{'meta'.$attr}){
                $this->controllerInstance->{'meta'.$attr} = str_replace('"',"'",$this->metaTagInstance->$method());
//               / dump($this->metaTagInstance->$method());
            }
        }

//        $this->controllerInstance->metatitle = $this->metaTagInstance->getTitle();
//        $this->controllerInstance->metadescription = $this->metaTagInstance->getDescription();
//        $this->controllerInstance->metakeywords = $this->metaTagInstance->getKeywords();
    }

    /**
     * @param Controller $controller
     * @param MetaTagInterface $metaTag
     */
    public function set(Controller $controller,MetaTagInterface $metaTag){
         $this->controllerInstance = $controller;
         $this->metaTagInstance = $metaTag;
         $this->_set();
    }

    public function getH1(){
        return $this->metaTagInstance ? $this->metaTagInstance->getH1() : '';
    }

}