<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 08.11.2018
 * Time: 09:25.
 */
class Canonical extends CComponent
{
    /**
     * @var array
     */
    protected $rules = [];

    public function init()
    {
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['catalog/index'], [$this, 'home']],
            [['news/item'], [$this, 'other'], ['news/index']],
        ];
    }

    /**
     * @param null $route
     *
     * @return mixed
     */
    public function ruleByRoute($route = null)
    {
        if ($route === null) {
            $route = Yii::app()->controller->getRoute();
        }
        if (!isset($this->rules[$route])) {
            $this->rules[$route] = [
                $route, [$this, 'general'], [],
            ];
            foreach ($this->rules() as $rule) {
                foreach ($rule[0] as $r) {
                    if ($r == $route) {
                        $this->rules[$route] = [
                            $route, $rule[1], isset($rule[2]) ? $rule[2] : [],
                        ];

                        return $this->rules[$route];
                    }
                }
            }
        }

        return $this->rules[$route];
    }

    /**
     * @return mixed
     */
    public function canonicalUrl()
    {
        // phpinfo();
        // die();
        $rule = $this->ruleByRoute();

        return call_user_func($rule[1], $rule[0], ...$rule[2]);
    }

    /**
     * @param $route
     *
     * @return mixed
     */
    public function general()
    {
        return Yii::app()->request->getUrl();
    }

    /**
     * @param $route
     * @param $userRoute
     *
     * @return mixed
     */
    public function other($route, $userRoute)
    {
        return Yii::app()->createUrl($userRoute);
    }

    public function home()
    {
        return '';
    }

    public function metaTag()
    {
        Yii::app()->clientScript->registerLinkTag('canonical', null, Yii::app()->request->getBaseUrl(true).$this->canonicalUrl());
    }
}
