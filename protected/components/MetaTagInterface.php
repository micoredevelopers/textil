<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 12.11.2018
 * Time: 11:33
 */

interface MetaTagInterface
{

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string
     */
    public function getKeywords();

    /**
     * @return string
     */
    public function getH1();

}