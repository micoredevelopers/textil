<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 20.12.2018
 * Time: 12:49
 */

class XmlItemProduct extends CActiveRecord implements XmlItemsListInterface,XmlItemInterface
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'categoryproduct';
    }

    /**
     * @return string
     */
    public function getLoc()
    {
        $u = $this->alias ?  $this->alias : $this->id;
        return Yii::app()->createUrl('catalog/product',['alias'=>$u]);
    }

    /**
     * @return string
     */
    public function getChangefreq()
    {
        return 'weekly';
    }

    /**
     * @return string
     */
    public function getPriority()
    {
        return 0.5;
    }

    /**
     * @return false|string
     */
    public function getLastmod()
    {
        // TODO: Implement getLastmod() method.
    }

    /**
     * @return mixed
     */
    public static function findXmlItems()
    {
        $chailds = ARFinder::find(static::model())->condition('parent != 0')->all();
        $parents = ARFinder::find(static::model())
            ->join('LEFT JOIN production ON production.category = t.id')
            ->condition('parent = 0 AND production.id IS NOT NULL')
            ->groupBy('t.id')
            ->all();
        return array_merge($chailds,$parents);
    }

}