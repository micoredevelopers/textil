<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 14.11.18
 * Time: 22:47
 */

interface ECommerceItemInterface
{

    /**
     * @return string
     */
    public function getType();

    /**
     * @return array
     */
    public function getData();

}