<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 09.11.2018
 * Time: 10:01
 */

abstract class XmlItemAbstract implements XmlItemInterface
{

    /**
     * @return string
     */
    abstract function getLoc();

    /**
     * @return string
     */
    function getChangefreq() {
         return 'weekly';
    }


    /**
     * @return string
     */
    function getPriority() {
        return '0.5';
    }

    /**
     * @return false|string
     */
    function getLastmod() {
        return date('Y-m-d');
    }

}