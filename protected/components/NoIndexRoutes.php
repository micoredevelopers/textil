<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 07.11.2018
 * Time: 15:09
 */

class NoIndexRoutes extends CComponent
{

    public function init(){

    }

    /**
     * @return array
     */
   public function routes(){
       return [
           [['cart/index','catalog/search','send_email/index'],[$this,'alwaysClose']]
       ];
   }

    /**
     * @return bool
     */
   public function isThis(){

       foreach ($this->routes() as $route){

           foreach ($route[0] as $r){
               if ($r == Yii::app()->controller->getRoute()){
                   return call_user_func($route[1],$r);
               }
           }
      }

       return false;
   }

    /**
     * @return bool
     */
   public function alwaysClose (){
       return true;
   }

   public function metaTag(){
       if ($this->isThis()){
           return '<meta name="robots" content="noindex,follow">';
       }
       return '';
   }

}