<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 13.11.2018
 * Time: 10:45
 */

abstract class MetaTagsModel implements MetaTagInterface
{

    /**
     * @var Category
     */
    protected $model;

    /**
     * MetaTagCategory constructor.
     * @param Category $category
     */
    public function __construct(ARMetaTagsInterface $model)
    {
        $this->model = $model;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getTagsByAttr('title');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getTagsByAttr('description');
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->getTagsByAttr('keywords');
    }

    public function getH1(){
        return $this->getTagsByAttr('h1','getH1');
    }


    public function getTagsByAttr($attr,$method=null){
        if (!$method) {
            $method = 'getMeta' . $attr;
        }
        $text = $this->model->$method();
        if (!$text){
            $text = $this->getTemplate()[$attr];
        }
        foreach ($this->getVariables() as $var=>$value){
            $text = str_replace('{'.$var.'}',$value,$text);
        }
        return $text;
    }


    abstract public function getVariables();

    abstract public function getTemplate();

}