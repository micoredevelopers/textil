<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 07.11.2018
 * Time: 11:42
 */

class Redirects extends CComponent
{

    public function init()
    {
        $this->runRedirects();
    }

    /**
     * @return HttpRequest
     */
    public function request()
    {
        return Yii::app()->request;
    }

    /**
     * @return string
     */
    public function baseUrl()
    {
        return $this->request()->getBaseUrl(true);
    }

    /**
     * @return string
     */
    public function url()
    {
        return $this->request()->getUrl();
    }

    /**
     * @return string
     */
    public function fullUrl()
    {
        return $this->baseUrl() . urldecode($this->url());
    }

    /**
     * @param $pattern
     * @param $replace
     * @return null|string|string[]
     */
    public function baseUrlReplace($pattern, $replace)
    {
        return preg_replace($pattern, $replace, $this->baseUrl());
    }

    /**
     * @param $pattern
     * @return false|int
     */
    public function baseUrlMatch($pattern)
    {
        return preg_match($pattern, $this->baseUrl());
    }

    /**
     * @param $pattern
     * @param $replace
     * @return null|string|string[]
     */
    public function fullUrlReplace($pattern, $replace)
    {
        return preg_replace($pattern, $replace, $this->fullUrl());
    }

    /**
     * @param $pattern
     * @return false|int
     */
    public function fullUrlMatch($pattern)
    {
       // dump($this->fullUrl(),1);
        return preg_match($pattern, $this->fullUrl());
    }

    /**
     * @param $pattern
     * @return false|int
     */
    public function fullUrlEq($pattern)
    {
        return $pattern === $this->fullUrl();
    }

    /**
     * @param $url
     * @param int $statusCode
     */
    public function redirect($url, $statusCode = 301)
    {
        $this->request()->redirect($url, $statusCode);
        $this->request()->send();
    }


    public function redirectRules()
    {

        $redirects =[
            ['/^(http:\/\/|https:\/\/)www\./', 'https://', 301], //without www
            ['/\/index.php(.*)?/', '$1', 301], //without index.php
            ['/([^\/])$/', '$1/'],  //with "/" in the end
            ['/U/','u'],
            ['/([A-Z])/', '\U1$1'],  // to lower case
            ['/.html/', ''],        // without ".html"
            ['/\/ru\//','/'],
            ['/(\/podzakaz\/)|(\/optom\/)|(\/news\/)/',''],

        ];

        if ($userRedirects = RedirectsModel::model()->findAll()){
            foreach ($userRedirects as $model){
                $redirects[] = [
                   $model->from_r,$model->to_r,$model->status
                ];
            }
        }

        return $redirects;
    }

    public function exceptions()
    {
        return [
            '/(\?q=)(.*)/', '/\/admin\//',
            '/\.js/','/\.css/','/\.woff/','/\.ttf/',
            '/\.jpeg/','/\.jpg/','/\.png/','/\.gif/','/\.svg/','/\.xml$/',
            '/([а-я])/'
         ];
    }

    /**
     *
     */
    public function runRedirects()
    {

        foreach ($this->exceptions() as $pattern){
            if ($this->fullUrlMatch($pattern)){
                return;
            }
        }

        foreach ($this->redirectRules() as $rule) {
            $statusCode = isset($rule[2]) ? $rule[2] : 301;
            if ($this->fullUrlEq($rule[0])){
                $this->redirect($rule[1], $statusCode);
            }

            if ($this->fullUrlMatch($rule[0])) {
                $this->redirect($this->fullUrlReplace($rule[0], $rule[1]), $statusCode);
                return;
            }
        }
        return;
    }


}