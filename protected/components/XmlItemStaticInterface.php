<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 09.11.2018
 * Time: 12:27
 */

class XmlItemStaticInterface extends XmlItemAbstract implements XmlItemsListInterface, XmlItemInterface
{

    protected $route;

    public function __construct($route='')
    {
        $this->route = $route;
    }

    /**
     * @return string
     */
    public function getLoc()
    {
        return Yii::app()->createUrl($this->route);
    }


    /**
     * @return array
     */
    protected static function notAllowed()
    {
        $result = ['main/index','sitemap/xml'];
        if ($nouIndexRoutes = (new NoIndexRoutes())->routes()) {
            foreach ($nouIndexRoutes as $item) {
                foreach ($item[0] as $route) {
                    $result[] = $route;
                }
            }
        }
        return $result;
    }

    /**
     * @return array|XmlItemInterface[]
     */
    public static function findXmlItems()
    {

        $itemsInstances = [];
        if ($rules = Yii::app()->urlManager->rules) {
            foreach ($rules as $url => $route) {
                if (!in_array($route, self::notAllowed()) && !preg_match('/<.*:.*>/', $url)) {
                   $itemsInstances[] = new static($route);
                }
            }
        }
        return $itemsInstances;

    }
}