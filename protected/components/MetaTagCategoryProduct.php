<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 09.01.2019
 * Time: 15:56
 */

class MetaTagCategoryProduct extends MetaTagsModel
{

    public function getVariables()
    {
        return [
            'title' => $this->model->getTitle(),
        ];
    }

    public function getTemplate()
    {
        return [
            'title' => 'Купить {title} оптом по выгодным ценам | Textileinternational.com.ua',
            'description' => '{title} - Оптовый интернет-магазин тканей Textileinternational.com.ua: Звоните ☎ +38(067)-482-6000 ✓ Огромный ассортимент ✓ Отличные цены ✈ Быстрая доставка!',
            'keywords' => '',
            'h1' => '{title} оптом',
        ];
    }
}