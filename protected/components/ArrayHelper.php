<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 04.12.2018
 * Time: 10:52
 */

class ArrayHelper
{

    protected $array = [];

    public function __construct($array)
    {
        $this->array = $array;
    }

    public static function getInstance($array){
        return new static($array);
    }

    public function leadToStructure($structure){
        foreach ($structure as $key=>$item){
            if (isset($this->array[$key])){
                $structure[$key] = $this->array[$key];
            }
        }
        return $structure;
    }

}