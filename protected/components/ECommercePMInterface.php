<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 14.11.18
 * Time: 23:00
 */

interface ECommercePMInterface
{

    /**
     * @return integer
     */
    public function getId();

    /**
     * @return name
     */
    public function getName();

}