<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 03.12.2018
 * Time: 16:28
 */

interface ContactItemInterface
{

    /**
     * @return string
     */
    public function getPhone();

    /**
     * @return string
     */
    public function getAddress();

    /**
     * @return string
     */
    public function getCity();

    /**
     * @return string
     */
    public function getCoordinates();

}