<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 14.11.2018
 * Time: 09:29
 */

class TextFileRobots extends TextFileAbstract
{

    /**
     * @return string|void
     */
    function filePath()
    {
        return Yii::getPathOfAlias('root') . '/robots.txt';
    }
}