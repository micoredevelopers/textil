<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 12.11.2018
 * Time: 11:38
 */

class MetaTagStatic extends CComponent implements MetaTagInterface
{

    protected $route;

    protected $config;

    protected $variables;

    public function __construct($route=null){
        $this->route = $route === null ? Yii::app()->controller->getRoute() : $route;
        $this->config = $this->getConfig();
        $this->variables = $this->getVariables();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getTextWithReplace('title');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getTextWithReplace('description');
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->getTextWithReplace('keywords');
    }

    public function getH1(){
        return $this->getTextWithReplace('h1');
    }

    /**
     * @return mixed
     */
    public function getConfig(){
        return $this->getRule($this->metaTagsConfig(),$this->route);
    }

    /**
     * @return mixed
     */
    public function getVariables(){
        return $this->getRule($this->metaTagsVariables(),$this->route);
    }


    /**
     * @param $config
     * @param $key
     * @param string $defaultKey
     * @return mixed
     */
    public function getRule($config,$key,$defaultKey = 'default'){
         foreach ($config as $item){
            if (is_array($item[0]) && in_array($key,$item[0])){
                return $item[1];
            } elseif (is_string($item[0]) && $item[0] == $key){
                return $item[1];
            }
        }
        return $this->getRule($config,$defaultKey,$defaultKey);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getTextWithReplace($key){
        return $this->replaceVariables($this->variables,$this->config[$key]);
    }

    /**
     * @param $variables
     * @param $text
     * @return mixed
     */
    public function replaceVariables($variables,$text){
        if ($variables){
            foreach ($variables as $var=>$value){
                $text = str_replace('{'.$var.'}',$value,$text);
            }
        }
        return $text;
    }

    /**
     * @return array
     */
    public function metaTagsConfig(){
        return [
            [['main/index'],[
                'title' => 'Ткани оптом по выгодным ценам: оптовый интернет-магазин текстиля Textileinternational.com.ua с доставкой по всей Украине',
                'description' => 'Ткани оптом от производителя - оптовый интернет-магазин текстиля Textileinternational.com.ua: Звоните ☎ +38(067)-482-6000, +38(067)-484-5225 ✓ Огромный ассортимент ✓ Отличные цены ✈ Быстрая доставка!',
                'keywords' => '',
                'h1' => 'Ткани оптом'
            ]],
            ['default',[
                'title' => '{label} | Textileinternational.com.ua',
                'description' => '{label} | Оптовый интернет-магазин тканей Textileinternational.com.ua: Звоните ☎ +38(067)-482-6000 ✓ Огромный ассортимент ✓ Отличные цены ✈ Быстрая доставка!',
                'keywords' => '',
                'h1' => ' {label}'
            ]]
        ];
    }

    /**
     * @return array
     */
    public function metaTagsVariables(){
        return [
            ['default',['label' => PageLabels::getByRoute()]],
            ];
    }
}