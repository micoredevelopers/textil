<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 09.11.2018
 * Time: 10:06
 */

interface XmlItemInterface
{

    /**
     * @return string
     */
    public function getLoc();

    /**
     * @return string
     */
    public function getChangefreq();


    /**
     * @return string
     */
    public function getPriority();

    /**
     * @return false|string
     */
    public function  getLastmod();

}