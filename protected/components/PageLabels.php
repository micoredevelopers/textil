<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 13.11.2018
 * Time: 10:22.
 */
class PageLabels
{
    public static function labels()
    {
        return [
            'news/index' => 'Статьи',
            'catalog/index' => 'Каталог',
            'pages/about' => 'О нас',
            'pages/delivery' => 'Оплата и доставка',
            'pages/contact' => 'Контакты',
            'cart/index' => 'Корзина',
            'pages/under_order' => 'Ткани под заказ',
            'pages/wholesale' => 'Оптом',
            'catalog/search' => 'Поиск',
            'main/addOrder' => 'Оформление',
            'site/error' => 'Страница не найдена',
        ];
    }

    public static function getByRoute()
    {
        $route = Yii::app()->controller->getRoute();
        $labels = self::labels();

        return isset($labels[$route]) ? $labels[$route] : $route;
    }
}
