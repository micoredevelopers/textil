<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 28.12.2018
 * Time: 14:06
 */

class PaginationLink
{

    /**
     * @var CPagination
     */
    protected $pages;

    /**
     * @var int
     */
    protected $page;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * PaginationLink constructor.
     * @param CPagination $pages
     * @param int $page
     * @param string $baseUrl
     */
    public function __construct(CPagination $pages, $page=1, $baseUrl='')
    {
        $this->pages = $pages;
        $this->page = $page;
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }


    /**
     * @return string
     */
    public function getUrl(){
        if ($this->page === 1){
            return $this->baseUrl;
        }
        $separator = strstr($this->baseUrl,'?') ? '&' : '?';
        return $this->baseUrl.$separator.$this->pages->pageVar.'='.$this->page;
    }

    /**
     * @return bool
     */
    public function isActive(){
        return ($this->pages->getCurrentPage()+1) === $this->page;
    }

    /**
     * @return bool
     */
    public function isNext(){

       return ($this->page - $this->pages->getCurrentPage()-1) === 1;
    }

    /**
     * @return bool
     */
    public function isPrevious(){
        return ($this->pages->getCurrentPage()+1 - $this->page) === 1;
    }


}