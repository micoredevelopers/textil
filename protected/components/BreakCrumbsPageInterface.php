<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 09.11.2018
 * Time: 15:30
 */

interface BreakCrumbsPageInterface
{

    /**
     * @return array
     */
    public function getCrumbs();

}