<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 14.11.2018
 * Time: 10:16
 */

class TextFileScripts extends TextFileAbstract
{

    /**
     * @var string
     */
    protected $file='';

    public function __construct($file)
    {
        $this->file = $file;
    }


    /**
     * @return string
     */
    public function filePath()
    {
        return Yii::getPathOfAlias('root') . '/files_txt/'.$this->file;
    }

    /**
     * @param $file
     * @return TextFileScripts
     */
    public static function instanceByFile($file){
        return new static($file);
    }
}