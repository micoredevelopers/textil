<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 14.11.18
 * Time: 23:02
 */

class ECommerceProduct implements ECommerceItemInterface
{

    /**
     * @var ECommercePMInterface
     */
    protected $model;

    /**
     * @var string
     */
    protected $type;

    public function __construct(ECommercePMInterface $model, $type='addImpression')
    {
        $this->model = $model;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return [
            'id' => $this->model->getId(),
            'name' => $this->model -> getName()
        ];
    }
}