<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 05.12.2018
 * Time: 14:36
 */

interface BreakCrumbsCategoryInterface
{

    public function getTitle();

    public function getUrl();

    public function getParent();

}