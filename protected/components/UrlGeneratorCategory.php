<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 13.11.2018
 * Time: 12:17
 */

/**
 * Class UrlGeneratorCategory
 * @property $title_ru
 * @property $alias
 */
class UrlGeneratorCategory extends CActiveRecord implements UrlGeneratorModelInterface
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Categoryproduct the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'categoryproduct';
    }

    public function getTitle()
    {
        return $this->title_ru;
    }

    public function getUrl()
    {
        return $this->alias;
    }

    public function setUrl($url)
    {
        $this->alias = $url;
    }
}