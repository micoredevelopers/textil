<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 04.12.2018
 * Time: 13:51
 */

class NavigationMenuItem implements NavigationMenuItemInterface
{

    /**
     * @var string
     */
    protected $label;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var bool
     */
    protected $inFooter;

    /**
     * @var bool
     */
    protected $inHeader;

    public function __construct($config=[])
    {
        if ($config){
            foreach ($config as $attribute=>$value){
                $this->$attribute = $value;
            }
        }
    }

    /**
     * @param array $config
     * @return NavigationMenuItem
     */
   public static function instanceByArray($config = []){
        return new static($config);
   }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Yii::app()->createUrl($this->url);
    }

    /**
     * @return bool
     */
    public function inFooter()
    {
       return $this->inFooter;
    }

    /**
     * @return bool
     */
    public function inHeader()
    {
        return $this->inHeader;
    }
}