<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 03.12.2018
 * Time: 16:30.
 */
class ContactItem implements ContactItemInterface
{
    public function __construct($config = [])
    {
        if ($config) {
            foreach ($config as $attribute => $value) {
                $this->$attribute = $value;
            }
        }
    }

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $address;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $coordinates;

    /**
     * @var string
     */
    public $fullAddress;

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    public function getPhoneHref(){
        return str_replace([' ','(',')'],'',$this->phone);
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    public function getFullAddress(){
        return $this->fullAddress ? $this->fullAddress : $this->phone.', '.$this->address;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    public function getCoordinates()
    {
        return $this->coordinates;
    }

    public static function findAll()
    {
        $arr = [
            [
                'phone' => '+38 (067) 483 99 92',
                'address' => 'Промрынок 7 км, Центральная стоянка, магазин №384',
                'fullAddress' => '<p>Одесса, Промрынок 7 км: 
                <p>магазин №384, Тел. — +38 (067) 483 99 92;</p>
                <p>магазин №4320, Тел. — +38 (067) 480 48 80;</p>
                <p>магазин №4320 (распродажа), Тел. — +38 (067) 483 99 92;</p>  
                <p>магазин №2288, Тел. — +38 (067) 480 81 70;</p>',
                'city' => 'Одесса',
                'coordinates' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2749.253396210761!2d30.632567315841563!3d46.44367597912477!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c62d7d12b5c3cd%3A0xab26377eade2d278!2z0J7QntCeICLQn9GA0L7QvNGC0L7QstCw0YDQvdGL0Lkg0YDRi9C90L7QuiIgNy3QuSDQutC40LvQvtC80LXRgtGAICI3INC60Lwi!5e0!3m2!1sen!2sua!4v1544006092057',
            ],
            [
                'phone' => '+38 (067) 487 72 78',
                'address' => 'ТЦ Барабашово, Афганская площадка, магазин №21-П1-10;',
                'fullAddress' => 'ТЦ Барабашово, Афганская площадка, магазин №21-П1-10, Тел. - +38 (067) 487 72 78;
                <p>ТЦ Барабашово, Афганская площадка, магазин №21-А1-40, Тел. - +38 (067) 485 45 76;</p>
                <p>ТЦ Барабашово, Афганская площадка, магазин №14 (распродажа), Тел. - +38 (073) 481 72 11</p>',
                'city' => 'Харьков',
                'coordinates' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1766.899066530231!2d36.30365354507921!3d50.0034109652129!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x820db9dc00e54183!2sBarabashovo+Market!5e0!3m2!1sen!2sua!4v1544006045089',
            ],
            [
                'phone' => '+38 (067) 557 08 87',
                'address' => 'Вещевой рынок, ул. Геологов 7, магазин №7-8',
                'city' => 'Хмельницкий',
                'coordinates' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2595.4369336111695!2d26.951880170598294!3d49.41955298709855!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDnCsDI1JzEwLjQiTiAyNsKwNTcnMTMuMCJF!5e0!3m2!1sen!2sua!4v1544005929149',
            ],
        ];
        $result = [];
        foreach ($arr as $item) {
            $result[] = new static($item);
        }

        return $result;
    }
}
