<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 13.11.2018
 * Time: 10:49
 */

class MetaTagPage extends MetaTagsModel
{

    public function getVariables(){
        return [
            'title' => $this->model->getTitle()
        ];
    }

    public function getTemplate(){
        return [
            'title' => '{title} | Textileinternational.com.ua',
            'description' => '{title} | Оптовый интернет-магазин тканей Textileinternational.com.ua: Звоните ☎ +38(067)-482-6000 ✓ Огромный ассортимент ✓ Отличные цены ✈ Быстрая доставка!',
            'keywords' => '',
            'h1' => '{title}'
        ];
    }

}