<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{

public function createMultilanguageReturnUrl($lang='ru'){
    if (count($_GET)>0){
        $arr = $_GET;
        $arr['lang']= $lang;
    }
    else 
        $arr = array('lang'=>$lang);
    return $this->createUrl('', $arr);
}

    public $activemenu = '';
    public $active = array();
    public $metatitle;
    public $metadescription;
    public $metakeywords;
    public $header = '';
    public $params = [];
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='layout';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	// мультиязычность
	public function __construct($id,$module=null){

    parent::__construct($id,$module);
		Yii::app()->language = isset($_GET['lang']) ? $_GET['lang'] : 'ru';

	}

    protected function _showPageInfo($pageID,$addData=[],$viewFile=null){
	    if ($viewFile===null){
            $viewFile = Yii::app()->controller->action->id;
        }
        $pageInfo = Pages::model()->findByPk($pageID);
        Yii::app()->metaTags->set($this,new MetaTagPage($pageInfo));
        $this->render($viewFile,array_merge(['pageInfo' => $pageInfo],$addData));
    }


}
