<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 04.12.2018
 * Time: 13:43
 */

class NavigationMenu
{

    /**
     * @return NavigationMenuItemInterface[]
     */
    public static $all=[];

    /**
     * @return NavigationMenuItemInterface[]
     */
    public function all(){
       if (!static::$all){
           $array = [
               [
                   'label'=>'О нас',
                   'url' =>'pages/about',
                   'inFooter' => true,
                   'inHeader' => true
               ],
               [
                   'label'=>'Каталог',
                   'url' =>'catalog/index',
                   'inFooter' => true,
                   'inHeader' => true
               ],
//               [
//                   'label'=>'Статьи',
//                   'url' =>'news/index',
//                   'inFooter' => true,
//                   'inHeader' => true
//               ],
               [
                   'label'=>'Оплата и доставка',
                   'url' =>'pages/delivery',
                   'inFooter' => true,
                   'inHeader' => true
               ],
               [
                   'label'=>'Контакты',
                   'url' =>'pages/contact',
                   'inFooter' => true,
                   'inHeader' => true
               ],


           ];
           foreach ($array as $item){
               static::$all[] = NavigationMenuItem::instanceByArray($item);
           }
       }
       return static::$all;
    }

    /**
     * @return NavigationMenuItemInterface[]
     */
    public function header(){
       $result = [];
       foreach ($this->all() as $item){
           if ($item->inHeader()){
               $result[] = $item;
           }
       }
       return $result;
    }

    /**
     * @return NavigationMenuItemInterface[]
     */
    public function footer(){
        $result = [];
        foreach ($this->all() as $item){
            if ($item->inFooter()){
                $result[] = $item;
            }
        }
        return $result;
    }

}