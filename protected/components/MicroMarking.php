<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 08.11.2018
 * Time: 13:32
 */

class MicroMarking extends CComponent
{

    /**
     * @var array
     */
    public $markings = [];

    /**
     * @return array
     */
    public function getMarkings()
    {
        return array_merge($this->markings, $this->defaultMarkings());
    }

    /**
     * @param array $markings
     */
    public function setMarkings($markings)
    {
        $this->markings = $markings;
    }

    /**
     * @return array
     */
    public function defaultMarkings()
    {
        return [
            [
                "@type" => "Organization",
                "url" => Yii::app()->request->getBaseUrl(true) . Yii::app()->request->getUrl(true),
                "contactPoint" => [
                    [
                        "@type" => "ContactPoint",
                        "telephone" => "+38 (067) 484 52 25",
                        "contactType" => "customer service"
                    ],
                    [
                        "@type" => "ContactPoint",
                        "telephone" => "+38 (067) 487 72 78",
                        "contactType" => "customer service"
                    ],
                    [
                        "@type" => "ContactPoint",
                        "telephone" => "+38 (067) 518 79 00",
                        "contactType" => "customer service"
                    ],
                    [
                        "@type" => "ContactPoint",
                        'telephone' => "+38 (067) 482 60 00",
                        "contactType" => "customer service"
                    ]
                ]
            ],
            [
                "@type" => "Organization",
                "image" => [
                    Yii::app()->request->getBaseUrl(true) . '/images/Foto/14472549192387.jpg',
                ],
                "@id" => Yii::app()->request->getBaseUrl(true),
                "name" => "Текстиль Интернешнл",
                "address" => [
                    [
                        "@type" => "PostalAddress",
                        "streetAddress" => "Промрынок 7 км",
                        "addressLocality" => "Одесса",
                        "addressRegion" => "Одесса",
                        "postalCode" => "65120",
                        "addressCountry" => "UK"
                    ],
                ],
                "telephone" => "+38 (067) 482 60 00",
            ],
            [
                "@type" => "WebSite",
                "url" => Yii::app()->request->getBaseUrl(true),
                "potentialAction" => [
                    "@type" => "SearchAction",
                    "target" => Yii::app()->request->getBaseUrl(true)."/search/?q={search_term_string}",
                    "query-input" => "required name=search_term_string"
                ]
            ],
        ];
    }

    public function defaultMarkingData()
    {
        return [
            "@context" => "http://schema.org",
        ];
    }

    public function init()
    {
    }

    /**
     * @param $data
     * @param MicroMarkingAbstract|null $objectAdd
     */
    public function addMarking($data, MicroMarkingAbstract $objectAdd = null)
    {
        if ($objectAdd) {
            $objectAdd->setData($data);
            $data = $objectAdd->getMarking();
        }
        array_push($this->markings, $data);
    }

    /**
     * @return string
     */
    public function string()
    {
        $result = '';
        foreach ($this->getMarkings() as $marking) {
            $result .= '<script type="application/ld+json">' . PHP_EOL;
            $result .= json_encode($marking + $this->defaultMarkingData(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . PHP_EOL;
            $result .= '</script>';
        }
        return $result;
    }

}