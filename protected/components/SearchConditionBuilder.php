<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 13.12.2018
 * Time: 10:07
 */

class SearchConditionBuilder
{


    protected $searchString;

    protected $fields;

    protected $likeTemplate = '%{searchString}%';

    protected $separator = 'OR';

    protected $alias = '';

    /**
     * SearchConditionBuilder constructor.
     * @param string $searchString
     * @param array $fields
     */
    public function __construct($searchString='',$fields=[])
    {
        $this->searchString = $searchString;
        $this->fields = $fields;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    /**
     * @return string
     */
    public function getLikeTemplate()
    {
        return $this->likeTemplate;
    }

    /**
     * @param $likeTemplate
     * @return $this
     */
    public function setLikeTemplate($likeTemplate)
    {
        $this->likeTemplate = $likeTemplate;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeparator()
    {
        return $this->separator;
    }

    /**
     * @param $separator
     * @return $this
     */
    public function setSeparator($separator)
    {
        $this->separator = $separator;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param $alias
     * @return $this
     * @throws CException
     */
    public function setAlias($alias)
    {
        if (substr($alias,-1,1) !== '.'){
            throw new CException('the alias must contain "."');
        }
        $this->alias = $alias;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getSearchString(){
       return  str_replace('{searchString}',$this->searchString,$this->likeTemplate);
    }

    /**
     * @return string
     */
    public function getCondition(){
        $conditionArray = [];
        foreach ($this->fields as $item){
            $field = strstr($item,'.') ? $item : $this->alias.$item;
            $conditionArray[] = $field.' LIKE "'.$this->getSearchString().'"';
        }
        return implode(' '.$this->separator.' ',$conditionArray);
    }

    /**
     * @param string $searchString
     * @param array $fields
     * @return SearchConditionBuilder
     */
    public static function instance($searchString='',$fields=[]){
        return new static($searchString,$fields);
    }

}