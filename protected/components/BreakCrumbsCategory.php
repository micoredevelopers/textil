<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 05.12.2018
 * Time: 14:36
 */

class BreakCrumbsCategory implements BreakCrumbsPageInterface
{

    protected $crumbs = [];

    public function __construct(BreakCrumbsCategoryInterface $category)
    {
        if ($category->getParent() !== 0){
            $categoryParent = Categoryproduct::model()->findByPk($category->getParent());
            if ($categoryParent){
                $this->crumbs[]=[
                    'label' => $categoryParent->getTitle(),
                    'url' => $categoryParent->getUrl()
                ];
            }
        }

        $this->crumbs[]=[
            'label' => $category->getTitle(),
            'url' => ''
        ];
      //  dump($this->crumbs,1);
      //  dump($this->crumbs,1);
    }

    /**
     * @return array
     */
    public function getCrumbs()
    {
        return $this->crumbs;
    }
}