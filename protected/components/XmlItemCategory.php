<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 09.11.2018
 * Time: 10:56
 */

/**
 * Class XmlItemCategory
 * @property $alias
 */
class XmlItemCategory extends CActiveRecord implements XmlItemsListInterface,XmlItemInterface
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'categoryproduct';
    }

    /**
     * @return string
     */
    public function getLoc()
    {
        $u = $this->alias ?  $this->alias : $this->id;
        return Yii::app()->createUrl('catalog/category',['alias'=>$u]);
    }

    /**
     * @return string
     */
    public function getChangefreq()
    {
        return 'weekly';
    }

    /**
     * @return string
     */
    public function getPriority()
    {
        return 0.5;
    }

    /**
     * @return false|string
     */
    public function getLastmod()
    {
        // TODO: Implement getLastmod() method.
    }

    /**
     * @return mixed
     */
    public static function findXmlItems()
    {
        return ARFinder::find(static::model())->condition('parent = 0')->all();
    }

}