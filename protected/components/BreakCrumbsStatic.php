<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 09.11.2018
 * Time: 15:36
 */

class BreakCrumbsStatic implements BreakCrumbsPageInterface
{

    /**
     * @return mixed
     */
    public function label(){
       return PageLabels::getByRoute();
    }

    /**
     * @return array
     */
    public function getCrumbs()
    {
       // dump($this->label(),1);
        return [
            [
                'label' => $this->label(),
                //'url' => Yii::app()->createUrl(Yii::app()->controller->getRoute())
                'url' => ''
            ]
        ];
    }
}