<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 08.11.2018
 * Time: 13:48
 */

abstract class MicroMarkingAbstract
{
    protected $markingData;

    public function setData($data){
        $this->markingData = $data;
    }

    public function getData(){
        return $this->markingData;
    }

    abstract public function getMarking();
}