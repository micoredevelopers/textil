<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 05.12.2018
 * Time: 15:53
 */

class MetaTagProduct extends MetaTagsModel
{

    public function getVariables()
    {
        return [
            'title' => $this->model->getTitle(),
        ];
    }

    public function getTemplate()
    {
        return [
            'title' => '{title} оптом по выгодным ценам | Textileinternational.com.ua',
            'description' => 'Ищете {title} оптом? Оптовый интернет-магазин тканей Textileinternational.com.ua: Звоните ☎ +38(067)-482-6000 ✓ Огромный ассортимент ✓ Отличные цены ✈ Быстрая доставка!',
            'keywords' => '',
            'h1' => '{title}',
        ];
    }

}