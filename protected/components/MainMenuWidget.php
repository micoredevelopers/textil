<?php
/* рассмотрим здесь "параметризированный" виджет*/
class MainMenuWidget extends CWidget {
    public $activemenu;
    /*в общем случаем может
    хранить целый массив свойст - нас же интересует
    только текущее действие
    - чтобы подсветить активный элемент меню*/
    public $params = array(
        // пусть по умолчанию будет активна ссылка на главную
        // 'action'=>'index',
        // // следующие два не использую - просто для примера
        // 'parameter 2'=>'value 2',
        // 'id'=>'3'
    );

    public function createUrl($input, $input2 = null) {
        return Yii::app()->createUrl($input , $input2);
    }
    public function run() {
        // var_dump($activemenu);
        // var_dump($this->params);
        // передаем данные в представление виджета
        $criteria = new CDbCriteria;
        $criteria->condition = "status = 1";
        $criteria->order = "position ASC";
        $cat_solution = Categorysolution::model()->findAll($criteria);


        $criteria = new CDbCriteria;
        $criteria->condition = "parent = 0 AND status = 1";
        $criteria->order = "position ASC";
        $cat_articols = Category::model()->findAll($criteria);
        $cat_product = Categoryproduct::model()->findAll($criteria);
        $this->render('mainmenuwidget', array(
            'cat_solution'=>$cat_solution,
            'cat_articols'=>$cat_articols,
            'cat_product'=>$cat_product,
            ));
    }
    // public function reduceContent($string, $length, $id = '')
    // {
       
    //     //$link = ($id == '') ? '.' : CHtml::link('[...]', array('articles/item', 'id' => $id));
    //     if ($length && strlen($string) > $length)
    //     {
    //         $str =  strip_tags($string);
    //         //$str = CHtml::encode($str);
    //         $str = substr($str, 0, $length);
    //         $pos = strrpos($str, ' ');

    //         return substr($str, 0, $pos);
    //     }
        
    //     return $string; 
    // }

    
}

?>