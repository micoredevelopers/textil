<?php
/* рассмотрим здесь "параметризированный" виджет*/
class ApplicationWidget extends CWidget {

   public $params = array(
       // пусть по умолчанию будет активна ссылка на главную
   );

   public function run() {
       // передаем данные в представление виджета

       $this->render('applicationwidget');
   }
}