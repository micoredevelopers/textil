<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 14.11.2018
 * Time: 09:26
 */

abstract class TextFileAbstract
{

    /**
     * @return bool|string
     */
    public function read(){
        $text = is_file($this->filePath()) ? file_get_contents($this->filePath()) : '';
        return $this->handlingReadFile($text);
    }

    /**
     * @param $text
     * @return mixed
     */
    public function handlingReadFile($text){
        return $text;
    }

    /**
     * @param string $text
     * @return bool|int
     */
    public function write($text){
        $text = $this-> handlingWriteFile($text);
        return file_put_contents($this->filePath(),$text);
    }

    public function handlingWriteFile($text){
        return $text;
    }

    /**
     * @return string
     */
    abstract function filePath();

}