<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 30.11.2018
 * Time: 09:54
 */

class ValueDependsOnCustomer
{

    public static function get($desktop,$mobil){
        if (key_exists('User-Agent',getallheaders()) && strstr(getallheaders()['User-Agent'],'Mobile')){
            return $mobil;
        }
        return $desktop;
    }

}