<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 13.11.2018
 * Time: 12:11
 */

interface UrlGeneratorModelInterface
{

    public function getTitle();

    public function getUrl();

    public function setUrl($url);

    public function save();

}