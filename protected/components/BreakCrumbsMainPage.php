<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 05.12.2018
 * Time: 10:03
 */

class BreakCrumbsMainPage implements BreakCrumbsPageInterface
{

    /**
     * @return array
     */
    public function getCrumbs()
    {
        return [
            [
                'label' => 'Ткани оптом',
                'url' => Yii::app()->createUrl('main/index')
            ]
        ];
    }
}