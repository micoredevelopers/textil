<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 14.11.18
 * Time: 23:46.
 */
class ECommerceProductCart implements ECommerceItemInterface
{
    protected $type;

    protected $data;

    public function __construct(array $data, $type = 'addProduct')
    {
        $this->data = [
            'id' => $data['id'],
            'name' => $data['title'],
            'price' => $data['price'],
            'quantity' => $data['count'],
            'category' => static::findCategoryNameByProductID($data['id']),
        ];
        $this->type = $type;
    }

    public static function findCategoryNameByProductID($id)
    {
        $product = Production::model()->findByPk($id);
        if ($product) {
            $category = Categoryproduct::model()->findByPk($product->category);

            return $category->getTitle();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}
