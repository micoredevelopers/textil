<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 14.11.18
 * Time: 22:42.
 */
class ECommerce
{
    /**
     * @var ECommerceItemInterface[] array
     */
    protected $items = [];

    protected $actions = [];

    public function init()
    {
    }

    /**
     * @param ECommerceItemInterface $EComerceItem
     */
    public function add(ECommerceItemInterface $EComerceItem)
    {
        $this->items[] = $EComerceItem;
    }

    public function addAction($action, array $data = [])
    {
        $this->actions[] = [
            $action, $data,
        ];
    }

    /**
     * @return string
     */
    public function getScripts()
    {
        $script = '<script>'.PHP_EOL;
        $script .= "ga('require', 'ec');".PHP_EOL;
        if ($this->items) {
            foreach ($this->items as $instance) {
                $script .= 'ga("ec:'.$instance->getType().'", '.json_encode($instance->getData(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE).');'.PHP_EOL;
            }
        }
        if ($this->actions) {
            foreach ($this->actions as $action) {
                $script .= 'ga("ec:setAction","'.$action[0].'"';
                if ($action[1]) {
                    $script .= ', '.json_encode($action[1], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                }
                $script .= ');'.PHP_EOL;
            }
        }
        $script .= "ga('send', 'pageview');";
        $script .= '</script>';

        return  $script;
    }
}
