<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 09.11.2018
 * Time: 09:41
 */

class XmlCreator extends CComponent
{
    protected $xmlString;

    protected $domain;

    protected $version = '1.0';

    protected $encoding = 'UTF-8';

    protected $protocol = 'http://www.sitemaps.org/schemas/sitemap/0.9';

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getEncoding()
    {
        return $this->encoding;
    }

    /**
     * @param string $encoding
     */
    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;
    }

    /**
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * @param string $protocol
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }



    /**
     * @return mixed
     */
    public function getXmlString()
    {
        return '<?xml version="'.$this->getVersion().'" encoding="'.$this->getEncoding().'" ?><urlset xmlns="'.$this->getProtocol().'">'.$this->xmlString.'</urlset>';
    }

    /**
     * @param mixed $xmlString
     */
    public function setXmlString($xmlString)
    {
        $this->xmlString = $xmlString;
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        if (!$this->domain){
            $this->domain = Yii::app()->request->getBaseUrl(true);
        }
        return $this->domain;
    }

    /**
     * @param mixed $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }


    /**
     * @param $array
     */
    public function addXmlString($array)
    {
        $this->xmlString .= '<url>
              <loc>'.$this->getDomain().$array['loc'].'</loc>
              <lastmod>'.$array['lastmod'].'</lastmod>
              <changefreq>'.$array['changefreq'].'</changefreq>
              <priority>'.$array['priority'].'</priority>
              </url>';
    }

    /**
     * @param $array
     * @throws Exception
     */
    public function addXmlArray($array){
        if (!isset($array['loc'])){
            throw new Exception('not valid');
        }

        $this->addXmlString ([
            'loc' => $array['loc'],
            'lastmod' => isset($array['lastmod']) ? $array['lastmod'] : date('Y-m-d'),
            'changefreq' => isset($array['changefreq']) ? $array['changefreq'] : 'weekly',
            'priority' => isset($array['priority']) ? $array['priority'] : 0.5,
        ]);
    }

    /**
     * @param XmlItemInterface $xmlItem
     */
    public function addXmlObject(XmlItemInterface $xmlItem){
         $this->addXmlString ([
            'loc' => $xmlItem->getLoc(),
            'lastmod' => $xmlItem->getLastmod() ? $xmlItem->getLastmod() : date('Y-m-d'),
            'changefreq' => $xmlItem->getChangefreq(),
            'priority' => $xmlItem->getPriority()
        ]);
    }

    /**
     * @param XmlItemsListInterface $xmlItemsList
     * @throws Exception
     */
    public function addXmlItemsList(XmlItemsListInterface $xmlItemsList){
        $items = $xmlItemsList::findXmlItems();
        if ($items){
            foreach ($items as $item){
                if ($item instanceof XmlItemInterface){
                   $this->addXmlObject($item);
                } else {
                    $this->addXmlArray($item);
                }
            }
        }
    }


}