<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 09.11.2018
 * Time: 15:22
 */

class BreakCrumbs extends CComponent
{

    protected $crumbs=[];

    public function init(){}

    /**
     * @return array
     */
    public function getCrumbs()
    {
        return $this->crumbs;
    }

    /**
     * @param $crumbs
     */
    public function setCrumbs($crumbs)
    {
        $this->crumbs = $crumbs;
    }

    /**
     * @param BreakCrumbsPageInterface $crumbsPage
     * @return $this
     */
    public function setCrumbsByPage (BreakCrumbsPageInterface $crumbsPage){
        $this->setCrumbs($crumbsPage->getCrumbs());
        return $this;
    }

    /**
     * @param BreakCrumbsPageInterface $crumbsPage
     * @return $this
     */
    public function addCrumbsByPage(BreakCrumbsPageInterface $crumbsPage){
        $this->setCrumbs(array_merge($this->getCrumbs(), $crumbsPage->getCrumbs()));
        return $this;
    }

    /**
     * @param array $array
     * @return $this
     */
    public function addCrumbsByArray($array){
        $this->setCrumbs(array_merge($this->getCrumbs(), $array));
        return $this;
    }

    /**
     * @return array
     */
    public function linksToWidget(){
        return $this->getCrumbs();
        $result = [];
        foreach ($this->getCrumbs() as $crumb){
            $result[$crumb['label']] = $crumb['url'];
        }
        return $result;
    }



}