<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 09.11.2018
 * Time: 11:16
 */

interface XmlItemsListInterface
{

    /**
     * @return array|XmlItemInterface[]
     */
    public static function findXmlItems();

}