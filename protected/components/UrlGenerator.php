<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 13.11.2018
 * Time: 12:08
 */

class UrlGenerator
{

    protected $model;

    protected $isAlways = true;

    public function __construct(UrlGeneratorModelInterface $model)
    {
        $this->model = $model;
    }

    /**
     * @return bool
     */
    public function isAlways()
    {
        return $this->isAlways;
    }

    /**
     * @param bool $isAlways
     */
    public function setIsAlways($isAlways)
    {
        $this->isAlways = $isAlways;
    }



    public function setUrl(){
        $url = $this->model->getUrl();
        if (!$url || !preg_match('/\/$/',$url) || $this->isAlways()){
            $this->model->setUrl($this->title2Url());
        }
        return $this;
    }

    public function save(){
        $this->model->save();
    }

    function title2Url()
    {
        $tr = array(
            "А"=>"a", "Б"=>"b", "В"=>"v", "Г"=>"g", "Д"=>"d",
            "Е"=>"e", "Ё"=>"yo", "Ж"=>"zh", "З"=>"z", "И"=>"i",
            "Й"=>"j", "К"=>"k", "Л"=>"l", "М"=>"m", "Н"=>"n",
            "О"=>"o", "П"=>"p", "Р"=>"r", "С"=>"s", "Т"=>"t",
            "У"=>"u", "Ф"=>"f", "Х"=>"kh", "Ц"=>"ts", "Ч"=>"ch",
            "Ш"=>"sh", "Щ"=>"sch", "Ъ"=>"", "Ы"=>"y", "Ь"=>"",
            "Э"=>"e", "Ю"=>"yu", "Я"=>"ya", "а"=>"a", "б"=>"b",
            "в"=>"v", "г"=>"g", "д"=>"d", "е"=>"e", "ё"=>"yo",
            "ж"=>"zh", "з"=>"z", "и"=>"i", "й"=>"j", "к"=>"k",
            "л"=>"l", "м"=>"m", "н"=>"n", "о"=>"o", "п"=>"p",
            "р"=>"r", "с"=>"s", "т"=>"t", "у"=>"u", "ф"=>"f",
            "х"=>"kh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh", "щ"=>"sch",
            "ъ"=>"", "ы"=>"y", "ь"=>"", "э"=>"e", "ю"=>"yu",
            "я"=>"ya", " "=>"-", "."=>"", ","=>"", "/"=>"-",
            ":"=>"", ";"=>"","—"=>"", "–"=>"-", '(' => '', ')' => ''
        );
        $str = strtr($this->model->getTitle(),$tr);
        $str = preg_replace('/[^\w\d-]/','',$str);
        return  mb_strtolower($str);
    }

}