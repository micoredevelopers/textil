<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 14.11.2018
 * Time: 11:21
 */

class TextFileRedirects extends TextFileAbstract
{

    public function handlingWriteFile($array)
    {
        dump($array,1);
        $text = '[]';
        if (is_array($array) && $array) {
            foreach ($array as $k=>$item){
                $array[$k] = [$item['from'],$item['to'],$item['code']];
            }
            $text = json_encode(array_values($array));
        }
        return $text;
    }

    public function handlingReadFile($text)
    {
        $arr = json_decode($text, true);
        return $arr ? $arr : [];
    }

    public static function validationRedirects($array)
    {
        if (!is_array($array))
            return false;
        foreach ($array as $item) {
            $condition = isset($item[0]) && is_string($item[0]) && $item[0] && isset($item[1]) && is_string($item[1]) && isset($item[2]) && is_numeric($item[2]);
            if (!$condition)
                return false;
        }
        return true;
    }

    /**
     * @return string
     */
    function filePath()
    {
        return Yii::getPathOfAlias('root') . '/files_txt/redirects.json';
    }
}