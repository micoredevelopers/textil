<?php
/* рассмотрим здесь "параметризированный" виджет*/
class TwomainblocksWidget extends CWidget {
    public $activemenu;
    /*в общем случаем может
    хранить целый массив свойст - нас же интересует
    только текущее действие
    - чтобы подсветить активный элемент меню*/
    public $params = array(
        // пусть по умолчанию будет активна ссылка на главную
        // 'action'=>'index',
        // // следующие два не использую - просто для примера
        // 'parameter 2'=>'value 2',
        // 'id'=>'3'
    );

    public function createUrl($input, $input2 = null) {
        return Yii::app()->createUrl($input, $input2);
    }
    public function run() {
        // передаем данные в представление виджета
        $criteria = new CDbCriteria;
        $criteria->order = "date DESC";
        $criteria->condition = "status = 'active'";
        $action = Action::model()->find($criteria);
        $criteria = new CDbCriteria;
        $criteria->condition = "main_page = 1";
        $criteria->limit = 6;
        $model = Solution::model()->findAll($criteria);

        $foto = Foto::model()->findByPk(10);
        $foto1 = Foto::model()->findByPk(9);
        $this->render('twomainblockswidget', array(
            'action'=>$action,
            'model'=>$model,
            'foto'=>$foto,
            'foto1'=>$foto1,
            ));
    }
    // public function reduceContent($string, $length, $id = '')
    // {
       
    //     //$link = ($id == '') ? '.' : CHtml::link('[...]', array('articles/item', 'id' => $id));
    //     if ($length && strlen($string) > $length)
    //     {
    //         $str =  strip_tags($string);
    //         //$str = CHtml::encode($str);
    //         $str = substr($str, 0, $length);
    //         $pos = strrpos($str, ' ');

    //         return substr($str, 0, $pos);
    //     }
        
    //     return $string; 
    // }

    
}

?>