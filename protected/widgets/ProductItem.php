<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 30.11.2018
 * Time: 12:03
 */

class ProductItem extends CWidget
{

    public $categoryProduct;

    public $viewFile = 'product_item';

    public $carouselID = 'product-carousel';

    public static $sliderID = ['product-carousel'=>0];

    public function init()
    {
        if (!isset(static::$sliderID[$this->carouselID])){
            static::$sliderID[$this->carouselID] = 0;
        }
        static::$sliderID[$this->carouselID]++;
    }

    public function run(){
        $firstProduct = $this->categoryProduct->produ ? $this->categoryProduct->produ[0] : null;
        $this->render($this->viewFile, array('categoryProduct'=>$this->categoryProduct,'carouselID' => $this->carouselID,'sliderID'=>static::$sliderID[$this->carouselID],'firstProduct'=>$firstProduct));
    }

}