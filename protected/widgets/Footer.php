<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 04.12.2018
 * Time: 13:34
 */

class Footer extends CWidget
{

    public function init()
    {

    }

    public function run(){
        $contacts = ContactItem::findAll();
        $navigationMenu = (new NavigationMenu())->footer();
        $this->render('footer',['contacts' => $contacts,'navigationMenu' => $navigationMenu]);
    }

}