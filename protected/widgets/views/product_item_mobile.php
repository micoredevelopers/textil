<?php if ($firstProduct): ?>
<?php
Yii::app()->microMarking->addMarking($categoryProduct->microMarking());
?>
<div class="<?=$sliderID == 1 ? 'active ' : '' ?>item carousel-item" data-slide-number="<?=$sliderID?>">
    <a href="<?= $categoryProduct->getCASUrl()?>">
        <img src="<?= $firstProduct->getImagePath() ?>" class="img-fluid" alt="<?= $firstProduct->getTitle() ?>">
    </a>
    <div class="goods-content">
        <div class="inner">
            <div class="mob-optionsBlock">
                <div class="title" data-toggle="tooltip" data-placement="top" title="<?= $categoryProduct->getTitle() ?>"><?= $categoryProduct->getTitle() ?></div>
                <div class="options">
                    <?= $firstProduct->articul ?>
                </div>
            </div>

            <div class="price">
                <span> <?= $firstProduct->price_new ?> $</span>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>