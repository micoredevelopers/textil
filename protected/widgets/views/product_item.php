<?php if ($firstProduct) : ?>
    <?php
    Yii::app()->microMarking->addMarking($categoryProduct->microMarking());
    ?>
    <div class="col-md-4 col-12 goodsItem" id="myCarousel_<?= $sliderID ?>">
        <div class="carousel slide" data-interval="false">
            <div class="slider-for">

                <?php foreach ($categoryProduct->produ as $k => $product) : ?>
                    <?php
                     Yii::app()->ECommerce->add(new ECommerceProduct($product));
                    ?>
                    <div class="slick-slide <?= $k == 0 ? 'slick-active ' : '' ?>item carousel-item"
                         data-slide-number="<?= $k ?>" data-title="<?= $product->getTitle() ?>"
                         data-options="<?= $product->articul ?>" data-id="<?= $product->id ?>"
                         data-price="<?= $product->price_new ?> $">
                        <a href="<?= $categoryProduct->getCASUrl() ?>">
                            <img src="<?= $product->getImagePath() ?>" class="img-fluid"
                                 alt="<?= $product->getTitle() ?>">
                        </a>
                    </div>
                <?php endforeach; ?>


            </div>

            <div class="goods-content">
                <div class="inner">
                    <div class="title" data-toggle="tooltip" data-placement="top" title=""
                         data-original-title="<?= $categoryProduct->getTitle() ?>">
                        <?= $categoryProduct->getTitle() ?> <span><?= $firstProduct->getTitle() ?></span>
                    </div>
                    <div class="options">
                        <?= $firstProduct->articul ?>
                    </div>

                    <div class="price">
                        <span><?= $firstProduct->price_new ?> $</span>
                        <button class="add-to-cart" data-slider_selector="#myCarousel_<?= $sliderID ?>">Купить</button>
                    </div>
                </div>
                <div class="separator"></div>
                <div class="slider-nav thumbs" data-slider_selector="#myCarousel_<?= $sliderID ?>">
                    <?php foreach ($categoryProduct->produ as $k => $product) : ?>
                        <div class="slick-slide">
                            <div class="list-inline-item" data-price="<?= $product->price_new ?>$">
                                <img src="<?= $product->getImagePath() ?>" class="img-fluid"
                                     alt="<?= $product->getTitle() ?>">
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
<?php endif; ?>