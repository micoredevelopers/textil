<nav class="navbar navbar-dark navbar-expand-xl bg-white justify-content-between">
    <button class="navbar-toggler mobileHeaderNav" type="button">
        <span class="navbar-toggler-icon navbartogglerButton"></span>
    </button>
    <?php if ($navigationMenu) : ?>
        <div class="navbar-collapse collapse dual-nav w-100">
            <ul class="navbar-nav">
                <?php foreach ($navigationMenu as $menu) : ?>
                    <li class="nav-item active">
                        <a class="nav-link cool-link" href="<?= $menu->getUrl() ?>">
                            <span><?= Yii::t('main', $menu->getLabel()) ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <a href="/" class="navbar-brand mx-auto d-block text-center w-10"><img src="/public/static/images/logo.svg" alt="Ткани оптом - интернет-магазин Textile International" title="Ткани оптом - интернет-магазин Textile International"></a>
    <div class="navbar-collapse collapse dual-nav w-100">
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link phone" href="tel:+380674804880">
                    +38 (067) 482 60 00
                </a>
                <img src="/public/static/images/phoneIcon.svg" alt="">
            </li>
            <li class="nav-item">
                <div class="dropdown">
                    <button onclick="searchDropDown()" class="dropbtn nav-link searchButton">
                        Поиск
                        <img src="/public/static/images/searchIcon.svg" alt="Search">
                    </button>
                    <form method="get" action="<?= Yii::app()->createUrl('catalog/search') ?>">
                        <div id="dropSearch" class="dropdown-content">
                            <div class="searchPopup">
                                <div class="arrow-up"></div>
                                <input name="q" type="search" placeholder="Бенгалин"/>
                                <button>Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </li>

            <li class="nav-item">
                <div class="dropdown">
                    <button onclick="productsDropDown()" class="nav-link productCartButton">
                        Корзина

                        <span class="cartCounter cartIcon">
                            <span class="counter q-total-cart"><?= $cart->totalUniqueQ() ?></span>
                        </span>
                    </button>
                    <div id="dropProducts" class="product-dropdown-content">
                        <div class="productPopup">
                            <div class="arrow-up"></div>

                            <div class="productsListInCart cart-products-list">
                                <?php if ($cartItems) : ?>
                                    <?php foreach ($cartItems as $item) : ?>
                                        <div class="product-inPopup"
                                             data-product_cart="<?= $item->getItem()->getID() ?>">
                                            <img src="<?= $item->getItem()->getImg() ?>"
                                                 alt="<?= $item->getItem()->getTitle() ?>"/>
                                            <div class="pip-info">
                                                <span class="title"><?= $item->getItem()->getTitle() ?></span>
                                                <span class="pip-count">Количество: <span
                                                            data-product_cart="q"><?= $item->getQ() ?></span>рул.</span>
                                            </div>

                                            <div class="pip-price">
                                                <span data-product_cart="price"><?= $item->getPriceSum() ?></span>$
                                            </div>

                                            <i class="removeFromCart"
                                               onclick="deleteFromCart(<?= $item->getItem()->getID() ?>)"></i>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <span class="if-cart-is-empty" <?= $cartItems ? 'style="display:none"' : '' ?>>
                                        Нет товаров добавленных в корзину
                                    </span>
                            </div>

                            <div class="pip-footer if-cart-is-not-empty" <?= !$cartItems ? 'style="display:none"' : '' ?>>
                                <span class="priceAmount">Итого: <span
                                            class="price-total-cart"><?= $cart->total() ?></span>$</span>
                                <a href="<?= Yii::app()->createUrl('cart/index') ?>" rel="nofollow">
                                    <span>В корзину</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
	            <div id="dropAdd" class="dropdown-block">
		            <div class="add_to_cart_popup">
			            <div class="arrow-up"></div>
			            <p>Товар добавлен в корзину!</p>
		            </div>
	            </div>
            </li>
        </ul>
    </div>

    <ul class="nav navbar-nav ml-auto mobileNav">
        <li class="nav-item">
            <div class="dropdown">
                <button onclick="searchDropDownMobile()" class="dropbtn nav-link searchButton">
                    <img src="/public/static/images/searchIcon.svg" alt=""/>
                </button>
                <div id="dropSearchMobiles" class="dropdown-contentMobile">
                    <form method="get" action="/search/" class="searchPopupMobile">
	                    <i class="mobileCloseButton"></i>
                        <input name="q" type="search" placeholder="Бенгалин"/>
	                    <div class="arrow-up"></div>
                        <button type="submit"><img src="/public/static/images/arrow-right-search.svg" alt=""></button>
                    </form>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <div class="dropdown">
                <button onclick="productsMobileDropDown()" class="nav-links">
                        <span class="cartCounter cartIcon">
                            <span class="counter q-total-cart"><?= $cart->totalUniqueQ() ?></span>
                        </span>
                </button>
            </div>
            <div id="dropAddMobile" class="dropdown-block">
                <div class="add_to_cart_popup">
                    <div class="arrow-up"></div>
                    <p>Товар добавлен в корзину!</p>
                </div>
            </div>
        </li>
    </ul>


    <div id="dropProductsMobile" class="product-dropdown-content-mobile drp-mobile">
        <div class="productPopup">
            <div class="arrow-up"></div>
            <div class="cart-products-list">
                <?php if ($cartItems) : ?>
                    <div class="products_column">
                        <?php foreach ($cartItems as $item) : ?>
                            <div class="product-inPopup" data-product_cart="<?= $item->getItem()->getID() ?>">
                                <img src="<?= $item->getItem()->getImg() ?>" alt="<?= $item->getItem()->getTitle() ?>"/>
                                <div class="pip-info">
                                    <span class="title"><?= $item->getItem()->getTitle() ?></span>
                                    <span class="pip-count">Количество: <span data-product_cart="q"><?= $item->getQ() ?></span>рул.</span>
                                </div>

                                <div class="pip-price">
                                    <span data-product_cart="price"><?= $item->getPriceSum() ?></span>$
                                </div>

                                <i class="removeFromCart" onclick="deleteFromCart(<?= $item->getItem()->getID() ?>)"></i>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <div class="pip-footer if-cart-is-not-empty" <?= !$cartItems ? 'style="display:none"' : '' ?>>
                    <span class="priceAmount">Итого: <span class="price-total-cart"><?= $cart->total() ?></span>$</span>
                    <a href="<?= Yii::app()->createUrl('cart/index') ?>" rel="nofollow">
                        <span>В корзину</span>
                    </a>
                </div>
            </div>
            <span class="if-cart-is-empty" <?= $cartItems ? 'style="display:none"' : '' ?>>
                                        Нет товаров добавленных в корзину
                                    </span>
        </div>
    </div>
</nav>
