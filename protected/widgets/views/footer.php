<section class="footer">
    <div class="container">
        <div class="col-md-12 footerInfo">

            <?php if ($contacts) : ?>
                <div class="col-md-8 row">
                    <div class="title col-12">
                        Контакты
                    </div>
                    <?php foreach ($contacts as $contact) : ?>
                        <div class="footerAddress col-12 col-xl-4">
                            <span class="titleAddress"><?= $contact->city; ?>:</span>
                            <span class="info">
                        <a target="_blank"
                           href="<?=$contact->getCoordinates()?>">
                            <?= $contact->address; ?>
                        </a>
                        <br/>
                        <a href="tel:<?= $contact->getPhoneHref(); ?>">Тел. — <?= $contact->phone; ?></a>
                    </span>
                        </div>
                    <?php endforeach; ?>

                    <div class="footerAddress col-12 col-xl-4">
                        <span class="titleAddress">Другое:</span>
                        <p class="info">Тел. сайта: <a href="tel:+380674826000">+38 (067) 482 60 00</a></p>
                        <p class="info">Почта: <a href="mailto:textile-international@ukr.net">textile-international@ukr.net</a> </p>
                        <p class="info">Режим работы: <br><span>Пн -Сб 9.00 - 18.00</span> </p>
                    </div>

                </div>
            <?php endif; ?>
            <div class="col-md-4 footerFeedBack">
                <div class="title">
                    Свяжитесь с нами
                </div>

                <input type="text" data-type="name" name="name" placeholder="Имя">
                <input type="text" data-type="phone" name="telefone" placeholder="Номер телефона">
                <input type="text" data-type="email" name="email" placeholder="E-mail">
                <button class="sendFeedback" rel="nofollow" data-url="<?= Yii::app()->createUrl('send_email/index') ?>"
                        data-callback="feedBackModal">Отправить
                </button>
            </div>
        </div>

        <div class="copyright">
            textileinternational.com.ua (c) 2000-2017, <?=date('Y')?> — upd v2.0 <br/>
            разработка — <a href="https://comnd-x.com" target="_blank">comnd-x.com</a>, реклама — <a href="https://seomasters.com.ua/" target="_blank">seomasters.com.ua</a>
        </div>
    </div>
</section>
<div class="mobLayer"></div>

<div class="mobileNavigation">
    <?php if ($navigationMenu) : ?>
        <ul>
            <?php foreach ($navigationMenu as $menu) : ?>
                <li>
                    <a href="<?= $menu->getUrl() ?>"><?= $menu->getLabel() ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>


    <div class="phone_btn">
        <a href="tel:+38067826000">
            <span><i class="phone"></i></span>
            +38 (067) 482 60 00
        </a>
    </div>
</div>