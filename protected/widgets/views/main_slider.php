<?php if ($slider) :?>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <?php $i=0; foreach ($slider as $item) :?>
        <div class="carousel-item<?=$i==0 ? ' active' : ''?>">
            <img class="d-block w-100" src="/images/Carousel_header/<?=$item->image?>" alt="<?=$item->title ?>">
<!--            <img class="d-block w-100" src="/public/static/images/sliderImage1.png" alt="--><?//=$item->title ?><!--">-->

            <div class="banner-slogan">
                <?=$item->title ?>
<!--                <p>--><?//=$item->title ?><!--</p>-->
<!--                <p>--><?//=$item->short_body?><!--</p>-->
<!--                <p>--><?//=$item->meta_description?><!--</p>-->
<!--                <p>--><?//=$item->meta_keywords?><!--</p>-->

            </div>
        </div>
        <?php $i++; endforeach; ?>
    </div>
    <div class="carouseIndi">
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <ol class="carousel-indicators">
            <?php $i=0; foreach ($slider as $item) :?>
            <li data-target="#carouselExampleIndicators" data-slide-to="<?=$i?>"<?=$i==0 ? ' class="active"' : ''?>></li>
            <?php $i++; endforeach; ?>
        </ol>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<?php endif; ?>
