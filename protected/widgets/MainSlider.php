<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 30.11.2018
 * Time: 11:12
 */

class MainSlider extends CWidget
{

    public function init()
    {
        // этот метод будет вызван внутри CBaseController::beginWidget()
    }

    public function run(){
        $criteria = new CDbCriteria;
        $criteria->condition = "status = 1";
        $slider = Carousel_header::model()->findAll($criteria);
        $this->render('main_slider',array('slider'=>$slider));
    }

}