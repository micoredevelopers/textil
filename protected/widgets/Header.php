<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 04.12.2018
 * Time: 14:11
 */

class Header extends CWidget
{

    public function init()
    {

    }

    public function run(){
        $navigationMenu = (new NavigationMenu())->header();
        $sessionStorage = new CartStorageSession();
        $products = CartItemProduct::instancesByStorage($sessionStorage);
        $cart = (new CartList($sessionStorage))->addItemList($products);
        $cartItems = $cart->getItems();
        $this->render('header',['navigationMenu' => $navigationMenu, 'cart' => $cart, 'cartItems'=>$cartItems]);
    }

}