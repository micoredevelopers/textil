<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 05.12.2018
 * Time: 11:23
 */

class PhotosSlider extends CWidget
{



    public function init()
    {

    }

    public function run(){
        $criteria = new CDbCriteria;
        $criteria->condition = "status = 1";
        $slider = Carousel_header::model()->findAll($criteria);
        $this->render('main_slider',array('slider'=>$slider));
    }

}