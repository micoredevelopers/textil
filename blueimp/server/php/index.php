<?php
/*
 * jQuery File Upload Plugin PHP Example 5.7
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
$yii=$_SERVER['DOCUMENT_ROOT'].'/../../YiiRoot_1.1.13/framework/yii.php';
// var_dump($yii);
$config=$_SERVER['DOCUMENT_ROOT'].'/protected/config/main.php';
// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',false);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config);

session_start();
$_GET['id'] = isset($_GET['id']) ? $_GET['id'] : '';
$_SESSION['id'] = isset($_SESSION['id']) ? $_SESSION['id'] : '';
$id = isset($_GET['id']) ? $_GET['id'] : $_SESSION['id'];
if(!intval($id)) exit();
if(!Yii::app()->user->isAdmin()) exit();
$_GET['up_folder'] = isset($_GET['up_folder']) ? $_GET['up_folder'] : 'portfolio';
$up_folder = $_GET['up_folder']?$_GET['up_folder']:'portfolio';

error_reporting(E_ALL | E_STRICT);

require('upload.class.php');

// $dir_picture1 = $_SERVER["DOCUMENT_ROOT"].'/uploads/'.DATABASE_TABLE.'_others/files/'.$val['3'].'/';
$dir_picture2 = $_SERVER["DOCUMENT_ROOT"].'/images/'.$up_folder.'_others/files/'.intval($id).'/';
if( !is_dir($dir_picture2) ) {
    mkdir($dir_picture2, 0777,true);
} 

$o = array(
            'script_url' => '/blueimp/server/php/',
            'upload_dir' => dirname($_SERVER['SCRIPT_FILENAME']).'/../../../images/'.$_GET['up_folder'].'_others/files/'.intval($id).'/',
            'upload_url' => '/images/'.$_GET['up_folder'].'_others/files/'.intval($id).'/',
#            'param_name' => 'files',
#            // Set the following option to 'POST', if your server does not support
#            // DELETE requests. This is a parameter sent to the client:
#            'delete_type' => 'DELETE',
#            // The php.ini settings upload_max_filesize and post_max_size
#            // take precedence over the following max_file_size setting:
#            'max_file_size' => null,
#            'min_file_size' => 1,
#            'accept_file_types' => '/.+$/i',
#            // The maximum number of files for the upload directory:
#            'max_number_of_files' => null,
#            // Image resolution restrictions:
#            'max_width' => null,
#            'max_height' => null,
#            'min_width' => 1,
#            'min_height' => 1,
#            // Set the following option to false to enable resumable uploads:
#            'discard_aborted_uploads' => true,
#            // Set to true to rotate images based on EXIF meta data, if available:
#            'orient_image' => false,
            'image_versions' => array(
                // Uncomment the following version to restrict the size of
                // uploaded images. You can also add additional versions with
                // their own upload directories:
                /*
                'large' => array(
                    'upload_dir' => dirname($_SERVER['SCRIPT_FILENAME']).'/files/',
                    'upload_url' => $this->getFullUrl().'/files/',
                    'max_width' => 1920,
                    'max_height' => 1200,
                    'jpeg_quality' => 95
                ),
                */
                'thumbnail' => array(
                    // 'upload_dir' => dirname($_SERVER['SCRIPT_FILENAME']).'/../../../images/'.$up_folder.'_others/files/'.intval($id).'/thumbnails',
                    // 'upload_url' => '/images/'.$up_folder.'_others/files/'.intval($id).'/thumbnails',

#                    'upload_dir' => dirname($_SERVER['SCRIPT_FILENAME']).'/thumbnails/',
#                    'upload_url' => getFullUrl().'/thumbnails/',
#                    'max_width' => 80,
#                    'max_height' => 80
                )
            )
        );

$upload_handler = new UploadHandler($o);

header('Pragma: no-cache');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Content-Disposition: inline; filename="files.json"');
header('X-Content-Type-Options: nosniff');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: X-File-Name, X-File-Type, X-File-Size');

switch ($_SERVER['REQUEST_METHOD']) {
    case 'OPTIONS':
        break;
    case 'HEAD':
    case 'GET':
        $upload_handler->get();
        break;
    case 'POST':
        if (isset($_REQUEST['_method']) && $_REQUEST['_method'] === 'DELETE') {
            $upload_handler->delete();
        } else {
            $upload_handler->post();
        }
        break;
    case 'DELETE':
        $upload_handler->delete();
        break;
    default:
        header('HTTP/1.1 405 Method Not Allowed');
}

