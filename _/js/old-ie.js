(function() {

var me = {};

me.DOMContentLoaded = function(c) {
	var d=!1,e=!0,f=document.documentElement,b=function(a){a=a.type;
	if("readystatechange"!==a||"complete"===document.readyState)
	("load"===a?window:document).detachEvent("on"+a,b,!1),d||(d=!0,c())},
	g=function(){try{f.doScroll("left")}catch(a){setTimeout(g,50);return}
	b("poll")};if("complete"===document.readyState)c();else{
	if(document.createEventObject&&f.doScroll){try{e=!window.frameElement}
	catch(h){}e&&g()}document.attachEvent("onreadystatechange",b,!1);
	window.attachEvent("onload", b,!1)}
};

me.initOldIeMessage = function() {
	var msgRu = '<strong>�� ����������� ���������� �������.</strong> (�������&nbsp;� ��� ��������� ���&nbsp;��������� ������.)<br />������� ���� ������������ �&nbsp;������ ���������� ����, �&nbsp;����� ���������������� ����������. ���������� ����������� �������: <a href="http://firefox.com/">Firefox</a> (�������������), <a href="http://www.google.com/chrome/">Chrome</a> ���&nbsp;<a href="http://www.opera.com/">Opera</a>, ���&nbsp;�������� <a href="http://ie.microsoft.com/">Internet Explorer</a> ��&nbsp;������ ������. (��� ��� ���������.)</p><hr />';

	var msgEn = '<strong>You are using old browser.</strong> (Browser is&nbsp;a&nbsp;program to&nbsp;view websites.) So, the&nbsp;page is displayed with&nbsp;very simplified styling, and&nbsp;some features are not available. Install a&nbsp;modern browserГўв‚¬вЂќ<a href="http://firefox.com/">Firefox</a> (recommended), <a href="http://www.google.com/chrome/">Chrome</a>, <a href="http://www.opera.com/">Opera</a>, or&nbsp;update your <a href="http://ie.microsoft.com/">Internet Explorer</a> to&nbsp;latest version. (All of&nbsp;them are&nbsp;free.)';

	var msg = 'ru' === document.documentElement.getAttribute('lang')
	        ? msgRu : msgEn;

	var body = document.body,
	    elem = document.createElement('div');

	elem.id = 'old-ie';
	elem.innerHTML = '<p>' + msg + '</p><hr />';

	body.insertBefore(elem, body.firstChild);
};

me.DOMContentLoaded(function() {
	me.initOldIeMessage();
});

if (!document.querySelector) {
	me.ie7 = {};

	me.ie7.removeComments = function(){
		for(var b=document.getElementsByTagName("*"),a=b.length,c=[],d=0;d<a;d++)
		for(var f=b[d].childNodes,h=f.length,e=0;e<h;e++){
		var g=f[e];8===g.nodeType&&c.push(g)}b=c.length;
		for(a=0;a<b;a++)c[a].removeNode();document.body.className+=""
	};

	me.DOMContentLoaded(me.ie7.removeComments);
}

})();